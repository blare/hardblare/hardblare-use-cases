	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"malloc.c"
	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#64
	sub	sp, sp, #64
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, #2
	mov	r1, #0
	str	r1, [r11, #-4]
	str	r1, [r11, #-20]
	movw	r2, #1500
	str	r2, [r11, #-12]
	ldr	r2, [r11, #-12]
	movw	r3, :lower16:.L.str
	movt	r3, :upper16:.L.str
	str	r0, [r11, #-24]         @ 4-byte Spill
	mov	r0, r3
	str	r1, [r11, #-28]         @ 4-byte Spill
	mov	r1, r2
	bl	printf
.PTM_main_1:
	ldr	r1, [r11, #-28]         @ 4-byte Reload
	str	r0, [sp, #32]           @ 4-byte Spill
	mov	r0, r1
	bl	sbrk
.PTM_main_2:
	str	r0, [r11, #-20]
	ldr	r1, [r11, #-20]
	movw	r0, :lower16:.L.str.1
	movt	r0, :upper16:.L.str.1
	bl	printf
.PTM_main_3:
	ldr	r1, [r11, #-12]
	lsl	r1, r1, #2
	str	r0, [sp, #28]           @ 4-byte Spill
	mov	r0, r1
	bl	malloc
.PTM_main_4:
	movw	r1, #0
	str	r0, [r11, #-8]
	ldr	r0, [r11, #-8]
	cmp	r0, r1
	bne	.LBB0_2
	b	.LBB0_1
.LBB0_1:                                @ %if.then
.PTM_main_5:
	movw	r0, :lower16:.L.str.2
	movt	r0, :upper16:.L.str.2
	bl	printf
.PTM_main_6:
	movw	lr, #0
	str	r0, [sp, #24]           @ 4-byte Spill
	mov	r0, lr
	bl	exit
.LBB0_2:                                @ %if.else
.PTM_main_7:
	movw	r0, :lower16:.L.str.3
	movt	r0, :upper16:.L.str.3
	bl	printf
.PTM_main_8:
	movw	lr, :lower16:.L.str.4
	movt	lr, :upper16:.L.str.4
	str	r0, [sp, #20]           @ 4-byte Spill
	mov	r0, lr
	bl	printf
.PTM_main_9:
	movw	lr, #0
	movw	r1, :lower16:sbrk
	movt	r1, :upper16:sbrk
	str	r0, [sp, #16]           @ 4-byte Spill
	mov	r0, lr
	blx	r1
.PTM_main_10:
	movw	r1, :lower16:.L.str.5
	movt	r1, :upper16:.L.str.5
	str	r0, [r11, #-20]
	ldr	r0, [r11, #-20]
	str	r0, [sp, #12]           @ 4-byte Spill
	mov	r0, r1
	ldr	r1, [sp, #12]           @ 4-byte Reload
	bl	printf
.PTM_main_11:
	ldr	r1, [r11, #-8]
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, r1
	bl	free
.PTM_main_12:
	movw	r0, #0
	movw	r1, :lower16:sbrk
	movt	r1, :upper16:sbrk
	blx	r1
.PTM_main_13:
	movw	r1, :lower16:.L.str.6
	movt	r1, :upper16:.L.str.6
	str	r0, [r11, #-20]
	ldr	r0, [r11, #-20]
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, r1
	ldr	r1, [sp, #4]            @ 4-byte Reload
	bl	printf
.PTM_main_14:
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB0_3
.LBB0_3:                                @ %if.end
.PTM_main_15:
	movw	r0, #0
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"Enter number of elements: %d\n"
	.size	.L.str, 30

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"Program break Location 1 :%p\n"
	.size	.L.str.1, 30

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"Memory not allocated.\n"
	.size	.L.str.2, 23

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"Memory successfully allocated using malloc. \n"
	.size	.L.str.3, 46

	.type	.L.str.4,%object        @ @.str.4
.L.str.4:
	.asciz	"The elements of the array are: "
	.size	.L.str.4, 32

	.type	.L.str.5,%object        @ @.str.5
.L.str.5:
	.asciz	"Program break Location 2 :%p\n"
	.size	.L.str.5, 30

	.type	.L.str.6,%object        @ @.str.6
.L.str.6:
	.asciz	"Program break Location 3 :%p\n"
	.size	.L.str.6, 30


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	5
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430528
	.long	474284032
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.long	.PTM_main_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
