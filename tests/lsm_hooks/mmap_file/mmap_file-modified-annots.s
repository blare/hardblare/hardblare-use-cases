	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"mmap_file.c"
	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#224
	sub	sp, sp, #224
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, #0
	movw	r1, #1
	movw	r2, #2
	mov	r3, #0
	str	r3, [r11, #-4]
	movw	r12, :lower16:.L.str
	movt	r12, :upper16:.L.str
	str	r12, [sp, #100]
	str	r0, [sp, #84]           @ 4-byte Spill
	mov	r0, r12
	str	r1, [sp, #80]           @ 4-byte Spill
	mov	r1, r3
	str	r2, [sp, #76]           @ 4-byte Spill
	bl	open
.PTM_main_1:
	str	r0, [r11, #-8]
	ldr	r0, [r11, #-8]
	lsr	r0, r0, #31
	ldr	r2, [sp, #100]
	str	r0, [sp, #72]           @ 4-byte Spill
	str	r2, [sp, #68]           @ 4-byte Spill
	bl	__errno_location
.PTM_main_2:
	ldr	r0, [r0]
	bl	strerror
.PTM_main_3:
	movw	r1, :lower16:.L.str.1
	movt	r1, :upper16:.L.str.1
	ldr	r2, [sp, #72]           @ 4-byte Reload
	str	r0, [sp, #64]           @ 4-byte Spill
	mov	r0, r2
	ldr	r2, [sp, #68]           @ 4-byte Reload
	ldr	r3, [sp, #64]           @ 4-byte Reload
	bl	check
.PTM_main_4:
	ldr	r0, [r11, #-8]
	add	r1, sp, #112
	bl	fstat
.PTM_main_5:
	str	r0, [sp, #108]
	ldr	r0, [sp, #108]
	lsr	r0, r0, #31
	ldr	r2, [sp, #100]
	str	r0, [sp, #60]           @ 4-byte Spill
	str	r2, [sp, #56]           @ 4-byte Spill
	bl	__errno_location
.PTM_main_6:
	ldr	r0, [r0]
	bl	strerror
.PTM_main_7:
	movw	r1, :lower16:.L.str.2
	movt	r1, :upper16:.L.str.2
	ldr	r2, [sp, #60]           @ 4-byte Reload
	str	r0, [sp, #52]           @ 4-byte Spill
	mov	r0, r2
	ldr	r2, [sp, #56]           @ 4-byte Reload
	ldr	r3, [sp, #52]           @ 4-byte Reload
	bl	check
.PTM_main_8:
	ldr	r0, [sp, #160]
	str	r0, [sp, #104]
	ldr	r1, [sp, #104]
	ldr	r0, [r11, #-8]
	mov	r2, sp
	mov	r3, #0
	str	r3, [r2, #12]
	str	r3, [r2, #8]
	str	r0, [r2]
	mov	r2, #1
	mov	r0, #2
	str	r0, [sp, #48]           @ 4-byte Spill
	mov	r0, r3
	ldr	r3, [sp, #48]           @ 4-byte Reload
	bl	mmap
.PTM_main_9:
	mvn	r1, #0
	str	r0, [sp, #96]
	ldr	r0, [sp, #96]
	cmp	r0, r1
	movw	r0, #0
	moveq	r0, #1
	and	r0, r0, #1
	ldr	r2, [sp, #100]
	str	r0, [sp, #44]           @ 4-byte Spill
	str	r2, [sp, #40]           @ 4-byte Spill
	bl	__errno_location
.PTM_main_10:
	ldr	r0, [r0]
	bl	strerror
.PTM_main_11:
	movw	r1, :lower16:.L.str.3
	movt	r1, :upper16:.L.str.3
	ldr	r2, [sp, #44]           @ 4-byte Reload
	str	r0, [sp, #36]           @ 4-byte Spill
	mov	r0, r2
	ldr	r2, [sp, #40]           @ 4-byte Reload
	ldr	r3, [sp, #36]           @ 4-byte Reload
	bl	check
.PTM_main_12:
	movw	r0, #0
	str	r0, [sp, #92]
	b	.LBB0_1
.LBB0_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_13:
	ldr	r0, [sp, #92]
	ldr	r1, [sp, #104]
	cmp	r0, r1
	bhs	.LBB0_11
	b	.LBB0_2
.LBB0_2:                                @ %for.body
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_14:
	movw	r0, :lower16:.L.str.4
	movt	r0, :upper16:.L.str.4
	ldr	r1, [sp, #96]
	ldr	r2, [sp, #92]
	add	r1, r1, r2
	ldrb	r1, [r1]
	strb	r1, [sp, #91]
	ldr	r1, [sp, #96]
	ldr	r2, [sp, #92]
	add	r1, r1, r2
	ldrb	r2, [sp, #91]
	bl	printf
.PTM_main_15:
	str	r0, [sp, #32]           @ 4-byte Spill
	b	.LBB0_4
@ BB#3:                                 @ %cond.true
.PTM_main_16:
	ldrb	r0, [sp, #91]
	bl	isalpha
.PTM_main_17:
	cmp	r0, #0
	bne	.LBB0_7
.PTM_main_18:
	b	.LBB0_5
.LBB0_4:                                @ %cond.false
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_19:
	ldrb	r0, [sp, #91]
	orr	r0, r0, #32
	sub	r0, r0, #97
	cmp	r0, #26
	blo	.LBB0_7
	b	.LBB0_5
.LBB0_5:                                @ %land.lhs.true
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_20:
	ldrb	r0, [sp, #91]
	bl	__isspace
.PTM_main_21:
	cmp	r0, #0
	bne	.LBB0_7
	b	.LBB0_6
.LBB0_6:                                @ %if.then
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_22:
	ldrb	r0, [sp, #91]
	bl	putchar
.PTM_main_23:
	str	r0, [sp, #28]           @ 4-byte Spill
	b	.LBB0_7
.LBB0_7:                                @ %if.end
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_24:
	movw	r0, #80
	ldr	r1, [sp, #92]
	movw	r2, #26215
	movt	r2, #26214
	smmul	r2, r1, r2
	asr	r3, r2, #5
	add	r2, r3, r2, lsr #31
	add	r2, r2, r2, lsl #2
	sub	r1, r1, r2, lsl #4
	cmp	r1, #79
	str	r0, [sp, #24]           @ 4-byte Spill
	bne	.LBB0_9
	b	.LBB0_8
.LBB0_8:                                @ %if.then31
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_25:
	movw	r0, #10
	bl	putchar
.PTM_main_26:
	str	r0, [sp, #20]           @ 4-byte Spill
	b	.LBB0_9
.LBB0_9:                                @ %if.end33
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_27:
	b	.LBB0_10
	b	.LBB0_10
.LBB0_10:                               @ %for.inc
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_28:
	ldr	r0, [sp, #92]
	add	r0, r0, #1
	str	r0, [sp, #92]
	b	.LBB0_1
.LBB0_11:                               @ %for.end
.PTM_main_29:
	movw	r0, #0
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend

	.p2align	2
	.type	check,%function
	.code	32                      @ @check
check:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_check_0:
	.pad	#8
	sub	sp, sp, #8
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#24
	sub	sp, sp, #24
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	str	r3, [r11, #12]
	str	r2, [r11, #8]
	str	r0, [r11, #-4]
	str	r1, [r11, #-8]
	ldr	r0, [r11, #-4]
	cmp	r0, #0
	beq	.LBB1_2
	b	.LBB1_1
.LBB1_1:                                @ %if.then
.PTM_check_1:
	add	r0, r11, #8
	str	r0, [sp, #12]
	movw	r0, :lower16:stderr
	movt	r0, :upper16:stderr
	ldr	r0, [r0]
	ldr	r1, [r11, #-8]
	ldr	r2, [sp, #12]
	bl	vfprintf
.PTM_check_2:
	movw	r1, :lower16:.L.str.5
	movt	r1, :upper16:.L.str.5
	movw	r2, :lower16:stderr
	movt	r2, :upper16:stderr
	add	lr, sp, #12
	ldr	r2, [r2]
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, r2
	str	lr, [sp, #4]            @ 4-byte Spill
	bl	fprintf
.PTM_check_3:
	movw	r1, #1
	str	r0, [sp]                @ 4-byte Spill
	mov	r0, r1
	bl	exit
.LBB1_2:                                @ %if.end
.PTM_check_4:
	mov	sp, r11
	pop	{r11, lr}
	add	sp, sp, #8
	bx	lr
.Lfunc_end1:
	.size	check, .Lfunc_end1-check
	.cantunwind
	.fnend

	.p2align	2
	.type	__isspace,%function
	.code	32                      @ @__isspace
__isspace:
	.fnstart
@ BB#0:                                 @ %entry
.PTM___isspace_0:
	.pad	#8
	sub	sp, sp, #8
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC2_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC2_0+8))
.LPC2_0:
	ldr	r9, [pc, r9]
	movw	r1, #1
	str	r0, [sp, #4]
	ldr	r0, [sp, #4]
	cmp	r0, #32
	str	r1, [sp]                @ 4-byte Spill
	beq	.LBB2_2
	b	.LBB2_1
.LBB2_1:                                @ %lor.rhs
.PTM___isspace_1:
	ldr	r0, [sp, #4]
	sub	r0, r0, #9
	cmp	r0, #5
	movw	r0, #0
	movlo	r0, #1
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB2_2
.LBB2_2:                                @ %lor.end
.PTM___isspace_2:
	ldr	r0, [sp]                @ 4-byte Reload
	and	r0, r0, #1
	add	sp, sp, #8
	bx	lr
.Lfunc_end2:
	.size	__isspace, .Lfunc_end2-__isspace
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"test.txt"
	.size	.L.str, 9

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"open %s failed: %s"
	.size	.L.str.1, 19

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"stat %s failed: %s"
	.size	.L.str.2, 19

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"mmap %s failed: %s"
	.size	.L.str.3, 19

	.type	.L.str.4,%object        @ @.str.4
.L.str.4:
	.asciz	"\t%p = %c\n"
	.size	.L.str.4, 10

	.type	.L.str.5,%object        @ @.str.5
.L.str.5:
	.asciz	"\n"
	.size	.L.str.5, 2


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm 2b5c7ea7a610af4c0e9bb77143223b95c7b22f8b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	5
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430368
	.long	474873856
	.long	476381184
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474218496
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474218496
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	478937088
	.long	474284032
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474218496
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.long	.PTM_main_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357604864
	.long	2357604864
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.long	.PTM_main_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_17
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_18
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.long	.PTM_main_19
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_20
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_21
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_22
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_23
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.long	.PTM_main_24
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2391294336
	.long	478478336
	.long	509804544
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_25
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_26
	.section	.HB.annot,"a",%progbits
.Ltmp26:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp26(sbrel)
	.long	.PTM_main_27
	.section	.HB.annot,"a",%progbits
.Ltmp27:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp27(sbrel)
	.long	.PTM_main_28
	.section	.HB.annot,"a",%progbits
.Ltmp28:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp28(sbrel)
	.long	.PTM_main_29
	.section	.HB.annot,"a",%progbits
.Ltmp29:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp29(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_0
	.section	.HB.annot,"a",%progbits
.Ltmp30:
	.long	5
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp30(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430584
	.long	506527744
	.long	504430568
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_1
	.section	.HB.annot,"a",%progbits
.Ltmp31:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp31(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_2
	.section	.HB.annot,"a",%progbits
.Ltmp32:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp32(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474218496
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_3
	.section	.HB.annot,"a",%progbits
.Ltmp33:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp33(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_4
	.section	.HB.annot,"a",%progbits
.Ltmp34:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp34(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.long	504365064
	.long	502136832
	.section	.HB.bbt,"a",%progbits
	.long	.PTM___isspace_0
	.section	.HB.annot,"a",%progbits
.Ltmp35:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp35(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430584
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM___isspace_1
	.section	.HB.annot,"a",%progbits
.Ltmp36:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp36(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.section	.HB.bbt,"a",%progbits
	.long	.PTM___isspace_2
	.section	.HB.annot,"a",%progbits
.Ltmp37:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp37(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365064
	.long	502136832

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
