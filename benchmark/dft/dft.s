	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"dft.c"
	.globl	dft
	.p2align	3
	.type	dft,%function
	.code	32                      @ @dft
dft:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_dft_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#88
	sub	sp, sp, #88
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r12, #3
	str	r0, [r11, #-8]
	str	r1, [r11, #-12]
	str	r2, [r11, #-16]
	str	r3, [r11, #-20]
	mov	r0, #0
	str	r0, [sp, #28]
	str	r0, [sp, #24]
	ldr	r0, [r11, #-12]
	lsl	r0, r0, #3
	str	r12, [sp, #20]          @ 4-byte Spill
	bl	malloc
.PTM_dft_1:
	str	r0, [sp, #28]
	ldr	r0, [r11, #-12]
	lsl	r0, r0, #3
	bl	malloc
.PTM_dft_2:
	movw	r1, #0
	str	r0, [sp, #24]
	ldr	r0, [sp, #28]
	cmp	r0, r1
	beq	.LBB0_2
	b	.LBB0_1
.LBB0_1:                                @ %lor.lhs.false
.PTM_dft_3:
	movw	r0, #0
	ldr	r1, [sp, #24]
	cmp	r1, r0
	bne	.LBB0_3
	b	.LBB0_2
.LBB0_2:                                @ %if.then
.PTM_dft_4:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB0_23
.LBB0_3:                                @ %if.end
.PTM_dft_5:
	movw	r0, #0
	str	r0, [r11, #-24]
	b	.LBB0_4
.LBB0_4:                                @ %for.cond
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_6 Depth 2
.PTM_dft_6:
	ldr	r0, [r11, #-24]
	ldr	r1, [r11, #-12]
	cmp	r0, r1
	bge	.LBB0_11
	b	.LBB0_5
.LBB0_5:                                @ %for.body
                                        @   in Loop: Header=BB0_4 Depth=1
.PTM_dft_7:
	movw	r0, #0
	vldr	d16, .LCPI0_0
	vmov.f64	d17, #2.000000e+00
	vldr	d18, .LCPI0_1
	movw	r1, #3
	ldr	r2, [sp, #28]
	ldr	r3, [r11, #-24]
	mov	r12, #0
	str	r12, [r2, r3, lsl #3]!
	str	r12, [r2, #4]
	ldr	r2, [sp, #24]
	ldr	r3, [r11, #-24]
	add	r2, r2, r3, lsl #3
	vstr	d18, [r2]
	ldr	r2, [r11, #-8]
	sub	r2, r0, r2
	vmov	s0, r2
	vcvt.f64.s32	d18, s0
	vmul.f64	d17, d18, d17
	vmul.f64	d16, d17, d16
	ldr	r2, [r11, #-24]
	vmov	s0, r2
	vcvt.f64.s32	d17, s0
	vmul.f64	d16, d16, d17
	ldr	r2, [r11, #-12]
	vmov	s0, r2
	vcvt.f64.s32	d17, s0
	vdiv.f64	d16, d16, d17
	vstr	d16, [r11, #-40]
	str	r0, [r11, #-28]
	str	r1, [sp, #16]           @ 4-byte Spill
	b	.LBB0_6
.LBB0_6:                                @ %for.cond11
                                        @   Parent Loop BB0_4 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_dft_8:
	ldr	r0, [r11, #-28]
	ldr	r1, [r11, #-12]
	cmp	r0, r1
	bge	.LBB0_9
	b	.LBB0_7
.LBB0_7:                                @ %for.body14
                                        @   in Loop: Header=BB0_6 Depth=2
.PTM_dft_9:
	movw	r0, #3
	vldr	s0, [r11, #-28]
	vcvt.f64.s32	d16, s0
	vldr	d17, [r11, #-40]
	vmul.f64	d0, d16, d17
	str	r0, [sp, #12]           @ 4-byte Spill
	bl	cos
.PTM_dft_10:
	vstr	d0, [sp, #40]
	vldr	s2, [r11, #-28]
	vcvt.f64.s32	d16, s2
	vldr	d17, [r11, #-40]
	vmul.f64	d0, d16, d17
	bl	sin
.PTM_dft_11:
	vstr	d0, [sp, #32]
	ldr	r0, [r11, #-16]
	ldr	lr, [r11, #-28]
	add	r0, r0, lr, lsl #3
	vldr	d16, [r0]
	vldr	d17, [sp, #40]
	vmul.f64	d16, d16, d17
	ldr	r0, [r11, #-20]
	add	r0, r0, lr, lsl #3
	vldr	d17, [r0]
	vldr	d0, [sp, #32]
	vmls.f64	d16, d17, d0
	ldr	r0, [sp, #28]
	ldr	lr, [r11, #-24]
	add	r0, r0, lr, lsl #3
	vldr	d17, [r0]
	vadd.f64	d16, d17, d16
	vstr	d16, [r0]
	ldr	r0, [r11, #-16]
	ldr	lr, [r11, #-28]
	add	r0, r0, lr, lsl #3
	vldr	d16, [r0]
	vldr	d17, [sp, #32]
	ldr	r0, [r11, #-20]
	add	r0, r0, lr, lsl #3
	vldr	d0, [r0]
	vldr	d18, [sp, #40]
	vmul.f64	d18, d0, d18
	vmla.f64	d18, d16, d17
	ldr	r0, [sp, #24]
	ldr	lr, [r11, #-24]
	add	r0, r0, lr, lsl #3
	vldr	d16, [r0]
	vadd.f64	d16, d16, d18
	vstr	d16, [r0]
	b	.LBB0_8
.LBB0_8:                                @ %for.inc
                                        @   in Loop: Header=BB0_6 Depth=2
.PTM_dft_12:
	ldr	r0, [r11, #-28]
	add	r0, r0, #1
	str	r0, [r11, #-28]
	b	.LBB0_6
.LBB0_9:                                @ %for.end
                                        @   in Loop: Header=BB0_4 Depth=1
.PTM_dft_13:
	b	.LBB0_10
	b	.LBB0_10
.LBB0_10:                               @ %for.inc34
                                        @   in Loop: Header=BB0_4 Depth=1
.PTM_dft_14:
	ldr	r0, [r11, #-24]
	add	r0, r0, #1
	str	r0, [r11, #-24]
	b	.LBB0_4
.LBB0_11:                               @ %for.end36
.PTM_dft_15:
	ldr	r0, [r11, #-8]
	cmp	r0, #1
	bne	.LBB0_17
	b	.LBB0_12
.LBB0_12:                               @ %if.then39
.PTM_dft_16:
	movw	r0, #0
	str	r0, [r11, #-24]
	b	.LBB0_13
.LBB0_13:                               @ %for.cond40
                                        @ =>This Inner Loop Header: Depth=1
.PTM_dft_17:
	ldr	r0, [r11, #-24]
	ldr	r1, [r11, #-12]
	cmp	r0, r1
	bge	.LBB0_16
	b	.LBB0_14
.LBB0_14:                               @ %for.body43
                                        @   in Loop: Header=BB0_13 Depth=1
.PTM_dft_18:
	movw	r0, #3
	ldr	r1, [sp, #28]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vldr	d16, [r1]
	vldr	s0, [r11, #-12]
	vcvt.f64.s32	d17, s0
	vdiv.f64	d16, d16, d17
	ldr	r1, [r11, #-16]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	ldr	r1, [sp, #24]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vldr	d16, [r1]
	vldr	s0, [r11, #-12]
	vcvt.f64.s32	d17, s0
	vdiv.f64	d16, d16, d17
	ldr	r1, [r11, #-20]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	str	r0, [sp, #8]            @ 4-byte Spill
	b	.LBB0_15
.LBB0_15:                               @ %for.inc52
                                        @   in Loop: Header=BB0_13 Depth=1
.PTM_dft_19:
	ldr	r0, [r11, #-24]
	add	r0, r0, #1
	str	r0, [r11, #-24]
	b	.LBB0_13
.LBB0_16:                               @ %for.end54
.PTM_dft_20:
	b	.LBB0_22
.LBB0_17:                               @ %if.else
.PTM_dft_21:
	movw	r0, #0
	str	r0, [r11, #-24]
	b	.LBB0_18
.LBB0_18:                               @ %for.cond55
                                        @ =>This Inner Loop Header: Depth=1
.PTM_dft_22:
	ldr	r0, [r11, #-24]
	ldr	r1, [r11, #-12]
	cmp	r0, r1
	bge	.LBB0_21
	b	.LBB0_19
.LBB0_19:                               @ %for.body58
                                        @   in Loop: Header=BB0_18 Depth=1
.PTM_dft_23:
	movw	r0, #3
	ldr	r1, [sp, #28]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vldr	d16, [r1]
	ldr	r1, [r11, #-16]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	ldr	r1, [sp, #24]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vldr	d16, [r1]
	ldr	r1, [r11, #-20]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	str	r0, [sp, #4]            @ 4-byte Spill
	b	.LBB0_20
.LBB0_20:                               @ %for.inc63
                                        @   in Loop: Header=BB0_18 Depth=1
.PTM_dft_24:
	ldr	r0, [r11, #-24]
	add	r0, r0, #1
	str	r0, [r11, #-24]
	b	.LBB0_18
.LBB0_21:                               @ %for.end65
.PTM_dft_25:
	b	.LBB0_22
	b	.LBB0_22
.LBB0_22:                               @ %if.end66
.PTM_dft_26:
	ldr	r0, [sp, #28]
	bl	free
.PTM_dft_27:
	ldr	r0, [sp, #24]
	bl	free
.PTM_dft_28:
	movw	r0, #1
	str	r0, [r11, #-4]
	b	.LBB0_23
.LBB0_23:                               @ %return
.PTM_dft_29:
	ldr	r0, [r11, #-4]
	mov	sp, r11
	pop	{r11, pc}
	.p2align	3
@ BB#24:
.LCPI0_0:
	.long	1414677840              @ double 3.1415926540000001
	.long	1074340347
.LCPI0_1:
	.long	0                       @ double 0
	.long	0
.Lfunc_end0:
	.size	dft, .Lfunc_end0-dft
	.cantunwind
	.fnend

	.globl	main
	.p2align	3
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#40
	sub	sp, sp, #40
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r0, #0
	bl	time
.PTM_main_1:
	bl	srand
.PTM_main_2:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB1_1
.LBB1_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_3:
	ldr	r0, [r11, #-4]
	cmp	r0, #512
	bhs	.LBB1_4
	b	.LBB1_2
.LBB1_2:                                @ %for.body
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_main_4:
	movw	r0, :lower16:main.img
	movt	r0, :upper16:main.img
	movw	r1, #3
	str	r0, [r11, #-8]          @ 4-byte Spill
	str	r1, [r11, #-12]         @ 4-byte Spill
	bl	rand
.PTM_main_5:
	vmov	s0, r0
	vcvt.f64.s32	d16, s0
	vldr	d17, .LCPI1_0
	vdiv.f64	d16, d16, d17
	vadd.f64	d16, d16, d16
	vmov.f64	d18, #1.000000e+00
	vsub.f64	d16, d16, d18
	ldr	r0, [r11, #-4]
	movw	r1, :lower16:main.real
	movt	r1, :upper16:main.real
	add	r0, r1, r0, lsl #3
	vstr	d16, [r0]
	vstr	d17, [sp, #16]          @ 8-byte Spill
	vstr	d18, [sp, #8]           @ 8-byte Spill
	bl	rand
.PTM_main_6:
	vmov	s0, r0
	vcvt.f64.s32	d16, s0
	vldr	d17, [sp, #16]          @ 8-byte Reload
	vdiv.f64	d16, d16, d17
	vadd.f64	d16, d16, d16
	vldr	d18, [sp, #8]           @ 8-byte Reload
	vsub.f64	d16, d16, d18
	ldr	r0, [r11, #-4]
	movw	r1, :lower16:main.img
	movt	r1, :upper16:main.img
	add	r0, r1, r0, lsl #3
	vstr	d16, [r0]
	b	.LBB1_3
.LBB1_3:                                @ %for.inc
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_main_7:
	ldr	r0, [r11, #-4]
	add	r0, r0, #1
	str	r0, [r11, #-4]
	b	.LBB1_1
.LBB1_4:                                @ %for.end
.PTM_main_8:
	movw	r0, #1
	movw	r1, #2
	movw	r2, :lower16:main.real
	movt	r2, :upper16:main.real
	movw	r3, :lower16:main.img
	movt	r3, :upper16:main.img
	bl	dft
.PTM_main_9:
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
	.p2align	3
@ BB#5:
.LCPI1_0:
	.long	4290772992              @ double 2147483647
	.long	1105199103
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cantunwind
	.fnend

	.type	main.real,%object       @ @main.real
	.local	main.real
	.comm	main.real,4096,8
	.type	main.img,%object        @ @main.img
	.local	main.img
	.comm	main.img,4096,8

	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430504
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.long	.PTM_dft_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.long	.PTM_dft_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357739776
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.long	.PTM_dft_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.long	.PTM_dft_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.long	.PTM_dft_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.long	.PTM_dft_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.long	.PTM_dft_17
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_18
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.long	.PTM_dft_19
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.long	.PTM_dft_20
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.long	.PTM_dft_21
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.long	.PTM_dft_22
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_23
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.long	.PTM_dft_24
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.long	.PTM_dft_25
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.long	.PTM_dft_26
	.section	.HB.annot,"a",%progbits
.Ltmp26:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp26(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_27
	.section	.HB.annot,"a",%progbits
.Ltmp27:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp27(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_dft_28
	.section	.HB.annot,"a",%progbits
.Ltmp28:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp28(sbrel)
	.long	.PTM_dft_29
	.section	.HB.annot,"a",%progbits
.Ltmp29:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp29(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp30:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp30(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430552
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp31:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp31(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp32:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp32(sbrel)
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp33:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp33(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp34:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp34(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp35:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp35(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp36:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp36(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp37:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp37(sbrel)
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp38:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp38(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp39:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp39(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
