#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHECK_NOT_FROM_SUSPICIOUS_INPUT(X, Y) printf("Check %s , pointer = %p , len %d\n", X, &X, Y);


int check_integrity_word(char *sql_word)
{
  if(strncmp("SELECT", sql_word, strlen("SELECT"))) {
    CHECK_NOT_FROM_SUSPICIOUS_INPUT(sql_word, strlen(sql_word));
  }
  else if(strncmp("CREATE", sql_word, strlen("CREATE"))){
    CHECK_NOT_FROM_SUSPICIOUS_INPUT(sql_word, strlen(sql_word));
  }
  else if(strncmp("OR", sql_word, strlen("OR"))){
    CHECK_NOT_FROM_SUSPICIOUS_INPUT(sql_word, strlen(sql_word));
  }
  else if(strncmp("=", sql_word, strlen("="))){
    CHECK_NOT_FROM_SUSPICIOUS_INPUT(sql_word, strlen(sql_word));
  }
  else{}
}


int main(int argc, const char* argv[])
{
  char input[100];
  char sql_query[300];
  char* word;
  const char* separator = " ";

  fgets(input,100,stdin);
  sprintf(sql_query, "SELECT * FROM users WHERE name = 'admin' AND password = '%s'", input);
  printf("%s\n", sql_query);

  word = strtok(sql_query, separator);

  while (word != NULL) {
    printf("%p = %s\n", &word, word);
    check_integrity_word(word);
    word = strtok(NULL, separator);
  }

  // check_integrity(sql_query);
  // mysql_query(con, sql_query);

}
