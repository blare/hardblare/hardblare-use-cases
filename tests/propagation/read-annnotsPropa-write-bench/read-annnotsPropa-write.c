#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {

  int fd_input, fd_fuite, fd_notes, ret_input, ret_notes, i;
  char buffer_input_file[50];
  char buffer_notes_file[50];
  char buffer[50];
  int k = 0;

  for(k= 0; k<1000; k++){

  fd_input = open("input.txt", O_RDONLY);
  fd_notes = open("notes.txt", O_RDONLY);
  fd_fuite = open("fuite.txt",  O_WRONLY | O_APPEND);

  if (fd_notes < 0) {
    perror("Error fd_notes.txt \n"); exit(1);
  }
  if (fd_input < 0) {
    perror("Error input.txt \n"); exit(1);
  }
  if (fd_fuite < 0) {
    perror("Error output.txt \n"); exit(1);
  }

  ret_input = read(fd_input, buffer_input_file, sizeof(buffer_input_file));

  if (ret_input < 0) {
    perror("Error when reading \n"); exit(1);
  }

  ret_notes = read(fd_notes, buffer_notes_file, sizeof(buffer_notes_file));

  if (ret_notes < 0) {
    perror("Error when reading \n"); exit(1);
  }

  if(argc == 1){
	for(i = 0; i < sizeof(buffer_notes_file); i++) {
		buffer[i] = buffer_notes_file[i];
  	}
  	write(fd_fuite, buffer, ret_notes);
  }
  else{
	for(i = 0; i < sizeof(buffer_input_file); i++) {
		buffer[i] = buffer_input_file[i];
  	}
  	write(fd_fuite, buffer, ret_input);
  }

  close(fd_notes);
  close(fd_input);
  close(fd_fuite);

  }
}
