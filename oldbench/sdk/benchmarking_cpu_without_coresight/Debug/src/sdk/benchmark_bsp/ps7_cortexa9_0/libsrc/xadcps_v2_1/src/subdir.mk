################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_g.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_intr.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_selftest.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_sinit.c 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_g.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_intr.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_selftest.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_sinit.o 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_g.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_intr.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_selftest.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/xadcps_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/xadcps_v2_1/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


