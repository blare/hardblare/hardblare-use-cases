################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_bdring.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_control.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_g.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_hw.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_intr.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_sinit.c 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_bdring.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_control.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_g.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_hw.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_intr.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_sinit.o 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_bdring.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_control.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_g.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_hw.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_intr.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/xemacps_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/emacps_v2_2/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


