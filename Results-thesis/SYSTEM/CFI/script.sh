ETB="/sys/bus/coresight/devices/f8801000.etb"
TPIU="/sys/bus/coresight/devices/f8803000.tpiu"
PTM_CPU_0="/sys/bus/coresight/devices/f889c000.ptm0"
PTM_CPU_1="/sys/bus/coresight/devices/f889d000.ptm1"
CPU_1="/sys/devices/system/cpu/cpu1"
CPU_0="/sys/devices/system/cpu/cpu0"

chmod 777 /dev/uio0

# User application with annotations + BBT
EXEC=/home/root/$1

# Load the bitstream 
cat /home/root/PL_to_PS.bit > /dev/xdevcfg

# Disable CPU 1
echo -n 0 > $CPU_1/online 

# Disable CS components
echo -n 0 > $PTM_CPU_0/enable_source
echo -n 0 > $PTM_CPU_1/enable_source
echo -n 0 > $ETB/enable_sink
echo -n 0 > $TPIU/enable_sink
echo -n 1 > $PTM_CPU_0/reset

# Broadcast Branch
echo -n 30 > $PTM_CPU_0/mode

# Address range 

# echo -n $3 $4 > $PTM_CPU_0/addr_range 
# echo -n 0x0 0xffffffff > $PTM_CPU_0/addr_range

echo -n 0 > $PTM_CPU_0/addr_idx
echo -n 100 > $PTM_CPU_0/addr_acctype


# echo -n 1 > $PTM_CPU_0/addr_idx 
# echo -n 0xffff0000 0xffffffff > $PTM_CPU_0/addr_range
# echo -n 100 > $PTM_CPU_0/addr_acctype   


printf "\033[35m Tracing from $TEXT_START_ADDRESS to $TEXT_END_ADDRESS  \033[0m \n"

# HardBlare

# echo -n 0x6F > $PTM_CPU_0/trigger_event
echo -n 0xAC6F > $PTM_CPU_0/hardblare_ETMTEEVR
# echo -n 0x0 > $PTM_CPU_0/hardblare_ETMTSSCR
# echo -n 0x0 > $PTM_CPU_0/hardblare_ETMTECR1
echo -n 0xfff > $PTM_CPU_0/ctxid_mask

# Configure Coresight components
echo -n 0 > $ETB/enable_sink
# echo -n 1 > $TPIU/enable_sink
echo -n 0xfff > $PTM_CPU_0/sync_freq

# Run binary
sh $EXEC $2 $3

# Disable CS components
echo -n 0 > $ETB/enable_sink
echo -n 0 > $TPIU/enable_sink
echo -n 0 > $PTM_CPU_0/enable_source
echo -n 0 > $PTM_CPU_1/enable_source 


# debug tcesraces
# echo -e "\n\n ============== Basic Block Table + Annotations ================= \n\n"
# /home/root/accessMem -x bbt-annot -s $3

# DEBUG
# echo -e "\n\n ============== Debug ================= \n\n"
# /home/root/accessMem -x debug -s $3

