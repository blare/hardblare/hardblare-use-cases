################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/_profile_clean.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/_profile_init.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/_profile_timer_hw.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_cg.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_hist.c 

S_UPPER_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/dummy.S \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_mcount_arm.S \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_mcount_mb.S \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_mcount_ppc.S 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/_profile_clean.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/_profile_init.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/_profile_timer_hw.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/dummy.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_cg.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_hist.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_mcount_arm.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_mcount_mb.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_mcount_ppc.o 

S_UPPER_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/dummy.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_mcount_arm.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_mcount_mb.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_mcount_ppc.d 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/_profile_clean.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/_profile_init.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/_profile_timer_hw.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_cg.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/profile_hist.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/standalone_v4_2/src/profile/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


