


/*

  CWE-134: Use of Externally-Controlled Format String
  ---------------------------------------------------


  Description :
  ------------
  The software uses a function that accepts a format string as an argument, but the format string originates from an external source.
  When an attacker can modify an externally-controlled format string, this can lead to buffer overflows, denial of service, or data representation problems.
  It should be noted that in some circumstances, such as internationalization, the set of format strings is externally controlled by design. If the source of these format strings is trusted (e.g. only contained in library files that are only modifiable by the system administrator), then the external control might not itself pose a vulnerability.

  Example :
  --------
  The following program prints a string provided as an argument.
  The example is exploitable, because of the call to printf() in the printWrapper() function. Note: The stack buffer was added to make exploitation more simple.

 */


void printWrapper(char *string) {
  printf(string);
}

int main(int argc, char **argv) {
  char buf[5012];
  memcpy(buf, argv[1], 5012);
  printWrapper(argv[1]);
  return (0);
}





int main(int argc, const char* argv[])
{
  
  int secret_key[256] = {0x42};			// Secret variable (key encryption)
  char* message = argv[1];				// Not trusted input - size of data we want to 
  int size = sizeof(message);

  vuln (secret, size)

}




void vuln(char * param, int size) {

	char buff[12];
	char decision = 0;			// Could be modified (buffer overflow)

	for (int i = 0; i < size; i++ )
	{
		buff[i] = param[i];
	}

	if decision == 0
		hash (param)
	else
		printf (param)
}


hash(char*)

print_secret
 print(secret)

printf()





hash secret
printf est public






ROP
secret - public






