	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"read-annnotsPropa-write.c"
	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#216
	sub	sp, sp, #216
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r2, #0
	str	r2, [r11, #-4]
	str	r0, [r11, #-8]
	str	r1, [r11, #-12]
	str	r2, [sp, #24]
	str	r2, [sp, #24]
	b	.LBB0_1
.LBB0_1:                                @ %for.cond
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_14 Depth 2
                                        @     Child Loop BB0_19 Depth 2
.PTM_main_1:
	ldr	r0, [sp, #24]
	cmp	r0, #1000
	bge	.LBB0_25
	b	.LBB0_2
.LBB0_2:                                @ %for.body
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_2:
	movw	r0, :lower16:.L.str
	movt	r0, :upper16:.L.str
	movw	r1, #0
	bl	open
.PTM_main_3:
	movw	r1, :lower16:.L.str.1
	movt	r1, :upper16:.L.str.1
	movw	lr, #0
	str	r0, [r11, #-16]
	mov	r0, r1
	mov	r1, lr
	bl	open
.PTM_main_4:
	movw	r1, :lower16:.L.str.2
	movt	r1, :upper16:.L.str.2
	movw	lr, #1025
	str	r0, [r11, #-24]
	mov	r0, r1
	mov	r1, lr
	bl	open
.PTM_main_5:
	str	r0, [r11, #-20]
	ldr	r0, [r11, #-24]
	cmp	r0, #0
	bge	.LBB0_4
	b	.LBB0_3
.LBB0_3:                                @ %if.then
.PTM_main_6:
	movw	r0, :lower16:.L.str.3
	movt	r0, :upper16:.L.str.3
	bl	perror
.PTM_main_7:
	movw	r0, #1
	bl	exit
.LBB0_4:                                @ %if.end
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_8:
	ldr	r0, [r11, #-16]
	cmp	r0, #0
	bge	.LBB0_6
	b	.LBB0_5
.LBB0_5:                                @ %if.then5
.PTM_main_9:
	movw	r0, :lower16:.L.str.4
	movt	r0, :upper16:.L.str.4
	bl	perror
.PTM_main_10:
	movw	r0, #1
	bl	exit
.LBB0_6:                                @ %if.end6
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_11:
	ldr	r0, [r11, #-20]
	cmp	r0, #0
	bge	.LBB0_8
	b	.LBB0_7
.LBB0_7:                                @ %if.then8
.PTM_main_12:
	movw	r0, :lower16:.L.str.5
	movt	r0, :upper16:.L.str.5
	bl	perror
.PTM_main_13:
	movw	r0, #1
	bl	exit
.LBB0_8:                                @ %if.end9
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_14:
	movw	r2, #50
	sub	r1, r11, #86
	ldr	r0, [r11, #-16]
	bl	read
.PTM_main_15:
	str	r0, [r11, #-28]
	ldr	r0, [r11, #-28]
	cmp	r0, #0
	bge	.LBB0_10
	b	.LBB0_9
.LBB0_9:                                @ %if.then12
.PTM_main_16:
	movw	r0, :lower16:.L.str.6
	movt	r0, :upper16:.L.str.6
	bl	perror
.PTM_main_17:
	movw	r0, #1
	bl	exit
.LBB0_10:                               @ %if.end13
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_18:
	movw	r2, #50
	add	r1, sp, #80
	ldr	r0, [r11, #-24]
	bl	read
.PTM_main_19:
	str	r0, [r11, #-32]
	ldr	r0, [r11, #-32]
	cmp	r0, #0
	bge	.LBB0_12
	b	.LBB0_11
.LBB0_11:                               @ %if.then17
.PTM_main_20:
	movw	r0, :lower16:.L.str.6
	movt	r0, :upper16:.L.str.6
	bl	perror
.PTM_main_21:
	movw	r0, #1
	bl	exit
.LBB0_12:                               @ %if.end18
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_22:
	ldr	r0, [r11, #-8]
	cmp	r0, #1
	bne	.LBB0_18
	b	.LBB0_13
.LBB0_13:                               @ %if.then20
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_23:
	movw	r0, #0
	str	r0, [r11, #-36]
	b	.LBB0_14
.LBB0_14:                               @ %for.cond21
                                        @   Parent Loop BB0_1 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_main_24:
	ldr	r0, [r11, #-36]
	cmp	r0, #50
	bhs	.LBB0_17
	b	.LBB0_15
.LBB0_15:                               @ %for.body23
                                        @   in Loop: Header=BB0_14 Depth=2
.PTM_main_25:
	add	r0, sp, #30
	add	r1, sp, #80
	ldr	r2, [r11, #-36]
	add	r1, r1, r2
	ldrb	r1, [r1]
	ldr	r2, [r11, #-36]
	add	r0, r0, r2
	strb	r1, [r0]
	b	.LBB0_16
.LBB0_16:                               @ %for.inc
                                        @   in Loop: Header=BB0_14 Depth=2
.PTM_main_26:
	ldr	r0, [r11, #-36]
	add	r0, r0, #1
	str	r0, [r11, #-36]
	b	.LBB0_14
.LBB0_17:                               @ %for.end
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_27:
	add	r1, sp, #30
	ldr	r0, [r11, #-20]
	ldr	r2, [r11, #-32]
	bl	write
.PTM_main_28:
	str	r0, [sp, #20]           @ 4-byte Spill
	b	.LBB0_23
.LBB0_18:                               @ %if.else
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_29:
	movw	r0, #0
	str	r0, [r11, #-36]
	b	.LBB0_19
.LBB0_19:                               @ %for.cond27
                                        @   Parent Loop BB0_1 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_main_30:
	ldr	r0, [r11, #-36]
	cmp	r0, #50
	bhs	.LBB0_22
	b	.LBB0_20
.LBB0_20:                               @ %for.body29
                                        @   in Loop: Header=BB0_19 Depth=2
.PTM_main_31:
	add	r0, sp, #30
	sub	r1, r11, #86
	ldr	r2, [r11, #-36]
	add	r1, r1, r2
	ldrb	r1, [r1]
	ldr	r2, [r11, #-36]
	add	r0, r0, r2
	strb	r1, [r0]
	b	.LBB0_21
.LBB0_21:                               @ %for.inc32
                                        @   in Loop: Header=BB0_19 Depth=2
.PTM_main_32:
	ldr	r0, [r11, #-36]
	add	r0, r0, #1
	str	r0, [r11, #-36]
	b	.LBB0_19
.LBB0_22:                               @ %for.end34
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_33:
	add	r1, sp, #30
	ldr	r0, [r11, #-20]
	ldr	r2, [r11, #-28]
	bl	write
.PTM_main_34:
	str	r0, [sp, #16]           @ 4-byte Spill
	b	.LBB0_23
.LBB0_23:                               @ %if.end37
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_35:
	ldr	r0, [r11, #-24]
	bl	close
.PTM_main_36:
	ldr	lr, [r11, #-16]
	str	r0, [sp, #12]           @ 4-byte Spill
	mov	r0, lr
	bl	close
.PTM_main_37:
	ldr	lr, [r11, #-20]
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, lr
	bl	close
.PTM_main_38:
	str	r0, [sp, #4]            @ 4-byte Spill
	b	.LBB0_24
.LBB0_24:                               @ %for.inc41
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_39:
	ldr	r0, [sp, #24]
	add	r0, r0, #1
	str	r0, [sp, #24]
	b	.LBB0_1
.LBB0_25:                               @ %for.end43
.PTM_main_40:
	ldr	r0, [r11, #-4]
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"input.txt"
	.size	.L.str, 10

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"notes.txt"
	.size	.L.str.1, 10

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"fuite.txt"
	.size	.L.str.2, 10

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"Error fd_notes.txt \n"
	.size	.L.str.3, 21

	.type	.L.str.4,%object        @ @.str.4
.L.str.4:
	.asciz	"Error input.txt \n"
	.size	.L.str.4, 18

	.type	.L.str.5,%object        @ @.str.5
.L.str.5:
	.asciz	"Error output.txt \n"
	.size	.L.str.5, 19

	.type	.L.str.6,%object        @ @.str.6
.L.str.6:
	.asciz	"Error when reading \n"
	.size	.L.str.6, 21


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430376
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476708864
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476708864
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_17
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_18
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_19
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_20
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_21
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_22
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_23
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.long	.PTM_main_24
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_25
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357604864
	.long	2357469696
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_26
	.section	.HB.annot,"a",%progbits
.Ltmp26:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp26(sbrel)
	.long	.PTM_main_27
	.section	.HB.annot,"a",%progbits
.Ltmp27:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp27(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_28
	.section	.HB.annot,"a",%progbits
.Ltmp28:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp28(sbrel)
	.long	.PTM_main_29
	.section	.HB.annot,"a",%progbits
.Ltmp29:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp29(sbrel)
	.long	.PTM_main_30
	.section	.HB.annot,"a",%progbits
.Ltmp30:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp30(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_31
	.section	.HB.annot,"a",%progbits
.Ltmp31:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp31(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357604864
	.long	2357469696
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_32
	.section	.HB.annot,"a",%progbits
.Ltmp32:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp32(sbrel)
	.long	.PTM_main_33
	.section	.HB.annot,"a",%progbits
.Ltmp33:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp33(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_34
	.section	.HB.annot,"a",%progbits
.Ltmp34:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp34(sbrel)
	.long	.PTM_main_35
	.section	.HB.annot,"a",%progbits
.Ltmp35:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp35(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_36
	.section	.HB.annot,"a",%progbits
.Ltmp36:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp36(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_37
	.section	.HB.annot,"a",%progbits
.Ltmp37:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp37(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_38
	.section	.HB.annot,"a",%progbits
.Ltmp38:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp38(sbrel)
	.long	.PTM_main_39
	.section	.HB.annot,"a",%progbits
.Ltmp39:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp39(sbrel)
	.long	.PTM_main_40
	.section	.HB.annot,"a",%progbits
.Ltmp40:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp40(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
