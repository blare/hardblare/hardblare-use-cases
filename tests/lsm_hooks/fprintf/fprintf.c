#include <sys/uio.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>


int main() {

  FILE * inputFile;

  inputFile = fopen( "test.txt", "a");

  if ( inputFile == NULL ) {
    fprintf( stderr, "Error fopen\n" );
    exit(0);
  }

  fprintf(inputFile, "Appending a new message in the file\n");

  fclose(inputFile);

  return 0;

}
