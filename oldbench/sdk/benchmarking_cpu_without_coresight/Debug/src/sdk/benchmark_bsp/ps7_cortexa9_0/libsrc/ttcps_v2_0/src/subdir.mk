################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_g.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_options.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_selftest.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_sinit.c 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_g.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_options.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_selftest.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_sinit.o 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_g.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_options.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_selftest.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/xttcps_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/ttcps_v2_0/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


