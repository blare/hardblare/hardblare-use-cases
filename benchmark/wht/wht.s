	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"wht.c"
	.globl	wht_bfly
	.p2align	2
	.type	wht_bfly,%function
	.code	32                      @ @wht_bfly
wht_bfly:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_wht_bfly_0:
	.pad	#12
	sub	sp, sp, #12
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	str	r0, [sp, #8]
	str	r1, [sp, #4]
	ldr	r0, [sp, #8]
	ldr	r0, [r0]
	str	r0, [sp]
	ldr	r0, [sp, #4]
	ldr	r0, [r0]
	ldr	r1, [sp, #8]
	ldr	r2, [r1]
	add	r0, r2, r0
	str	r0, [r1]
	ldr	r0, [sp]
	ldr	r1, [sp, #4]
	ldr	r1, [r1]
	sub	r0, r0, r1
	ldr	r1, [sp, #4]
	str	r0, [r1]
	add	sp, sp, #12
	bx	lr
.Lfunc_end0:
	.size	wht_bfly, .Lfunc_end0-wht_bfly
	.cantunwind
	.fnend

	.globl	log2_f
	.p2align	2
	.type	log2_f,%function
	.code	32                      @ @log2_f
log2_f:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_log2_f_0:
	.pad	#12
	sub	sp, sp, #12
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r1, #0
	str	r0, [sp, #8]
	str	r1, [sp, #4]
	b	.LBB1_1
.LBB1_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_log2_f_1:
	ldr	r0, [sp, #8]
	cmp	r0, #0
	ble	.LBB1_4
	b	.LBB1_2
.LBB1_2:                                @ %for.body
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_log2_f_2:
	ldr	r0, [sp, #4]
	add	r0, r0, #1
	str	r0, [sp, #4]
	b	.LBB1_3
.LBB1_3:                                @ %for.inc
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_log2_f_3:
	movw	r0, #1
	ldr	r1, [sp, #8]
	asr	r1, r1, #1
	str	r1, [sp, #8]
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB1_1
.LBB1_4:                                @ %for.end
.PTM_log2_f_4:
	ldr	r0, [sp, #4]
	add	sp, sp, #12
	bx	lr
.Lfunc_end1:
	.size	log2_f, .Lfunc_end1-log2_f
	.cantunwind
	.fnend

	.globl	fwht
	.p2align	2
	.type	fwht,%function
	.code	32                      @ @fwht
fwht:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_fwht_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#32
	sub	sp, sp, #32
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC2_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC2_0+8))
.LPC2_0:
	ldr	r9, [pc, r9]
	str	r0, [r11, #-4]
	str	r1, [r11, #-8]
	ldr	r0, [r11, #-8]
	bl	log2_f
.PTM_fwht_1:
	movw	r1, #0
	sub	r0, r0, #1
	str	r0, [r11, #-12]
	str	r1, [sp, #16]
	b	.LBB2_1
.LBB2_1:                                @ %for.cond
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB2_3 Depth 2
                                        @       Child Loop BB2_5 Depth 3
.PTM_fwht_2:
	ldr	r0, [sp, #16]
	ldr	r1, [r11, #-12]
	cmp	r0, r1
	bhs	.LBB2_12
	b	.LBB2_2
.LBB2_2:                                @ %for.body
                                        @   in Loop: Header=BB2_1 Depth=1
.PTM_fwht_3:
	movw	r0, #0
	str	r0, [sp, #12]
	b	.LBB2_3
.LBB2_3:                                @ %for.cond1
                                        @   Parent Loop BB2_1 Depth=1
                                        @ =>  This Loop Header: Depth=2
                                        @       Child Loop BB2_5 Depth 3
.PTM_fwht_4:
	movw	r0, #1
	ldr	r1, [sp, #12]
	ldr	r2, [r11, #-12]
	lsl	r0, r0, r2
	cmp	r1, r0
	bhs	.LBB2_10
	b	.LBB2_4
.LBB2_4:                                @ %for.body3
                                        @   in Loop: Header=BB2_3 Depth=2
.PTM_fwht_5:
	movw	r0, #0
	str	r0, [sp, #8]
	b	.LBB2_5
.LBB2_5:                                @ %for.cond4
                                        @   Parent Loop BB2_1 Depth=1
                                        @     Parent Loop BB2_3 Depth=2
                                        @ =>    This Inner Loop Header: Depth=3
.PTM_fwht_6:
	movw	r0, #1
	ldr	r1, [sp, #8]
	ldr	r2, [sp, #16]
	lsl	r0, r0, r2
	cmp	r1, r0
	bhs	.LBB2_8
	b	.LBB2_6
.LBB2_6:                                @ %for.body7
                                        @   in Loop: Header=BB2_5 Depth=3
.PTM_fwht_7:
	movw	r0, #2
	ldr	r1, [r11, #-4]
	ldr	r2, [sp, #12]
	ldr	r3, [sp, #8]
	add	r2, r2, r3
	add	r3, r1, r2, lsl #2
	ldr	r12, [sp, #16]
	mov	lr, #1
	add	r2, r2, lr, lsl r12
	add	r1, r1, r2, lsl #2
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, r3
	bl	wht_bfly
	b	.LBB2_7
.LBB2_7:                                @ %for.inc
                                        @   in Loop: Header=BB2_5 Depth=3
.PTM_fwht_8:
	ldr	r0, [sp, #8]
	add	r0, r0, #1
	str	r0, [sp, #8]
	b	.LBB2_5
.LBB2_8:                                @ %for.end
                                        @   in Loop: Header=BB2_3 Depth=2
.PTM_fwht_9:
	b	.LBB2_9
	b	.LBB2_9
.LBB2_9:                                @ %for.inc12
                                        @   in Loop: Header=BB2_3 Depth=2
.PTM_fwht_10:
	movw	r0, #1
	ldr	r1, [sp, #16]
	add	r1, r1, #1
	lsl	r0, r0, r1
	ldr	r1, [sp, #12]
	add	r0, r1, r0
	str	r0, [sp, #12]
	b	.LBB2_3
.LBB2_10:                               @ %for.end16
                                        @   in Loop: Header=BB2_1 Depth=1
.PTM_fwht_11:
	b	.LBB2_11
	b	.LBB2_11
.LBB2_11:                               @ %for.inc17
                                        @   in Loop: Header=BB2_1 Depth=1
.PTM_fwht_12:
	ldr	r0, [sp, #16]
	add	r0, r0, #1
	str	r0, [sp, #16]
	b	.LBB2_1
.LBB2_12:                               @ %for.end19
.PTM_fwht_13:
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end2:
	.size	fwht, .Lfunc_end2-fwht
	.cantunwind
	.fnend

	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC3_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC3_0+8))
.LPC3_0:
	ldr	r9, [pc, r9]
	movw	r0, #0
	bl	time
.PTM_main_1:
	bl	srand
.PTM_main_2:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB3_1
.LBB3_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_3:
	ldr	r0, [r11, #-4]
	cmp	r0, #4
	bhs	.LBB3_4
	b	.LBB3_2
.LBB3_2:                                @ %for.body
                                        @   in Loop: Header=BB3_1 Depth=1
.PTM_main_4:
	movw	r0, :lower16:main.data
	movt	r0, :upper16:main.data
	movw	r1, #2
	str	r0, [sp, #8]            @ 4-byte Spill
	str	r1, [sp, #4]            @ 4-byte Spill
	bl	rand
.PTM_main_5:
	ldr	r1, [r11, #-4]
	movw	lr, :lower16:main.data
	movt	lr, :upper16:main.data
	add	r1, lr, r1, lsl #2
	str	r0, [r1]
	b	.LBB3_3
.LBB3_3:                                @ %for.inc
                                        @   in Loop: Header=BB3_1 Depth=1
.PTM_main_6:
	ldr	r0, [r11, #-4]
	add	r0, r0, #1
	str	r0, [r11, #-4]
	b	.LBB3_1
.LBB3_4:                                @ %for.end
.PTM_main_7:
	movw	r0, :lower16:main.data
	movt	r0, :upper16:main.data
	movw	r1, #4
	bl	fwht
.PTM_main_8:
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cantunwind
	.fnend

	.type	main.data,%object       @ @main.data
	.local	main.data
	.comm	main.data,16,4

	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_wht_bfly_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	5
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430580
	.long	2357469696
	.long	2357469568
	.long	504365068
	.long	502136832
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_log2_f_0
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430580
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_log2_f_1
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_log2_f_2
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.long	.PTM_log2_f_3
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.long	.PTM_log2_f_4
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365068
	.long	502136832
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fwht_0
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430560
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fwht_1
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.long	.PTM_fwht_2
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fwht_3
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.long	.PTM_fwht_4
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fwht_5
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.long	.PTM_fwht_6
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fwht_7
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357740160
	.long	480444416
	.long	474284032
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fwht_8
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.long	.PTM_fwht_9
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.long	.PTM_fwht_10
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469568
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fwht_11
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.long	.PTM_fwht_12
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.long	.PTM_fwht_13
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430576
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	476708864
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp26:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp26(sbrel)
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp27:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp27(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp28:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp28(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
