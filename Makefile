
CWE_DIR := CWE
FPGA := fpga.bit
TRACE := trace.sh
TEST_TAG_MEMORY := test_tag_memory.elf


SOURCES_FILES := $(wildcard $(CWE_DIR)/*/*.c)

BINARIES := $(patsubst %.c, %.elf, $(SOURCES_FILES))

SRC_FILES := $(dir $(BINARIES))


all: $(BINARIES) test_tag_memory/$(TEST_TAG_MEMORY)
	echo $(BINARIES)
	echo ------------------
	echo  $(SRC_FILES)

CWE/%.elf: CWE/%.c
	@$(CC) $(CFLAGS) $< -o $@

test_tag_memory/$(TEST_TAG_MEMORY): test_tag_memory/test_tag_memory.c
	@$(CC) $(CFLAGS) $< -o $@

clean:
	@rm -rf $(BINARIES)
	@rm -rf test_tag_memory/$(TEST_TAG_MEMORY)

install:
	install -d $(DESTDIR)/home/root
	install -d $(DESTDIR)/home/root/test_tag_memory
	cp -r CWE $(DESTDIR)/home/root
	cp $(FPGA) $(DESTDIR)/home/root
	cp $(TRACE) $(DESTDIR)/home/root
	cp -r test_tag_memory $(DESTDIR)/home/root

.PHONY: clean
