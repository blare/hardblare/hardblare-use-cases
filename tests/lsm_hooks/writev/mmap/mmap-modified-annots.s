	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"mmap.c"
	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#80
	sub	sp, sp, #80
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, #7
	movw	r1, #34
	movw	r2, #0
	movw	r3, #20
	mov	r12, #0
	str	r12, [r11, #-4]
	str	r0, [r11, #-24]         @ 4-byte Spill
	str	r1, [r11, #-28]         @ 4-byte Spill
	str	r2, [r11, #-32]         @ 4-byte Spill
	str	r3, [r11, #-36]         @ 4-byte Spill
	bl	getpagesize
.PTM_main_1:
	str	r0, [r11, #-8]
	movw	r0, :lower16:.L.str
	movt	r0, :upper16:.L.str
	str	r0, [r11, #-12]
	ldr	r1, [r11, #-8]
	movw	r0, :lower16:.L.str.1
	movt	r0, :upper16:.L.str.1
	bl	printf
.PTM_main_2:
	ldr	r1, [r11, #-8]
	lsl	r1, r1, #20
	ldr	r2, [r11, #-8]
	mov	r3, sp
	mov	r12, #0
	str	r12, [r3, #12]
	str	r12, [r3, #8]
	str	r12, [r3]
	mov	r3, #7
	mov	r12, #34
	str	r0, [sp, #40]           @ 4-byte Spill
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r12
	bl	mmap
.PTM_main_3:
	mvn	r1, #0
	str	r0, [r11, #-16]
	ldr	r0, [r11, #-16]
	cmp	r0, r1
	bne	.LBB0_2
	b	.LBB0_1
.LBB0_1:                                @ %if.then
.PTM_main_4:
	movw	r0, :lower16:.L.str.2
	movt	r0, :upper16:.L.str.2
	bl	perror
.PTM_main_5:
	movw	r0, #1
	str	r0, [r11, #-4]
	b	.LBB0_5
.LBB0_2:                                @ %if.end
.PTM_main_6:
	movw	r0, :lower16:.L.str.3
	movt	r0, :upper16:.L.str.3
	ldr	r1, [r11, #-16]
	ldr	r2, [r11, #-12]
	str	r0, [sp, #36]           @ 4-byte Spill
	mov	r0, r1
	mov	r1, r2
	bl	strcpy
.PTM_main_7:
	ldr	r1, [r11, #-12]
	ldr	r2, [sp, #36]           @ 4-byte Reload
	str	r0, [sp, #32]           @ 4-byte Spill
	mov	r0, r2
	bl	printf
.PTM_main_8:
	movw	r1, :lower16:.L.str.4
	movt	r1, :upper16:.L.str.4
	ldr	r2, [r11, #-16]
	str	r0, [sp, #28]           @ 4-byte Spill
	mov	r0, r1
	mov	r1, r2
	bl	printf
.PTM_main_9:
	movw	r1, :lower16:.L.str.5
	movt	r1, :upper16:.L.str.5
	ldr	r2, [r11, #-16]
	str	r0, [sp, #24]           @ 4-byte Spill
	mov	r0, r1
	mov	r1, r2
	bl	printf
.PTM_main_10:
	movw	r1, #1024
	ldr	r2, [r11, #-16]
	str	r0, [sp, #20]           @ 4-byte Spill
	mov	r0, r2
	bl	munmap
.PTM_main_11:
	str	r0, [r11, #-20]
	ldr	r0, [r11, #-20]
	cmp	r0, #0
	beq	.LBB0_4
	b	.LBB0_3
.LBB0_3:                                @ %if.then9
.PTM_main_12:
	movw	r0, :lower16:.L.str.6
	movt	r0, :upper16:.L.str.6
	bl	perror
.PTM_main_13:
	movw	r0, #1
	str	r0, [r11, #-4]
	b	.LBB0_5
.LBB0_4:                                @ %if.end10
.PTM_main_14:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB0_5
.LBB0_5:                                @ %return
.PTM_main_15:
	ldr	r0, [r11, #-4]
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"Hello, poftut.com"
	.size	.L.str, 18

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"System page size: %zu bytes\n"
	.size	.L.str.1, 29

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"Could not mmap"
	.size	.L.str.2, 15

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"Address of text: %p\n"
	.size	.L.str.3, 21

	.type	.L.str.4,%object        @ @.str.4
.L.str.4:
	.asciz	"Address of region: %p\n"
	.size	.L.str.4, 23

	.type	.L.str.5,%object        @ @.str.5
.L.str.5:
	.asciz	"Contents of region: %s\n"
	.size	.L.str.5, 24

	.type	.L.str.6,%object        @ @.str.6
.L.str.6:
	.asciz	"Could not munmap"
	.size	.L.str.6, 17


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm 2b5c7ea7a610af4c0e9bb77143223b95c7b22f8b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430512
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	6
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	481034240
	.long	474152960
	.long	476315648
	.long	478478336
	.long	481165312
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474218496
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474218496
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.long	.PTM_main_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.long	.PTM_main_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
