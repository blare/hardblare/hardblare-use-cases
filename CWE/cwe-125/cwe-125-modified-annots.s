	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"cwe-125.c"
	.globl	getValueFromArray
	.p2align	2
	.type	getValueFromArray,%function
	.code	32                      @ @getValueFromArray
getValueFromArray:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_getValueFromArray_0:
	.pad	#20
	sub	sp, sp, #20
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	str	r0, [sp, #16]
	str	r1, [sp, #12]
	str	r2, [sp, #8]
	ldr	r0, [sp, #8]
	ldr	r1, [sp, #12]
	cmp	r0, r1
	bge	.LBB0_2
	b	.LBB0_1
.LBB0_1:                                @ %if.then
.PTM_getValueFromArray_1:
	movw	r0, #2
	ldr	r1, [sp, #16]
	ldr	r2, [sp, #8]
	add	r1, r1, r2, lsl #2
  str r1, [r9]  // Instr @address of read
	ldr	r1, [r1]  // Check
	str	r1, [sp, #4]
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB0_3
.LBB0_2:                                @ %if.else
.PTM_getValueFromArray_2:
	mvn	r0, #0
	str	r0, [sp, #4]
	b	.LBB0_3
.LBB0_3:                                @ %if.end
.PTM_getValueFromArray_3:
	ldr	r0, [sp, #4]
	add	sp, sp, #20
	bx	lr
.Lfunc_end0:
	.size	getValueFromArray, .Lfunc_end0-getValueFromArray
	.cantunwind
	.fnend

	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
	.LPC1_0:
	ldr	r9, [pc, r9]
	.save	{r4, r10, r11, lr}
	push	{r4, r10, r11, lr}
	.setfp	r11, sp, #8
	add	r11, sp, #8
	.pad	#208
	sub	sp, sp, #208
  str sp, [r9] // Instr SP
	movw	r2, :lower16:atoi
	movt	r2, :upper16:atoi
	movw	r3, #42
	movw	r12, #0
	movw	lr, #168
	add	r4, sp, #32
	str	r0, [r11, #-12]
	str	r1, [r11, #-16]
	mov	r0, r4
	and	r1, r12, #255
	str	r2, [sp, #20]           @ 4-byte Spill
	mov	r2, lr
	str	r3, [sp, #16]           @ 4-byte Spill
	bl	memset
.PTM_main_1:
	ldr	r0, [sp, #16]           @ 4-byte Reload
	str	r0, [sp, #32]
	ldr	r1, [r11, #-16]
	ldr	r0, [r1, #4]
	ldr	r1, [sp, #20]           @ 4-byte Reload
	blx	r1
.PTM_main_2:
	movw	r1, #42
	add	r2, sp, #32
	str	r0, [sp, #28]
	ldr	r0, [sp, #28]
	str	r0, [sp, #12]           @ 4-byte Spill
	mov	r0, r2
	ldr	r2, [sp, #12]           @ 4-byte Reload
	bl	getValueFromArray
.PTM_main_3:
	movw	r1, :lower16:.L.str
	movt	r1, :upper16:.L.str
	str	r0, [sp, #24]
	ldr	r0, [sp, #24]
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, r1
	ldr	r1, [sp, #8]            @ 4-byte Reload
	bl	printf
.PTM_main_4:
	movw	r1, #0
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, r1
	sub	sp, r11, #8
	pop	{r4, r10, r11, pc}
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"Value is: %x\n"
	.size	.L.str, 14


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm 2b5c7ea7a610af4c0e9bb77143223b95c7b22f8b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

/////////////////////////////////////////////
// Annotations pour .PTM_getValueFromArray_0
/////////////////////////////////////////////
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_getValueFromArray_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	// Nombre d'annotations
	// --------------------
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	// Annotations
	// -----------------
 	//  Init R9 Nothing to do
	//.long	125829120 	//  Sending SP value [ (Mounir) Instr - SP Value ]
	//.long 534642696   //  sub	sp, sp, #20               |  SP <= SP -  #20      | tag_rri SP SP 8
	//.long 2344615952	// 	str	r0, [sp, #16]             |  [SP + 16] <- r0	    | tag_mem_reg SP t0 16
	//.long 2344681484	// 	str	r1, [sp, #12]             |  [SP + 12] <- r1	    | tag_mem_reg SP t1 12
	//.long 2344747016	// 	str	r2, [sp, #8]              |  [SP + 8] <- r2	      | tag_mem_reg SP t2 12
	//.long 2483150856	// 	ldr	r0, [sp, #8]              |  r0 <- [SP + 8]	      | tag_tr_mem NULL t0 SP 8
	.long 2174746624	// NOP (t13 = 0)
	//.long XXXX	//  cmp	r0, r1                    |  Flags CSPR <- r0 X r1	  |
	// 	bge	.LBB0_2, 	b	.LBB0_1  |  TODO: Jump (PC <- CPSR) ???	  |


/////////////////////////////////////////////
// Annotations pour .PTM_getValueFromArray_1
/////////////////////////////////////////////	
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_getValueFromArray_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	// Nombre d'annotations
	// --------------------
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	// Annotations
	// -----------------
	.section	.HB.annot,"a",%progbits
  .long 35651584 // Instr g2 0 0
	.long 3154317312 // @[g2] == t1 == 8 

  // 	movw	r0, #2            |  Nothing to do
	//.long	2483281936  	// 	ldr	r1, [sp, #16] |  r1 <= [SP + #16]     |	Tag_tr_mem t1 SP 16
	//.long	2483413000  	// 	ldr	r2, [sp, #8]        |  r2 <= [SP + #8]      |	Tag_tr_mem t2 SP 8
	//.long	2216820736  	// 	add	r1, r1, r2, lsl #2  |  r1 <= r1 ??? r2      | Tag_reg_reg t1 t2 0
	//.long 33554432       //  str r1, [r9]            |  Receive the pointer address of r1 to r9	 | INSTR g1
	//.long 513802244     // g6 = 4     | tag_rri g6 0 4
	//.long 3156299776    // Check tag g1 - g6 |  Tag_check_mem g1 g6
	//.long	2284847104  	//  ldr	r1, [r1]            |  r1 <= [r1]	           |  Tag_mem_reg r1 g1
	//.long 2419855364   	// 	str	r1, [sp, #4]        |  [SP + 4] <= r1	       |   Tag_mem_tr SP t1 4
	//.long	2486960128    // 	str	r0, [sp]            |  [SP] <= r0	           | 	Tag_mem_tr SP t0 0


/////////////////////////////////////////////
// Annotations pour .PTM_getValueFromArray_2
/////////////////////////////////////////////
	.section        .HB.bbt,"a",%progbits
	.long	.PTM_getValueFromArray_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	// Nombre d'annotations
	// --------------------
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	// Annotations
	// -----------------
	.section	.HB.annot,"a",%progbits
  .long 2174746624	// NOP (t13 = 0)  
  //	.long	2419851268      // str	r0, [sp, #4]  |  [SP + 4] <- r0     |  Tag_mem_tr SP t0 4


/////////////////////////////////////////////
// Annotations pour .PTM_getValueFromArray_3
  /////////////////////////////////////////////
	.section  .HB.bbt, "a", %progbits
	.long	.PTM_getValueFromArray_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	// Nombre d'annotations
	// --------------------
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	// Annotations
	// ------------
	.section	.HB.annot,"a",%progbits
  .long 2174746624	// NOP (t13 = 0)  
  .long 2174746624	// NOP (t13 = 0)  
  //.long	2483150852      // 	ldr	r0, [sp, #4]  |  r0 <- 	[SP + 4]   | Tag_tr_mem t0 SP 4
	//.long	534642708      // 	add	sp, sp, #20   |  SP = SP + 20	     | Tag_rri SP SP 20



/////////////////////////////////////////////
// Annotations pour .PTM_main_0
/////////////////////////////////////////////
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	// Nombre d'annotations
	// --------------------
	.long	72
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	// Annotations
	// ------------
	.section	.HB.annot,"a",%progbits
	//  Init R9, init atoi, init r3, init r12, init lr    |
	.long	62914560  //  Sending SP value [ (Mounir) Instr - SP Value ]
	//	.long	XXXX        // 	push	{r4, r10, r11, lr}  |  [SP] <- r4	          |
	//	.long	XXXX        // 	push	{r4, r10, r11, lr}  |  [SP + 4] <- r10      |
	//  .long	XXXX        // 	push	{r4, r10, r11, lr}  |  [SP + 8] <- r11	    |
	//  .long	XXXX        // 	push	{r4, r10, r11, lr}  |  [SP + 12] <- lr	    |
  //  .long	528351240     //  add	r11, sp, #8           |  r11 <- SP            |	tag_rri g11 SP 8
  //  .long	534675664     //	sub	sp, sp, #208          |  SP = SP - 208        | tag_rri SP SP-208
  .long   2149580808   // t1 = 8     | tag_reg_imm g6 0 8
  .long		2419855360   // [SP + 0] <- t1	     | Tag_mem_reg SP g6 0
  .long		2419855364   // [SP + 4] <- t1	     | Tag_mem_reg SP g6 4
  .long		2419855368   // ..
  .long		2419855372   // ..
  .long		2419855376   // ..
  .long		2419855380   // ..
  .long		2419855384   // ..
  .long		2419855388   // ..
  .long		2419855392   // ..
  .long		2419855396   // .. 10
  .long		2419855400   // ..
  .long		2419855404   // ..
  .long		2419855408   // ..
  .long		2419855412   // ..
  .long		2419855416   // ..
  .long		2419855420   // ..
  .long		2419855424   // ..
  .long		2419855428   // ..
  .long		2419855432   // ..
  .long		2419855436   // .. 20
  .long		2419855442   // ..
  .long		2419855446   // ..
  .long		2419855450   // ..
  .long		2419855454   // ..
  .long		2419855458   // ..
  .long		2419855462   // ..
  .long		2419855466   // ..
  .long		2419855470   // ..
  .long		2419855474   // ..
  .long		2419855478   // .. 30
  .long		2419855482   // ..
  .long		2419855486   // ..
  .long		2419855490   // ..
  .long		2419855494   // ..
  .long		2419855498   // ..
  .long		2419855502   // ..
  .long		2419855506   // ..
  .long		2419855510   // ..
  .long		2419855514   // ..
  .long		2419855518   // .. 40
  .long		2419855522 	// ..
  .long		2419855526 	// ..
  .long		2419855530 	// ..
  .long		2419855534 	// ..
  .long		2419855538 	// ..
  .long		2419855542 	// ..
  .long		2419855546 	// ..
  .long		2419855550 	// ..
  .long		2419855554 	// ..
  .long		2419855558 	// 50
  .long		2419855562 	// ..
  .long		2419855566 	// ..
  .long   2162229247 // TESTS OPERATORS t7 = 0101010101010101
  .long   2164304554 // TESTS OPERATORS t8 = 0000000011111111
  .long   2349237120 // TESTS OPERATORS
  .long   2353431424 // TESTS OPERATORS
  .long   2357625728 // TESTS OPERATORS
  .long   2361820032 // TESTS OPERATORS
  .long   2366014336 // TESTS OPERATORS
  .long   2370208640 // TESTS OPERATORS
  .long   2374402944 // TESTS OPERATORS
  .long   2378597248 // TESTS OPERATORS // 60 ...
  .long   2382791552 // TESTS OPERATORS
  .long   2386985856 // TESTS OPERATORS
  .long   2391180160 // TESTS OPERATORS
  .long   2395374464 // TESTS OPERATORS
  .long   2399568768 // TESTS OPERATORS
  .long   2403763072 // TESTS OPERATORS
  .long   2407957376 // TESTS OPERATORS
  .long   2412151680 // TESTS OPERATORS // 68..

  //.long	2483281936  // 	ldr	r1, [sp, #16]         |  r1 <= [SP + #16]     |	Tag_tr_mem t1 SP 16
  //.long	XXXX      // 	add	r4, sp, #32           |  r4 <- sp + 32	      |
  //.long 56623104    //  str r11, [r9]             |  Instrumentation      | INSTR g11 null 0
  //.long	190873612   // 	str	r0, [r11, #-12]       |  [r11 - 12] <- r0	    | SW g11 t0 -12
  //.long	190939152   // 	str	r1, [r11, #-16]       |  [r11 - 16] <- r1	    | SW g11 t1 -16
  //.long	2214854656  // 	mov	r0, r4	              |  r0 <- r4	            | Tag_reg_reg t0 t4 0
  //.long	2217476096  //  and	r1, r12, #255         |  r1 <- r12	          |	Tag_reg_reg t1 t12 0
  //.long	197263380   // 	str	r2, [sp, #20]         |  [SP + 20] <- r2	    | SW SP t2 20
  //.long	2219704320  // 	mov	r2, lr	       	      |  r2 <- lr	            |	Tag_reg_reg t2 t14 0
  //.long	3011        // 	str	r3, [sp, #16]	 	      |  [SP + 16] <- r3	    | SW SP t3 16

/////////////////////////////////////////////
// Annotations pour .PTM_main_1
/////////////////////////////////////////////
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp5:
   // Nombre d'annotations
	// --------------------
	.long	5
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)

	// Annotations
	// ------------
	.section	.HB.annot,"a",%progbits
	//.long	2483150864  // 	ldr	r0, [sp, #16] 	  |  r0 <- 	[SP + 16]	     | Tag_tr_mem t0 SP 16
	//.long	2419851296  //  str	r0, [sp, #32] 	  |  [SP + 32] <- r0	     | Tag_mem_tr SP t0 32
	//.long	2483271696  //	ldr	r1, [r11, #-16]	  |  r1 <- 	[r11 + -16]	   | Tag_tr_mem t1 g11 -16
	//.long	2483093508  //	ldr	r0, [r1, #4]	    |  r0 <- 	[r1 + 4]	     | Tag_tr_mem t0 g1 4
	//.long	2483281940  //	ldr	r1, [sp, #20]	    |  r1 <- 	[SP + 20]	     | Tag_tr_mem t1 SP 20
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)


  /////////////////////////////////////////////
  // Annotations pour .PTM_main_2
  /////////////////////////////////////////////
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp6:
  // Nombre d'annotations
	// --------------------
	.long	6
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)

	// Annotations
	// ------------
	.section	.HB.annot,"a",%progbits
	//.long	2219769856  //	add	r2, sp, #32     |  r2 <- sp                     | Tag_reg_reg t2 t15
  //.long	2419851292  //	str	r0, [sp, #28]	  |  [SP + 28] <- r0	            | Tag_mem_tr SP t0 28
  //.long	2483150876  //	ldr	r0, [sp, #28]	  |  r0 <- 	[SP + 28]	            |	Tag_tr_mem t0 SP 28
  //.long	2419851276  //	str	r0, [sp, #12]	  |  [SP + 12] <- r0	            |	Tag_mem_tr SP t0 12
  //.long	2214723584  //	mov	r0, r2          |  r0 <- r2	                    |	Tag_reg_reg t0 t2
  //.long	2483093508  //	ldr	r2, [sp, #12]   |  r0 <- 	[r1 + 4]	            | Tag_tr_mem t0 g1 4
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)



/////////////////////////////////////////////
// Annotations pour .PTM_main_3
/////////////////////////////////////////////
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp7:
        // Nombre d'annotations
	// --------------------
	.long	5
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)

	// Annotations
	// ------------
	.section	.HB.annot,"a",%progbits
  //	.long	2419851288   //	str	r0, [sp, #24]	        |  [SP + 24] <- r0	     |  	Tag_mem_tr SP t0 24
  // .long	2483150872   //	ldr	r0, [sp, #24] 	|  r0 <- 	[SP + 24]	     |  Tag_tr_mem t0 SP 24
  //  .long	2419851272   //	str	r0, [sp, #8] 	 |  [SP + 8] <- r0	       | 	Tag_mem_tr SP t0 8
  //.long	2214658048   //	mov	r0, r1          |  r0 <- r1	             | Tag_reg_reg t0 t1 0
  // .long	2952912904   //	ldr	r1, [sp, #8] 	  |  r0 <- 	[SP + 8]	     | 	Tag_tr_mem t0 SP 8
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)


/////////////////////////////////////////////
// Annotations pour .PTM_main_4
/////////////////////////////////////////////
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp8:
  // Nombre d'annotations
	// --------------------
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits

	// Annotations
	// ------------
//	.long 2419851268    //	str	r0, [sp, #4]	      |  [SP + 4] <- r0           | Tag_mem_tr SP t0 4
//  .long	2214658048    //	mov	r0, r1	            |  r0 <- r1                 | Tag_reg_reg t0 t1
//  .long	534478856     //	sub	sp, r11, #8	        |  SP = R11 - 8             | 	Tag_rri SP g11 -8
//	.long	XXXX        //	pop	{r4, r10, r11, pc}	|  r4  <- 	[SP]	          |
//	.long	XXXX        //	pop	{r4, r10, r11, pc}	|  r10 <- 	[SP + 4]	      |
//  .long	XXXX        //	pop	{r4, r10, r11, pc}	|  r11 <- 	[SP + 8]	      |
//  .long	XXXX        //	pop	{r4, r10, r11, pc}	|  pc  <- 	[SP + 12]	      |
	//.long	534642704     //	pop	{r4, r10, r11, pc}	|  SP = SP + 16	          |	  	Tag_rri SP SP 16

	.long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)
  .long 2174746624	// NOP (t13 = 0)

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
