################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer_g.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer_selftest.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer_sinit.c 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer_g.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer_selftest.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer_sinit.o 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer_g.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer_selftest.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/xscutimer_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scutimer_v2_0/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


