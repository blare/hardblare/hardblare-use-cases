#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 42

void app_nbody()
{
	static unsigned int init = 0;

	unsigned int i, j;
	double ax, ay, az;
	double dx, dy, dz;
	double invr, invr3;
	double f;
	double eps = 0.001;
	double dt = 0.001;

	static double x[N], y[N], z[N], m[N];
	static double xnew[N], ynew[N], znew[N];
	static double vx[N], vy[N], vz[N];

	if ( ! init )
	{
		srand(123333314);

		for ( i = 0; i < N; i++)
		{
			x[i]		= rand()/((double)RAND_MAX) * (1.0+1.0) -1.0;
			y[i]		= rand()/((double)RAND_MAX) * (1.0+1.0) -1.0;
			z[i]		= rand()/((double)RAND_MAX) * (1.0+1.0) -1.0;
			m[i]		= rand()/((double)RAND_MAX) * (1.0+1.0) -1.0;
			xnew[i]		= 0.0;
			ynew[i]		= 0.0;
			znew[i]		= 0.0;
			vx[i]		= 0.0;
			vy[i]		= 0.0;
			vz[i]		= 0.0;
		}
		init = 1;
	}

	for(i=0; i<N; i++)
	{
		ax=0.0;
		ay=0.0;
		az=0.0;

		for(j=0; j<N; j++)
		{
			dx=x[j]-x[i];
			dy=y[j]-y[i];
			dz=z[j]-z[i];
			invr = 1.0/sqrt(dx*dx + dy*dy + dz*dz + eps);
			invr3 = invr*invr*invr;
			f=m[j]*invr3;
			ax += f*dx;
			ay += f*dy;
			az += f*dx;
		}
		xnew[i] = x[i] + dt*vx[i] + 0.5*dt*dt*ax;
		ynew[i] = y[i] + dt*vy[i] + 0.5*dt*dt*ay;
		znew[i] = z[i] + dt*vz[i] + 0.5*dt*dt*az;
		vx[i] += dt*ax;
		vy[i] += dt*ay;
		vz[i] += dt*az;
	}

	for(i=0;i<N;i++)
	{
		x[i] = xnew[i];
		y[i] = ynew[i];
		z[i] = znew[i];
		//printf("%f %f %f\n", x[i], y[i], z[i]);
	}

}
