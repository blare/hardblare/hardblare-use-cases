#include <stdio.h>
#include <stdlib.h>
#include "benchmark.h"
#include "xtime_l.h"
#include "xil_types.h"
#include "xil_assert.h"
#include "xparameters.h"
#include "xil_printf.h"
#include <xil_io.h>


//#define CHOLESKI
//#define DFT
//#define FFT
//#define FIR
//#define LU
//#define MATRIX
#define RADIX
//#define WHT
//#define NBODY
//#define CRC


#define NB_REGIONS 		8 /**< MAXIMUM NUMBER OF REGIONS TO INCLUDE/EXCLUDE */
#define SYNC_FREQ		0X60 /**< Frequency for ETMSYNCFR */

int config_ptm_regions(unsigned int , unsigned int , unsigned int *, unsigned int *, unsigned int);
int config_ptm(unsigned int baseaddr);

#define TIMING

#ifdef TIMING
XTime t1,t2;
int EffectiveClockRate;
int Calibration;

#define TIMING_START    XTime_GetTime(&t1)
#define TIMING_STOP     XTime_GetTime(&t2)
#define TIMING_DISPLAY(txt) DisplayTime(txt,t2-t1-Calibration)
#define TIMING_INIT 	InitTiming()
#define NB_TIMES 100

void InitTiming()
{
	/*Calculate Global Timer Counter Consecutive Read overhead*/
	EffectiveClockRate = XPAR_CPU_CORTEXA9_0_CPU_CLK_FREQ_HZ / 2;
	XTime_GetTime(&t1);
	XTime_GetTime(&t2);
	Calibration = t2-t1;

}
void DisplayTime(char *txt,int Nclk)
{
	double Time_s = (double)Nclk/(double)EffectiveClockRate/(double)NB_TIMES;
	double Time_millis = 1e3*Time_s;
	double Time_micros = 1e6*Time_s;
	int MilS_int = (int)Time_millis;
	int MilS_frac = (int)((Time_millis-MilS_int)*1000.0);
	int MicS_int = (int)Time_micros;
	int MicS_frac = (int)((Time_micros-MicS_int)*1000.0);

	xil_printf("---------------------------------------------------------\n\r");
	xil_printf(txt);
	xil_printf("Number of Processor Clock Cycles : %d in %d iterations \n\r",2*Nclk, NB_TIMES);
	xil_printf("Mean Time duration: %d.%03d ms\n\r",MilS_int,MilS_frac);
	xil_printf("Mean Time duration: %d.%03d us\n\r",MicS_int,MicS_frac);
}

#else
#define TIMING_START
#define TIMING_STOP
#define TIMING_DISPLAY(txt)
#define TIMING_INIT

#endif

/*************** MEMORY MAP FOR CORESIGHT COMPONENTS IN ZYNQ ***************/
/**
 * Memory map of coresight components : baseaddresses depends on HW, the following addresses are for ZYNQ
 */
#define ETB_BASEADDR					0xF8801000
#define TPIU_BASEADDR					0xF8803000
#define FUNNEL_BASEADDR					0xF8804000
#define ITM_BASEADDR					0xF8805000
#define PTM_BASEADDR 					0xF889C000
#define PTM_1_BASEADDR 					0xF889D000

/***************************** PTM REGISTERS *****************************/
#define OFFSET_ETMCR 					0x000 		/**< Main control register                    */
#define OFFSET_ETMCCR					0x004		/**< Configuration code register              */
#define OFFSET_ETMTRIGGER 				0x008		/**< Trigger event register                   */
#define OFFSET_ETMSR    				0x010		/**< Status register                          */
#define OFFSET_ETMSCR					0x014		/**< System configuration register            */
#define OFFSET_ETMTSSCR					0x018		/**< TraceEnable Start/Stop Control Register  */
#define OFFSET_ETMTEEVR 				0x020		/**< TraceEnable event register               */
#define OFFSET_ETMTECR1					0x024		/**< TraceEnable Control Register             */
#define OFFSET_ETMSYNCFR				0x1E0		/**< Synchronization frequency register       */
#define OFFSET_ETMIDR					0x1E4		/**< ID register                              */
#define OFFSET_ETMCCER					0x1E8		/**< Configuration code extension register    */
#define OFFSET_ETMTRACEID				0x200		/**< trace id register                        */

/******************** Counters ********************/
#define CS_ETMCNTRLDVR(n)  		(0x140 + 4*(n))   	/**< Counter Reload Value Register (n) 		*/
#define CS_ETMCNTENR(n)    		(0x150 + 4*(n))   	/**< Counter Enable Register (n) 		 		*/
#define CS_ETMCNTRLDEVR(n) 		(0x160 + 4*(n))   	/**< Counter Reload Event Register (n) 		*/
#define CS_ETMCNTVR(n)     		(0x170 + 4*(n))   	/**< Counter Value Register (n)		 		*/

/******************** Sequencer *********************/
/** Test sequence transition 'a'->'b' as value against 'n' */
#define CS_SF(a,b,n) ((((a) << 4) | (b)) == n)

/** Calculate the event register offset for transition 'a'->'b' (uses #CS_SF macro) */
#define CS_ETMSQOFF(a,b) (CS_SF(a,b,0x12) ? 0 : CS_SF(a,b,0x21) ? 1 : CS_SF(a,b,0x23) ? 2 : CS_SF(a,b,0x31) ? 3 : CS_SF(a,b,0x32) ? 4 : CS_SF(a,b,0x13) ? 5 : -1)

#define CS_ETMSQEVRRAW(n) (0x180 + 4*(n))   /**< Sequencer Transition event register (n) [n=0-5] - get address offset by index */
#define CS_ETMSQEVR(a,b)  (0x180 + 4*CS_ETMSQOFF(a,b))  /**< Sequencer Transition event register for transition 'a'->'b' - get address offset by transition */
#define CS_ETMSEQ_STATES 3      /**< Number of sequencer states */
#define CS_ETMSEQ_TRANSITIONS (CS_ETMSEQ_STATES * (CS_ETMSEQ_STATES-1)) /**< Number of sequencer transitions */

#define CS_ETMSQ12EVR     0x180     /**< Sequencer Transition Event Register: transition state 1->2 */
#define CS_ETMSQ21EVR     0x184     /**< Sequencer Transition Event Register: transition state 2->1 */
#define CS_ETMSQ23EVR     0x188     /**< Sequencer Transition Event Register: transition state 2->3 */
#define CS_ETMSQ31EVR     0x18C     /**< Sequencer Transition Event Register: transition state 3->1 */
#define CS_ETMSQ32EVR     0x190     /**< Sequencer Transition Event Register: transition state 3->2 */
#define CS_ETMSQ13EVR     0x194     /**< Sequencer Transition Event Register: transition state 1->3 */
#define CS_ETMSQR         0x19C     /**< Sequencer State Register */

/**
 * Address Comparators
 */
#define CS_ETMACVR(n)    (0x040 + 4*(n))  /**< Address Comparator Value register (n) */
#define CS_ETMACTR(n)    (0x080 + 4*(n))  /**< Address Comparator Type register (n) */


/* ETM Event resources.
 * Event resource selectors consist of a seven bit field: 'type'[6:4] and 'index'[3:0]. These
 * resource selectors are then used in the #CS_ETMEVENT macros as 'a' or 'b' resources to create the
 * event descriptor.
 *
 * Due to the way events are encoded, an event resource
 * value can also be interpreted as the corresponding event, i.e.
 *   CS_ETMER_XXX == #CS_ETME_WHEN(CS_ETMER_XXX)
 *
 * Sequencer states are numbered 1..3 as in the ETM architecture.
 *
 * Comparators, external inputs etc. are numbered from 0, contrary to
 * the ETM architecture.
 */
#define CS_ETMER_SAC(n) ((n) + 0x00)    	/**< Single address comparator 1-16 [n=0-15] */
#define CS_ETMER_RANGE(n) ((n) + 0x10)  	/**< Address comparator range 1-8 [n=0-7] */
#define CS_ETMER_INST(n) ((n) + 0x18)   	/**< Instrumentation resource 1-4 [n=0-3] (implementation defined) */
#define CS_ETMER_EICE(n) ((n) + 0x20)   	/**< EICE watchpoint comparators 1-8 [n=0-7] (implementation defined) */
#define CS_ETMER_MMAPDEC(n) ((n) + 0x30)    /**< Memory map decodes 1-16 [n=0-15] */
#define CS_ETMER_CZERO(n) ((n) + 0x40)      /**< Counter 1-4 at zero [n=0-3] */
#define CS_ETMER_SEQSTATE(n) (((n) - 1) + 0x50) /**< Sequencer states 1-3 [n=1-3] */
#define CS_ETMER_CXID(n) ((n) + 0x58)       /**< Context ID comparator 1-3 [n=0-2] */
#define CS_ETMER_VMID(n) ((n) + 0x5B)       /**< VMID comparator [n=0] */
#define CS_ETMER_TRENABLE 0x5F              /**< Trace start/stop resource */
#define CS_ETMER_EXTIN(n) ((n) + 0x60)      /**< EXTIN 1-4 [n=0-3]  */
#define CS_ETMER_EXTEXTIN(n) ((n) + 0x68)   /**< Extended EXTIN 1-4 [n=0-3] */
#define CS_ETMER_NS 0x6D                    /**< Processor in Non-secure state */
#define CS_ETMER_TPROH 0x6E                 /**< Trace prohibited by procesor */
#define CS_ETMER_ALWAYS 0x6F                /**< Hardwired always TRUE resource input */
/** Never event. Note that "NEVER" is an event, but not an event resource. */
#define CS_ETME_NEVER CS_ETME_NOT(CS_ETMER_ALWAYS)


/************************* CORESIGHT MANAGEMENT REGISTERS for PTM *************************/
/* Register Number * 4 gives the offset 	*/
#define OFFSET_ETMLSR 					(1005*4)	/**< Lock status register (0xFB0) */
#define OFFSET_ETMLAR 					(0x3EC*4)   /**< Lock Access register (0xFB4) */

/**
 * FUNNEL REGISTERS
 */
#define OFFSET_CSTF_CTL					0x000 		/**< configuration control register */
#define OFFSET_CSTF_LAR					(0x3EC*4)   /**< Lock Access register (0xFB4)   */

/***************************** ETB REGISTERS *****************************/
#define OFFSET_ETB_ID 					0x000		/**< RO                                    */
#define OFFSET_ETB_RAM_DEPTH			0x004		/**< RO                                    */
#define OFFSET_ETB_RAM_WIDTH			0x008		/**< RO                                    */
#define OFFSET_ETB_STATUS				0x00C		/**< RO                                    */
#define OFFSET_ETBRRD 					0x010		/**< Ram Read data register                */
#define OFFSET_ETB_RAM_READ_POINTER		0x014		/**< RW                                    */
#define OFFSET_ETB_RAM_WRITE_POINTER	0x018		/**< RW                                    */
#define OFFSET_ETB_TRIGGER_COUNT		0x01C		/**< RW                                    */
#define OFFSET_ETB_CONTROL				0x020		/**< RW                                    */
#define OFFSET_ETBFFSR					0x300		/**< Formatter and Flush Status register   */
#define OFFSET_ETBFFCR					0x304		/**< Formatter and Flush Control register  */

/** BitFields for Flush and Format Control Register **/
#define CS_ETB_FLFMT_CTRL_StopTrig  	0x2000 /**< Stop formatter when trigger event observed */
#define CS_ETB_FLFMT_CTRL_StopFl    	0x1000 /**< Stop formatter when flush complete */
#define CS_ETB_FLFMT_CTRL_TrigFl    	0x0400 /**< Indicate a trigger on Flush completion */
#define CS_ETB_FLFMT_CTRL_TrigEvt   	0x0200 /**< Indicate a trigger on a trigger event */
#define CS_ETB_FLFMT_CTRL_TrigIn    	0x0100 /**< Indicate a trigger on TRIGIN being asserted */
#define CS_ETB_FLFMT_CTRL_FOnMan    	0x0040 /**< Flush on Manual */
#define CS_ETB_FLFMT_CTRL_FOnTrig   	0x0020 /**< Flush on Trigger */
#define CS_ETB_FLFMT_CTRL_FOnFlIn   	0x0010 /**< Flush on Flush in */
#define CS_ETB_FLFMT_CTRL_EnFCont   	0x0002  /**< Continuous Formatting */
#define CS_ETB_FLFMT_CTRL_EnFTC     	0x0001  /**< Enable Formatting */


/************************* CORESIGHT MANAGEMENT REGISTERS *************************/
#define OFFSET_ETBLSR					0xFB4		/**< RO */
#define OFFSET_ETBLAR					0xFB0		/**< RW */

//#define OFFSET_ETB_ALIASED_TRACE_RAM	0x100000

/************************************** TPIU **************************************/
/**
 *  TPIU REGISTERS
 */
#define OFFSET_TPIU_SUPPORTED_PORT_SIZE						0x000
#define OFFSET_TPIU_CURRENT_PORT_SIZE						0X004
#define OFFSET_TPIU_SUPPORTED_TRIGGER_MODES 				0X100
#define OFFSET_TPIU_TRIGGER_COUNTER_VALUE					0X104
#define OFFSET_TPIU_TRIGGER_MULTIPLIER						0X108
#define OFFSET_TPIU_SUPPORTED_TEST_PATTERN					0X200
#define OFFSET_TPIU_CURRENT_TEST_PATTERN					0X204
#define OFFSET_TPIU_TEST_PATTERN_REPEAT_COUNTER				0X208
#define OFFSET_TPIU_FORMATTER_AND_FLUSH_STATUS				0X300
#define OFFSET_TPIU_FORMATTER_AND_FLUSH_CONTROL				0X304
#define OFFSET_TPIU_FORMATTER_SYNCHRONIZATION_COUNTER		0X308
#define OFFSET_TPIU_LSR										0xFB4
#define OFFSET_TPIU_LAR										0xFB0


/************************************ FUNCTIONS ************************************/
/**
 * \brief 		Set value in a register
 * \param[in]  	baseaddr 	Base address for PTM component
 * \param[in]	offset		Offset allows to choose the register we want
 * \param[in]	value		The value which must be set in the register
 */
void set_register_value(unsigned int baseaddr, unsigned int offset, unsigned int value)
{
	Xil_Out32(baseaddr + offset, value);
}

/**
 * \brief 		Get value from a register
 * \param[in]  	baseaddr 	Base address for PTM component
 * \param[in]	offset		Offset allows to choose the register we want
 * \return 					The value read in the register located at address (baseaddr+offset)
 */
int get_register_value(unsigned int baseaddr, unsigned int offset)
{
	int temp = 0;
	temp = Xil_In32(baseaddr + offset);
	return (temp);
}

/**
 * \brief 					Prints values of different PTM configuration registers
 * \param[in]  	baseaddr 	Base address for PTM component
 */
void show_etm_configuration(unsigned int baseaddr)
{
	int r;
	/************************ STATIC **********************/
	printf("PTM Static Configuration \n");

	r = get_register_value(baseaddr,OFFSET_ETMCCR);
	printf("\t ETMCCR = 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETMCCER);
	printf("\t ETMCCER = 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETMSCR);
	printf("\t ETMSCR = 0x%08x\n",r);

	r = get_register_value(baseaddr, OFFSET_ETMIDR);
	printf("\t ETMIDR = %08x\n", r);

	/************************ DYNAMIC **********************/
	printf("PTM Dynamic Configuration \n");

	r = get_register_value(baseaddr,OFFSET_ETMCR);
	printf("\t ETMCR = 0x%08x\n",r);

	printf("  Cycle accurate: %s\n", (r>>12)&1?"true":"false");
	printf("  Branch output: %s\n", (r>>8)&1?"true":"false");
	printf("  Timestamp enabled: %s\n", (r>>28)&1?"true":"false");
	printf("  CONTEXTID size: %u bytes\n", (r>>15&1)<<1|(r>>14&1));

	r = get_register_value(baseaddr,OFFSET_ETMSR);
	printf("\t ETMSR = 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETMTECR1);
	printf("\t Trace enable control: ETMECR1 = 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETMTSSCR);
	printf("\t ETMTSSCR = 0x%08x\n",r);
	printf("\t Trace start comparators: %04x\n",r&0xFFFF);
	printf("\t Trace stop comparators: %04x\n",(r>>16));

	r = get_register_value(baseaddr,OFFSET_ETMTRIGGER);
	printf("ETMTRIGGER \t 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETMTEEVR);
	printf("ETMTEEVR \t 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETMTRACEID);
	printf("ETMTRACEID \t 0x%08x\n\n\n",r);

}

/****************************** ENABLE PTM PROGRAMMING ******************************/
/**
 * \brief					Enable PTM programmation
 * \details					PTM enabling is done by
 * 								Unlocking PTM by writing 0xC5ACCE55 in the ETMLAR regiser
 * 								Clearing Powerdown bit
 * \param[in]  	baseaddr 	Base address for PTM component
 * \return 				If everything goes right, the function returns 0 otherwise the function returns -1 indicating an error. Please look at the stderr for more information about the error.
 */
int enable_programming_ptm(unsigned int baseaddr)
{
	int r, i;

	/************************ UNLOCKING PTM FOR WRITE **********************/
	// Read Lock Status register to know if it is necessary to unlock
	r = get_register_value(baseaddr,OFFSET_ETMLSR);

	if ((r>>1)&1)
	{
		printf("Unlocking PTM ! \n");

		//unlock the PTM by writing 0xC5ACCE55 to the ETMLAR (OFFSET)
		set_register_value(baseaddr,OFFSET_ETMLAR,0xC5ACCE55);

		r = get_register_value(baseaddr,OFFSET_ETMLSR);
		if ( ~(r>>1)&1 )
		{
			printf("PTM UNLOCKED! \n");
			printf("AFTER UNLOCKING : ETMLSR \t\t 0x%08x\n",r);
		}
		else
		{
			fprintf(stderr, "PTM COULD NOT BE UNLOCKED \n");
			return -1;
		}
	}

	/************************ POWERDOWN BIT **********************/
	r = get_register_value(baseaddr,OFFSET_ETMCR);
	if (r&1)
	{
		/* ETM power down. "When this bit is set to 1, writes to some registers
		         and fields might be ignored." */
		/* Unset power-down state */
		set_register_value(baseaddr,OFFSET_ETMCR,r&(~1));
		for (i = 0; i < 32; i++)
		{
			r = get_register_value(baseaddr,OFFSET_ETMCR);
			if (r&1)
			{
				set_register_value(PTM_BASEADDR,OFFSET_ETMCR,r&(~1));
				fprintf(stderr,"Powerdown bit still set after %d iterations\n",i);
			}
			else
			{
				fprintf(stderr,"Powerdown bit clear after %d iterations\n",i);
				break;
			}
		}
	}

	/************************ VERIFY PROGRAMMING BIT **********************/
	// Read Status Register
	r = get_register_value(baseaddr,OFFSET_ETMSR);

	// bit 1 of status register
	if ( (r>>1)&0x1 )
	{
		printf("Starting Programming PTM Registers\n");
	}
	else
	{
		// Status bit not set means that programming bit is not set => wait until status bits equals '1'
		while ( ((r>>1)&0x1) != 0)
		{
			printf("Waiting for Programming bit (10th bit of ETMCR) to be equal to 1\n");
			r = get_register_value(baseaddr,OFFSET_ETMCR);
		}
	}

	return 0;
}

/****************************** DISABLE PTM PROGRAMMING ******************************/
/**
 * \brief					Enable PTM programmation
 * \details					\b THE PTM MUST BE DISABLED BEFORE THE TRACES CAN BE RECOVERED
 * \param[in]  	baseaddr 	Base address for PTM component
 * \return 					If everything goes right, the function returns 0.
 */

int disable_programming_ptm(unsigned int baseaddr)
{
	int r, val;
	/** CLEAR PROGRAMMING BIT PTM**/
	r = get_register_value(baseaddr,OFFSET_ETMCR);
	val = r&(~(1<<10));
	// Clear Programming bit (10) in Main Control Register Value
	set_register_value(baseaddr,OFFSET_ETMCR,val);

	// Read Status Register Value
	r = get_register_value(baseaddr,OFFSET_ETMSR);

	while ( ((r>>1)&0x1) != 0)
	{
		r = get_register_value(baseaddr,OFFSET_ETMSR);
	}

	return 0;
}

/**
@TODO: 	NEEDS TO SET THE DEPTH OF THE READ POINTER REGISTER
		STOP THE TRACE PROPERLY
 **/
/**
 * \brief					Program Funnel
 * \param[in]  	baseaddr 	Base address for PTM component
 */
void program_funnel(unsigned int baseaddr)
{
	int r;
	/************************ PROGRAMMING FUNNEL **********************/

	//printf("PROGRAMMING FUNNEL \n");

	set_register_value(baseaddr,OFFSET_CSTF_LAR,0xC5ACCE55);

	r = get_register_value(baseaddr,0);
	//printf("Funnel configuration REGISTER \t 0x%08x\n",r);

	set_register_value(baseaddr,0,r|1);
	r = get_register_value(baseaddr,0);
	//printf("AF: Funnel configuration REGISTER \t 0x%08x\n",r);

	set_register_value(baseaddr,OFFSET_CSTF_LAR,0);
}

/**
 * \brief					Prints values of ETB configuration registers
 * \param[in]  	baseaddr 	Base address for PTM component
 */
void show_etb_configuration(unsigned int baseaddr)
{
	int r;
	printf("\t ETB configuration \n");

	r = get_register_value(baseaddr,0x04);
	printf("ETB_RAM_DEPTH_REGISTER \t 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETBLSR);
	printf("ETB_LOCK_STATUS_REGISTER \t 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETB_STATUS);
	printf("ETB_STATUS \t 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETBFFSR);
	printf("ETBFFSR \t 0x%08x\n",r);
	r = get_register_value(baseaddr,OFFSET_ETBFFCR);
	printf("ETBFFCR \t 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETB_TRIGGER_COUNT);
	printf("ETB_TRIGGER_COUNT \t 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETB_RAM_WRITE_POINTER);
	printf("OFFSET_ETB_RAM_WRITE_POINTER \t 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETB_RAM_READ_POINTER);
	printf("OFFSET_ETB_RAM_READ_POINTER \t 0x%08x\n",r);

	r = get_register_value(baseaddr,OFFSET_ETB_RAM_WRITE_POINTER);
	printf("OFFSET_ETB_RAM_WRITE_POINTER \t 0x%08x\n",r);
}

/**
 * \brief					Allows to determine if buffer has wrapped
 * \param[in]  	baseaddr 	Base address for PTM component
 */
int buffer_has_wrapped(unsigned int baseaddr)
{
	int r;
	r = get_register_value(baseaddr,OFFSET_ETB_STATUS);

	if (r&1)
	{
		return 1;
	}
	else
		return 0;
}

/**
 * \brief					Program ETB (Embedded Trace Buffer)
 * \param[in]  	baseaddr 	Base address for PTM component
 * \return					The function returns 0 if successful otherwise -1. Please look at stderr for more information on the error.
 */
int program_ETB(unsigned int baseaddr)
{
	int r;
	/************************ PROGRAMMING ETB **********************/
	//unlock the ETB by writing 0xC5ACCE55 to the ETMLAR (OFFSET)
	set_register_value(baseaddr,OFFSET_ETBLAR,0xC5ACCE55);
	r = get_register_value(baseaddr,OFFSET_ETBLSR);
	if (r>>1 == 0)
	{
		printf("ETB Unlocked !\n");
	}
	else
	{
		fprintf(stderr, "ETB Still Locked \n");
		return -1;
	}

	r = get_register_value(baseaddr,OFFSET_ETB_RAM_WRITE_POINTER);
	printf("OFFSET_ETB_RAM_WRITE_POINTER \t 0x%08x\n",r);

	// ETB Trace Capture Enable
	set_register_value(baseaddr,OFFSET_ETB_CONTROL,1);

	// reset the full flag (ETB).
	if (buffer_has_wrapped(baseaddr)==1) {
		/* There appears to be no direct way of resetting the Full (wrapped) flag.
       The only way to reset it is as a side-effect of enabling trace
       capture.  Even if we try to do that briefly, we risk wrapping
       again if the process is pre-empted at an inopportune time, so we
       may need to take more than one attempt. */
		unsigned int retries = 0;
		/* We've observed that when StopTrig is enabled, the action of briefly
       enabling trace doesn't result in the Full (wrapped) indicator being reset.
       So, temporarily disable StopTrig. */
		unsigned int flc = get_register_value(baseaddr, OFFSET_ETBFFCR);
		if (flc & (CS_ETB_FLFMT_CTRL_StopTrig | CS_ETB_FLFMT_CTRL_StopFl)) {
			set_register_value(baseaddr, OFFSET_ETBFFCR, flc & ~(CS_ETB_FLFMT_CTRL_StopTrig | CS_ETB_FLFMT_CTRL_StopFl));
		}
		do {
			++retries;
			if (retries > 3) {
				unsigned int status = get_register_value(baseaddr, OFFSET_ETB_STATUS);
				unsigned int flstat = get_register_value(baseaddr, OFFSET_ETBFFSR);
				return -1;
			}
			/* Set the write pointer to the start as we don't want to wrap again */
			set_register_value(baseaddr, OFFSET_ETB_RAM_WRITE_POINTER, 0x00000000);
			set_register_value(baseaddr, OFFSET_ETB_CONTROL, 1);
			/* We're now capturing trace, hopefully briefly. */
			set_register_value(baseaddr, OFFSET_ETB_CONTROL, 0);
			/* We may have moved the write pointer a little bit.  We'd be very
         unlucky to have wrapped again, unless we were suspended while
         the trace was enabled. */
		} while (buffer_has_wrapped(baseaddr)==1);

		if (flc & CS_ETB_FLFMT_CTRL_StopTrig) {
			set_register_value(baseaddr, OFFSET_ETBFFCR, flc);
		}
	}

	r = get_register_value(baseaddr,OFFSET_ETB_RAM_WRITE_POINTER);
	//printf("OFFSET_ETB_RAM_WRITE_POINTER \t 0x%08x\n",r);

	set_register_value(baseaddr,OFFSET_ETB_RAM_WRITE_POINTER,0x00000000);
	/* We might as well program the read pointer here as an indicator that
     we aren't part-way through a buffer read.  But when we do read out,
     we need to write the read pointer again to trigger a RAM access. */
	set_register_value(baseaddr,OFFSET_ETB_RAM_READ_POINTER,0x00000000);

	// BYPASS MODE (Bit 0 and Bit 1) equals 0 => BYPASS
	// NORMAL MODE ('10' => bit 1 et bit 0)
	// CONTINUOUS MODE Both bits (0 and 1) equals '1'
	r = get_register_value(baseaddr,OFFSET_ETB_STATUS);
	while ( ((r>>2)&1) != 0 )
	{
		printf("Waiting for Acqcompbit \n");
	}
	return 0;
}

/**
 * \brief					Stop the trace collection in ETB
 * \param[in]  	baseaddr 	Base address for PTM component
 * \return 					0 if successful.
 */
int etb_stop_tracing(unsigned int baseaddr)
{
	int r;
	/* "Disable trace capture" */
	set_register_value(baseaddr,OFFSET_ETB_CONTROL,0);
	r = get_register_value(baseaddr,OFFSET_ETB_STATUS);
	/* Wait for formatter to flush */
	//printf("ETB_STATUS \t 0x%08x\n",r);
	while ( ((r>>3)&1) == 0)
	{
		printf("Waiting for DFEmpty \n");
	}

	/* After FtEmpty: "Formatter pipeline is empty. All data is stored to RAM." */
	/* "Capture is fully disabled, or complete, when FtStopped goes high" */

	r = get_register_value(baseaddr,OFFSET_ETBFFSR);
	while ( ((r>>1)&1) == 0)
	{
		printf("Waiting for FtStopped \n");
	}
	return 0;
}

/**
 * \brief					Read the ETB to collect trace
 * \details 				Reading the ETBRRD (ETB RAM read data) register automatically increments the RAM read pointer register
 * 							The trace is sent by the PTM only if 32 bits of trace are generated
 * 							The LSB should be printed first because they are collected before the MSB
 * \param[in]  	baseaddr 	Base address for PTM component
 * \param[in]	n			The number of bytes to read in the RAM read data register
 * \return 					The function returns 0 if successful.
 * \todo 					Automatically read all the trace in the buffer which means that the argument \a n will not be needed anymore.
 */
int read_trace(unsigned int baseaddr, unsigned int n)
{
	int r, i=0 ;

	set_register_value(baseaddr,OFFSET_ETB_RAM_READ_POINTER,0x00000000);
	r = get_register_value(baseaddr,OFFSET_ETB_RAM_WRITE_POINTER);

	while(1)
	{
		r = get_register_value(ETB_BASEADDR,OFFSET_ETBRRD);

		printf("%02x ",r&0xff);
		printf("%02x ",(r>>8)&0xff);
		printf("%02x ",(r>>16)&0xff);
		printf("%02x ",(r>>24)&0xff);

		i++;
		if ((i%4 == 0) & (i != 0))
			printf("\n");

		if (i==n)
			break;
	}
	return 0;
}

/********************* FUNCTIONS TO BE TRACED **********************/
int for_loop(unsigned int n)
{
	int i;
	for (i = 0; i < n; i++);
	return 0;
}

int factorial(unsigned int n)
{
	int i, fact = 0;
	for (i = 1; i <= n; i++)
		fact = fact * i;
	return fact;
}

int alea(unsigned int n)
{
	int i, r;
	for (i = 1; i <= n; i++)
	{
		r = rand()%100+1;
		if (r%2 == 0)
		{
			printf("Random Number is even : %d\n",r);
		}
		else
		{
			printf("Random Number is odd : %d\n", r);
		}
	}
	return 0;
}

/********************* MAIN **********************/
int main()
{
	unsigned int inst;
	//xil_printf("Hello World\n\r");


	show_etb_configuration(ETB_BASEADDR);
	enable_programming_ptm(PTM_BASEADDR);
	config_ptm(PTM_BASEADDR);
	disable_programming_ptm(PTM_BASEADDR);

	program_funnel(FUNNEL_BASEADDR);

	program_ETB(ETB_BASEADDR);

	for_loop(10);
	factorial(1000);
	alea(10);

	etb_stop_tracing(ETB_BASEADDR);
	read_trace(ETB_BASEADDR, 128);


	TIMING_INIT;

	TIMING_START;
	for ( inst = 0; inst < NB_TIMES; inst++ )
	{
#ifdef CHOLESKI
		//xil_printf(" CHOLESKI \r\n");
		app_choleski();
#endif
#ifdef DFT
		app_dft();
#endif
#ifdef FFT
		app_fft();
#endif
#ifdef FIR
		app_fir();
#endif
#ifdef LU
		app_lu();
#endif
#ifdef MATRIX
		app_matrix();
#endif
#ifdef RADIX
		app_radix();
#endif
#ifdef WHT
		app_wht();
#endif
#ifdef NBODY
		app_nbody();
#endif
#ifdef CRC
		app_crc();
#endif

		//xil_printf(".");

	}

	TIMING_STOP;
#ifdef CHOLESKI
	xil_printf(" CHOLESKI \r\n");
#endif
#ifdef DFT
	xil_printf(" DFT \r\n");
#endif
#ifdef FFT
	xil_printf(" FFT \r\n");
#endif
#ifdef FIR
	xil_printf(" FIR \r\n");
#endif
#ifdef LU
	xil_printf(" LU \r\n");
#endif
#ifdef MATRIX
	xil_printf(" MATRIX \r\n");
#endif
#ifdef RADIX
	xil_printf(" RADIX \r\n");
#endif
#ifdef WHT
	xil_printf(" WHT \r\n");
#endif
#ifdef NBODY
	xil_printf(" NBODY \r\n");
#endif
#ifdef CRC
	xil_printf(" CRC \r\n");
#endif

	TIMING_DISPLAY("With enabling PTM\r\n");

	xil_printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\r");
	/*    xil_printf("valid instr: %d\n", 	instr_val);
    xil_printf(" data access: %d\n", 	data_access);
    xil_printf("  data write: %d\n", 	data_write);
    xil_printf("  data read: %d\n\n", 	data_read);

    xil_printf("dcache request: %d\n", 	data_req);
    xil_printf(" dcache hit: %d\n", 	data_hit);
    xil_printf(" dcache miss: %d\n\n", 	data_req - data_hit);

    xil_printf("icache request: %d\n", 	inst_req);
    xil_printf(" icache hit: %d\n", 	inst_hit);
    xil_printf(" icache miss: %d\n", 	inst_req - inst_hit);
	 */
	return 0;
}



/**
 * \brief					Allows the configuration of PTM
 * \details 				PTM can be configured in \a following ways :
 * 							\enum Trace all instructions executed by the processor
 * 							\enum Trace only some regions of code through start and stop addresses (using ETMACVR and ETMACTR)
 * 							\enum Trace all except some regions of the code (using ETMACVR and ETMACTR)
 *							\enum Trace only single instruction (using SAC)
 *							This function has been modified and is not tested yet.
 *
 * \param[in]  	baseaddr 	Base address for PTM component
 * \param[in]	nb_regions	The number of regions to configure (can not be greater than the Maximum number of regions which is \a NB_REGIONS = 8)
 * \param[in]	start_addr	The start address(es) of the region(s)
 * \param[in]	stop_addr	The stop address(es) of the region(s)
 * \return 					The function returns 0 if successful.
 * \todo 					Implement SAC
 * 							Better understand the events that trigger the generation for PTM
 */
int config_ptm_regions(unsigned int baseaddr, unsigned int nb_regions, unsigned int start_addr[], unsigned int stop_addr[], unsigned int include)
{
	int include_bit = 0, r;
	/** MAKING SURE THAT WE HAVE THE CORRECT NUMBER OF ARGUMENTS **/
	if (nb_regions > NB_REGIONS)
	{
		fprintf(stderr,"The number of regions that can be included/excluded in the trace can not be bigger than the total number of regions : %d\n", NB_REGIONS);
		return EXIT_FAILURE;
	}
	// if the number of elements in start_addr[] and stop_addr[] is bigger than nb_regions, stop the program
	if ( (sizeof(start_addr)/sizeof(start_addr[0])) != nb_regions)
	{
		fprintf(stderr,"The number of elements in start_addr[] must be equal to nb_regions \n");
		return EXIT_FAILURE;
	}
	if ( (sizeof(stop_addr)/sizeof(stop_addr[0])) != nb_regions)
	{
		fprintf(stderr,"The number of elements in start_addr[] must be equal to nb_regions \n");
		return EXIT_FAILURE;
	}


	switch(nb_regions)
	{
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
	case 6:
	case 7:
	case 8:
	{
		//Start and stop addresses
		set_register_value(baseaddr,CS_ETMACVR(0),start_addr[0]);
		set_register_value(baseaddr,CS_ETMACTR(0),1);
		set_register_value(baseaddr,CS_ETMACVR(1),stop_addr[0]);
		set_register_value(baseaddr,CS_ETMACTR(1),1);

		if (nb_regions == 1)
		{
			set_register_value(baseaddr,OFFSET_ETMTECR1,1);
			break;
		}

		set_register_value(baseaddr,CS_ETMACVR(2),start_addr[1]);
		set_register_value(baseaddr,CS_ETMACTR(2),1);
		set_register_value(baseaddr,CS_ETMACVR(3),stop_addr[1]);
		set_register_value(baseaddr,CS_ETMACTR(3),1);

		if (nb_regions == 2)
		{
			set_register_value(baseaddr,OFFSET_ETMTECR1,1|1<<1);
			break;
		}

		set_register_value(baseaddr,CS_ETMACVR(4),start_addr[2]);
		set_register_value(baseaddr,CS_ETMACTR(4),1);
		set_register_value(baseaddr,CS_ETMACVR(5),stop_addr[2]);
		set_register_value(baseaddr,CS_ETMACTR(5),1);

		if (nb_regions == 3)
		{
			set_register_value(baseaddr,OFFSET_ETMTECR1,1|1<<1|1<<2);
			break;
		}

		set_register_value(baseaddr,CS_ETMACVR(6),start_addr[3]);
		set_register_value(baseaddr,CS_ETMACTR(6),1);
		set_register_value(baseaddr,CS_ETMACVR(7),stop_addr[3]);
		set_register_value(baseaddr,CS_ETMACTR(7),1);

		if (nb_regions == 4)
		{
			set_register_value(baseaddr,OFFSET_ETMTECR1,1|1<<1|1<<2|1<<3);
			break;
		}

		set_register_value(baseaddr,CS_ETMACVR(8),start_addr[4]);
		set_register_value(baseaddr,CS_ETMACTR(8),1);
		set_register_value(baseaddr,CS_ETMACVR(9),stop_addr[4]);
		set_register_value(baseaddr,CS_ETMACTR(9),1);

		if (nb_regions == 5)
		{
			set_register_value(baseaddr,OFFSET_ETMTECR1,1|1<<1|1<<2|1<<3|1<<4);
			break;
		}

		set_register_value(baseaddr,CS_ETMACVR(10),start_addr[5]);
		set_register_value(baseaddr,CS_ETMACTR(10),1);
		set_register_value(baseaddr,CS_ETMACVR(11),stop_addr[5]);
		set_register_value(baseaddr,CS_ETMACTR(11),1);

		if (nb_regions == 6)
		{
			set_register_value(baseaddr,OFFSET_ETMTECR1,1|1<<1|1<<2|1<<3|1<<4|1<<5);
			break;
		}

		set_register_value(baseaddr,CS_ETMACVR(12),start_addr[6]);
		set_register_value(baseaddr,CS_ETMACTR(12),1);
		set_register_value(baseaddr,CS_ETMACVR(13),stop_addr[6]);
		set_register_value(baseaddr,CS_ETMACTR(13),1);

		if (nb_regions == 7)
		{
			set_register_value(baseaddr,OFFSET_ETMTECR1,1|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6);
			break;
		}

		set_register_value(baseaddr,CS_ETMACVR(14),start_addr[7]);
		set_register_value(baseaddr,CS_ETMACTR(14),1);
		set_register_value(baseaddr,CS_ETMACVR(15),stop_addr[7]);
		set_register_value(baseaddr,CS_ETMACTR(15),1);

		if (nb_regions == 8)
		{
			set_register_value(baseaddr,OFFSET_ETMTECR1,1|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6|1<<7);
			break;
		}
	}
	default: /* By default, we trace all the "functions to be traced" and use only one start and stop addresses*/
	{
		//Start addresses
		set_register_value(baseaddr,CS_ETMACVR(0),(int)for_loop);
		set_register_value(baseaddr,CS_ETMACTR(0),1);
		//End addresses
		set_register_value(baseaddr,CS_ETMACVR(1),(int)main);
		set_register_value(baseaddr,CS_ETMACTR(1),1);
		break;
	}
	}

	printf("alea : %x\n",(int)alea);
	printf("for_loop : %x\n",(int)for_loop);

	/* ETMCR */
	/* bit 8 : BranchBroadcast */
	/* bit 12 : cycleAccurate */
	/* bit 28 : timestamping */
	r = get_register_value(baseaddr,OFFSET_ETMCR);
	set_register_value(baseaddr,OFFSET_ETMCR,(r|1<<8|1<<12|0<<28));

	/* ETMTECR1 */
	r = get_register_value(baseaddr,OFFSET_ETMTECR1);
	if (include)
		set_register_value(baseaddr,OFFSET_ETMTECR1,r|1<<24);
	else
		set_register_value(baseaddr,OFFSET_ETMTECR1,r|0<<24);

	/* ETMTSSCR */
	set_register_value(baseaddr,OFFSET_ETMTSSCR,0);

	set_register_value(baseaddr,CS_ETMACVR(2),(int)for_loop);
	set_register_value(baseaddr,CS_ETMACTR(2),1);

	set_register_value(baseaddr,CS_ETMACVR(3),(int)factorial);
	set_register_value(baseaddr,CS_ETMACTR(3),1);

	/* ETMTEEVR */
	set_register_value(baseaddr,OFFSET_ETMTEEVR,CS_ETMER_ALWAYS); /* Generate trace on all events */

	/* ETMSYNCFR */
	set_register_value(baseaddr,OFFSET_ETMSYNCFR,SYNC_FREQ); /* Frequency synchronization register */

	return 0;
}

/**
 * \brief					Allows the configuration of PTM
 * \details 				Configure PTM to recover trace for all instructions
 *
 * \param[in]  	baseaddr 	Base address for PTM component
 * \return 					The function returns 0 if successful.
 */
/*int config_ptm(unsigned int baseaddr)
{
	// /* ETMTECR1 *
	set_register_value(baseaddr,OFFSET_ETMTECR1,0<<24);
	// /* ETMTSSCR *
	set_register_value(baseaddr,OFFSET_ETMTSSCR,0);

	// /* ETMTEEVR *
	set_register_value(baseaddr,OFFSET_ETMTEEVR,CS_ETMER_ALWAYS); // /* Generate trace on all events *

	// /* ETMSYNCFR *
	set_register_value(baseaddr,OFFSET_ETMSYNCFR,SYNC_FREQ); // /* Frequency synchronization register *

	return 0;
}*/

int config_ptm(unsigned int baseaddr)
{
	int r;
	// ETMCR
	// bit 8 : BranchBroadcast
	// bit 12 : cycleAccurate
	// bit 28 : timestamping
	r = get_register_value(baseaddr,OFFSET_ETMCR);
	set_register_value(baseaddr,OFFSET_ETMCR,(r|1<<8|1<<12|0<<28));
	// ETMTECR1
	set_register_value(baseaddr,OFFSET_ETMTECR1,1);
	// ETMTSSCR
	set_register_value(baseaddr,OFFSET_ETMTSSCR,0); //SACs

	// ETMACVR[]
	//Start address
	set_register_value(baseaddr,CS_ETMACVR(0),(int)for_loop);
	set_register_value(baseaddr,CS_ETMACTR(0),1);

	//End address
	set_register_value(baseaddr,CS_ETMACVR(1),(int)main);
	set_register_value(baseaddr,CS_ETMACTR(1),1);

	// ETMTEEVR
	set_register_value(baseaddr,OFFSET_ETMTEEVR,0x6F);

	// ETMSYNCFR
	set_register_value(baseaddr,OFFSET_ETMSYNCFR,SYNC_FREQ);

	return 0;
}
