	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"write.c"
	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str
	movt	r0, :upper16:.L.str
	movw	r1, #1025
	movw	r2, #0
	str	r2, [r11, #-4]
	bl	open
.PTM_main_1:
	str	r0, [sp, #8]
	ldr	r0, [sp, #8]
	cmp	r0, #0
	bge	.LBB0_2
	b	.LBB0_1
.LBB0_1:                                @ %if.then
.PTM_main_2:
	mvn	r0, #0
	str	r0, [r11, #-4]
	b	.LBB0_5
.LBB0_2:                                @ %if.end
.PTM_main_3:
	movw	r1, :lower16:.L.str.1
	movt	r1, :upper16:.L.str.1
	movw	r2, #36
	ldr	r0, [sp, #8]
	bl	write
.PTM_main_4:
	cmp	r0, #36
	beq	.LBB0_4
	b	.LBB0_3
.LBB0_3:                                @ %if.then3
.PTM_main_5:
	movw	r0, #2
	movw	r1, :lower16:.L.str.2
	movt	r1, :upper16:.L.str.2
	movw	r2, #43
	bl	write
.PTM_main_6:
	mvn	r1, #0
	str	r1, [r11, #-4]
	str	r0, [sp, #4]            @ 4-byte Spill
	b	.LBB0_5
.LBB0_4:                                @ %if.end5
.PTM_main_7:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB0_5
.LBB0_5:                                @ %return
.PTM_main_8:
	ldr	r0, [r11, #-4]
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"test.txt"
	.size	.L.str, 9

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"This will be output to testfile.txt\n"
	.size	.L.str.1, 37

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"There was an error writing to testfile.txt\n"
	.size	.L.str.2, 44


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm 2b5c7ea7a610af4c0e9bb77143223b95c7b22f8b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430576
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
