################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_endpoint.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_g.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_hw.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_intr.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_sinit.c 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_endpoint.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_g.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_hw.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_intr.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_sinit.o 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_endpoint.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_g.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_hw.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_intr.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/xusbps_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/usbps_v2_2/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


