	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"radix.c"
	.globl	radixsort
	.p2align	2
	.type	radixsort,%function
	.code	32                      @ @radixsort
radixsort:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_radixsort_0:
	.save	{r4, r5, r11, lr}
	push	{r4, r5, r11, lr}
	.setfp	r11, sp, #8
	add	r11, sp, #8
	.pad	#104
	sub	sp, sp, #104
	.pad	#1024
	sub	sp, sp, #1024
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r2, #0
	movw	r3, #1
	str	r0, [r11, #-12]
	str	r1, [r11, #-16]
	str	r2, [sp, #88]
	str	r3, [sp, #84]
	str	r2, [r11, #-20]
	b	.LBB0_1
.LBB0_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_radixsort_1:
	ldr	r0, [r11, #-20]
	ldr	r1, [r11, #-16]
	cmp	r0, r1
	bge	.LBB0_6
	b	.LBB0_2
.LBB0_2:                                @ %for.body
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_radixsort_2:
	movw	r0, #2
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-20]
	add	r1, r1, r2, lsl #2
	ldr	r1, [r1]
	ldr	r2, [sp, #88]
	cmp	r1, r2
	str	r0, [sp, #40]           @ 4-byte Spill
	ble	.LBB0_4
	b	.LBB0_3
.LBB0_3:                                @ %if.then
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_radixsort_3:
	movw	r0, #2
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-20]
	add	r1, r1, r2, lsl #2
	ldr	r1, [r1]
	str	r1, [sp, #88]
	str	r0, [sp, #36]           @ 4-byte Spill
	b	.LBB0_4
.LBB0_4:                                @ %if.end
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_radixsort_4:
	b	.LBB0_5
	b	.LBB0_5
.LBB0_5:                                @ %for.inc
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_radixsort_5:
	ldr	r0, [r11, #-20]
	add	r0, r0, #1
	str	r0, [r11, #-20]
	b	.LBB0_1
.LBB0_6:                                @ %for.end
.PTM_radixsort_6:
	b	.LBB0_7
	b	.LBB0_7
.LBB0_7:                                @ %while.cond
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_9 Depth 2
                                        @     Child Loop BB0_13 Depth 2
                                        @     Child Loop BB0_17 Depth 2
                                        @     Child Loop BB0_21 Depth 2
.PTM_radixsort_7:
	ldr	r0, [sp, #88]
	ldr	r1, [sp, #84]
	bl	__aeabi_idiv
.PTM_radixsort_8:
	cmp	r0, #0
	ble	.LBB0_25
	b	.LBB0_8
.LBB0_8:                                @ %while.body
                                        @   in Loop: Header=BB0_7 Depth=1
.PTM_radixsort_9:
	movw	r0, #0
	movw	r1, #0
	movw	r2, #40
	add	r3, sp, #44
	str	r0, [sp, #32]           @ 4-byte Spill
	mov	r0, r3
	and	r1, r1, #255
	bl	memset
.PTM_radixsort_10:
	ldr	r0, [sp, #32]           @ 4-byte Reload
	str	r0, [r11, #-20]
	b	.LBB0_9
.LBB0_9:                                @ %for.cond4
                                        @   Parent Loop BB0_7 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_radixsort_11:
	ldr	r0, [r11, #-20]
	ldr	r1, [r11, #-16]
	cmp	r0, r1
	bge	.LBB0_12
	b	.LBB0_10
.LBB0_10:                               @ %for.body6
                                        @   in Loop: Header=BB0_9 Depth=2
.PTM_radixsort_12:
	add	r0, sp, #44
	movw	r1, #2
	ldr	r2, [r11, #-12]
	ldr	r3, [r11, #-20]
	ldr	r2, [r2, r3, lsl #2]
	ldr	r3, [sp, #84]
	str	r0, [sp, #28]           @ 4-byte Spill
	mov	r0, r2
	str	r1, [sp, #24]           @ 4-byte Spill
	mov	r1, r3
	bl	__aeabi_idiv
.PTM_radixsort_13:
	movw	r1, #26215
	movt	r1, #26214
	smmul	r1, r0, r1
	asr	r2, r1, #2
	add	r1, r2, r1, lsr #31
	add	r1, r1, r1, lsl #2
	sub	r0, r0, r1, lsl #1
	add	r1, sp, #44
	add	r0, r1, r0, lsl #2
	ldr	r1, [r0]
	add	r1, r1, #1
	str	r1, [r0]
	b	.LBB0_11
.LBB0_11:                               @ %for.inc11
                                        @   in Loop: Header=BB0_9 Depth=2
.PTM_radixsort_14:
	ldr	r0, [r11, #-20]
	add	r0, r0, #1
	str	r0, [r11, #-20]
	b	.LBB0_9
.LBB0_12:                               @ %for.end13
                                        @   in Loop: Header=BB0_7 Depth=1
.PTM_radixsort_15:
	movw	r0, #1
	str	r0, [r11, #-20]
	b	.LBB0_13
.LBB0_13:                               @ %for.cond14
                                        @   Parent Loop BB0_7 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_radixsort_16:
	ldr	r0, [r11, #-20]
	cmp	r0, #10
	bge	.LBB0_16
	b	.LBB0_14
.LBB0_14:                               @ %for.body16
                                        @   in Loop: Header=BB0_13 Depth=2
.PTM_radixsort_17:
	add	r0, sp, #44
	movw	r1, #2
	ldr	r2, [r11, #-20]
	add	r3, sp, #44
	add	r2, r3, r2, lsl #2
	ldr	r3, [r2, #-4]
	ldr	r12, [r2]
	add	r3, r12, r3
	str	r3, [r2]
	str	r0, [sp, #20]           @ 4-byte Spill
	str	r1, [sp, #16]           @ 4-byte Spill
	b	.LBB0_15
.LBB0_15:                               @ %for.inc19
                                        @   in Loop: Header=BB0_13 Depth=2
.PTM_radixsort_18:
	ldr	r0, [r11, #-20]
	add	r0, r0, #1
	str	r0, [r11, #-20]
	b	.LBB0_13
.LBB0_16:                               @ %for.end21
                                        @   in Loop: Header=BB0_7 Depth=1
.PTM_radixsort_19:
	ldr	r0, [r11, #-16]
	sub	r0, r0, #1
	str	r0, [r11, #-20]
	b	.LBB0_17
.LBB0_17:                               @ %for.cond23
                                        @   Parent Loop BB0_7 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_radixsort_20:
	ldr	r0, [r11, #-20]
	cmp	r0, #0
	blt	.LBB0_20
	b	.LBB0_18
.LBB0_18:                               @ %for.body25
                                        @   in Loop: Header=BB0_17 Depth=2
.PTM_radixsort_21:
	add	r0, sp, #92
	movw	r1, #2
	ldr	r2, [r11, #-12]
	ldr	r3, [r11, #-20]
	ldr	r2, [r2, r3, lsl #2]
	ldr	r3, [sp, #84]
	str	r0, [sp, #12]           @ 4-byte Spill
	mov	r0, r2
	str	r1, [sp, #8]            @ 4-byte Spill
	mov	r1, r3
	str	r2, [sp, #4]            @ 4-byte Spill
	bl	__aeabi_idiv
.PTM_radixsort_22:
	movw	r1, #26215
	movt	r1, #26214
	smmul	r1, r0, r1
	lsr	r2, r1, #2
	add	r1, r2, r1, lsr #31
	add	r1, r1, r1, lsl #2
	sub	r0, r0, r1, lsl #1
	add	r1, sp, #44
	ldr	r2, [r1, r0, lsl #2]
	sub	r2, r2, #1
	str	r2, [r1, r0, lsl #2]
	add	r0, sp, #92
	add	r0, r0, r2, lsl #2
	ldr	r1, [sp, #4]            @ 4-byte Reload
	str	r1, [r0]
	b	.LBB0_19
.LBB0_19:                               @ %for.inc32
                                        @   in Loop: Header=BB0_17 Depth=2
.PTM_radixsort_23:
	mvn	r0, #0
	ldr	r1, [r11, #-20]
	add	r0, r1, r0
	str	r0, [r11, #-20]
	b	.LBB0_17
.LBB0_20:                               @ %for.end34
                                        @   in Loop: Header=BB0_7 Depth=1
.PTM_radixsort_24:
	movw	r0, #0
	str	r0, [r11, #-20]
	b	.LBB0_21
.LBB0_21:                               @ %for.cond35
                                        @   Parent Loop BB0_7 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_radixsort_25:
	ldr	r0, [r11, #-20]
	ldr	r1, [r11, #-16]
	cmp	r0, r1
	bge	.LBB0_24
	b	.LBB0_22
.LBB0_22:                               @ %for.body37
                                        @   in Loop: Header=BB0_21 Depth=2
.PTM_radixsort_26:
	movw	r0, #2
	ldr	r1, [r11, #-20]
	add	r2, sp, #92
	ldr	r2, [r2, r1, lsl #2]
	ldr	r3, [r11, #-12]
	add	r1, r3, r1, lsl #2
	str	r2, [r1]
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB0_23
.LBB0_23:                               @ %for.inc40
                                        @   in Loop: Header=BB0_21 Depth=2
.PTM_radixsort_27:
	ldr	r0, [r11, #-20]
	add	r0, r0, #1
	str	r0, [r11, #-20]
	b	.LBB0_21
.LBB0_24:                               @ %for.end42
                                        @   in Loop: Header=BB0_7 Depth=1
.PTM_radixsort_28:
	movw	r0, #10
	ldr	r1, [sp, #84]
	mul	r0, r1, r0
	str	r0, [sp, #84]
	b	.LBB0_7
.LBB0_25:                               @ %while.end
.PTM_radixsort_29:
	sub	sp, r11, #8
	pop	{r4, r5, r11, pc}
.Lfunc_end0:
	.size	radixsort, .Lfunc_end0-radixsort
	.cantunwind
	.fnend

	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r0, #0
	bl	time
.PTM_main_1:
	bl	srand
.PTM_main_2:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB1_1
.LBB1_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_3:
	ldr	r0, [r11, #-4]
	cmp	r0, #256
	bge	.LBB1_4
	b	.LBB1_2
.LBB1_2:                                @ %for.body
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_main_4:
	movw	r0, :lower16:main.arr
	movt	r0, :upper16:main.arr
	movw	r1, #2
	str	r0, [sp, #8]            @ 4-byte Spill
	str	r1, [sp, #4]            @ 4-byte Spill
	bl	rand
.PTM_main_5:
	ldr	r1, [r11, #-4]
	movw	lr, :lower16:main.arr
	movt	lr, :upper16:main.arr
	add	r1, lr, r1, lsl #2
	str	r0, [r1]
	b	.LBB1_3
.LBB1_3:                                @ %for.inc
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_main_6:
	ldr	r0, [r11, #-4]
	add	r0, r0, #1
	str	r0, [r11, #-4]
	b	.LBB1_1
.LBB1_4:                                @ %for.end
.PTM_main_7:
	movw	r0, :lower16:main.arr
	movt	r0, :upper16:main.arr
	movw	r1, #256
	bl	radixsort
.PTM_main_8:
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cantunwind
	.fnend

	.type	main.arr,%object        @ @main.arr
	.local	main.arr
	.comm	main.arr,1024,4

	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430488
	.long	504429568
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509804544
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.long	.PTM_radixsort_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.long	.PTM_radixsort_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.long	.PTM_radixsort_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.long	.PTM_radixsort_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474284032
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.long	.PTM_radixsort_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474218496
	.long	476381184
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2391159040
	.long	476315648
	.long	474152960
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.long	.PTM_radixsort_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.long	.PTM_radixsort_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_17
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	478478336
	.long	2357876480
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_18
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.long	.PTM_radixsort_19
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.long	.PTM_radixsort_20
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_21
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474218496
	.long	476381184
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_22
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2391159040
	.long	476315648
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_23
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469568
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_24
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.long	.PTM_radixsort_25
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_26
	.section	.HB.annot,"a",%progbits
.Ltmp26:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp26(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	476381184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_27
	.section	.HB.annot,"a",%progbits
.Ltmp27:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp27(sbrel)
	.long	.PTM_radixsort_28
	.section	.HB.annot,"a",%progbits
.Ltmp28:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp28(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469568
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_radixsort_29
	.section	.HB.annot,"a",%progbits
.Ltmp29:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp29(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430584
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp30:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp30(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430576
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp31:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp31(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp32:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp32(sbrel)
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp33:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp33(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp34:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp34(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp35:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp35(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	476708864
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp36:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp36(sbrel)
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp37:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp37(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp38:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp38(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
