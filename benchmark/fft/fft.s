	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"fft.c"
	.globl	fft
	.p2align	3
	.type	fft,%function
	.code	32                      @ @fft
fft:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_fft_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#160
	sub	sp, sp, #160
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r12, #0
	movw	lr, #1
	strh	r0, [r11, #-2]
	str	r1, [r11, #-8]
	str	r2, [r11, #-12]
	str	r3, [r11, #-16]
	str	lr, [r11, #-20]
	str	r12, [r11, #-24]
	b	.LBB0_1
.LBB0_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_fft_1:
	ldr	r0, [r11, #-24]
	ldr	r1, [r11, #-8]
	cmp	r0, r1
	bge	.LBB0_4
	b	.LBB0_2
.LBB0_2:                                @ %for.body
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_fft_2:
	movw	r0, #1
	ldr	r1, [r11, #-20]
	lsl	r1, r1, #1
	str	r1, [r11, #-20]
	str	r0, [sp, #28]           @ 4-byte Spill
	b	.LBB0_3
.LBB0_3:                                @ %for.inc
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_fft_3:
	ldr	r0, [r11, #-24]
	add	r0, r0, #1
	str	r0, [r11, #-24]
	b	.LBB0_1
.LBB0_4:                                @ %for.end
.PTM_fft_4:
	movw	r0, #0
	movw	r1, #1
	ldr	r2, [r11, #-20]
	asr	r2, r2, #1
	str	r2, [r11, #-40]
	str	r0, [r11, #-32]
	str	r0, [r11, #-24]
	str	r1, [sp, #24]           @ 4-byte Spill
	b	.LBB0_5
.LBB0_5:                                @ %for.cond1
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_9 Depth 2
.PTM_fft_5:
	ldr	r0, [r11, #-24]
	ldr	r1, [r11, #-20]
	sub	r1, r1, #1
	cmp	r0, r1
	bge	.LBB0_13
	b	.LBB0_6
.LBB0_6:                                @ %for.body3
                                        @   in Loop: Header=BB0_5 Depth=1
.PTM_fft_6:
	ldr	r0, [r11, #-24]
	ldr	r1, [r11, #-32]
	cmp	r0, r1
	bge	.LBB0_8
	b	.LBB0_7
.LBB0_7:                                @ %if.then
                                        @   in Loop: Header=BB0_5 Depth=1
.PTM_fft_7:
	movw	r0, #3
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vldr	d16, [r1]
	vstr	d16, [sp, #80]
	ldr	r1, [r11, #-16]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vldr	d16, [r1]
	vstr	d16, [sp, #72]
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-32]
	add	r2, r1, r2, lsl #3
	vldr	d16, [r2]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	ldr	r1, [r11, #-16]
	ldr	r2, [r11, #-32]
	add	r2, r1, r2, lsl #3
	vldr	d16, [r2]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	vldr	d16, [sp, #80]
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-32]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	vldr	d16, [sp, #72]
	ldr	r1, [r11, #-16]
	ldr	r2, [r11, #-32]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	str	r0, [sp, #20]           @ 4-byte Spill
	b	.LBB0_8
.LBB0_8:                                @ %if.end
                                        @   in Loop: Header=BB0_5 Depth=1
.PTM_fft_8:
	ldr	r0, [r11, #-40]
	str	r0, [r11, #-36]
	b	.LBB0_9
.LBB0_9:                                @ %while.cond
                                        @   Parent Loop BB0_5 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_fft_9:
	ldr	r0, [r11, #-36]
	ldr	r1, [r11, #-32]
	cmp	r0, r1
	bgt	.LBB0_11
	b	.LBB0_10
.LBB0_10:                               @ %while.body
                                        @   in Loop: Header=BB0_9 Depth=2
.PTM_fft_10:
	movw	r0, #1
	ldr	r1, [r11, #-36]
	ldr	r2, [r11, #-32]
	sub	r1, r2, r1
	str	r1, [r11, #-32]
	ldr	r1, [r11, #-36]
	asr	r1, r1, #1
	str	r1, [r11, #-36]
	str	r0, [sp, #16]           @ 4-byte Spill
	b	.LBB0_9
.LBB0_11:                               @ %while.end
                                        @   in Loop: Header=BB0_5 Depth=1
.PTM_fft_11:
	ldr	r0, [r11, #-36]
	ldr	r1, [r11, #-32]
	add	r0, r1, r0
	str	r0, [r11, #-32]
	b	.LBB0_12
.LBB0_12:                               @ %for.inc15
                                        @   in Loop: Header=BB0_5 Depth=1
.PTM_fft_12:
	ldr	r0, [r11, #-24]
	add	r0, r0, #1
	str	r0, [r11, #-24]
	b	.LBB0_5
.LBB0_13:                               @ %for.end17
.PTM_fft_13:
	movw	r0, #0
	movw	r1, #1
	vldr	d16, .LCPI0_0
	vmov.f64	d17, #-1.000000e+00
	vstr	d17, [r11, #-64]
	vstr	d16, [r11, #-72]
	str	r1, [r11, #-52]
	str	r0, [r11, #-44]
	b	.LBB0_14
.LBB0_14:                               @ %for.cond18
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_16 Depth 2
                                        @       Child Loop BB0_18 Depth 3
.PTM_fft_14:
	ldr	r0, [r11, #-44]
	ldr	r1, [r11, #-8]
	cmp	r0, r1
	bge	.LBB0_27
	b	.LBB0_15
.LBB0_15:                               @ %for.body20
                                        @   in Loop: Header=BB0_14 Depth=1
.PTM_fft_15:
	movw	r0, #0
	vldr	d16, .LCPI0_0
	vmov.f64	d17, #1.000000e+00
	movw	r1, #1
	ldr	r2, [r11, #-52]
	str	r2, [r11, #-48]
	ldr	r2, [r11, #-52]
	lsl	r2, r2, #1
	str	r2, [r11, #-52]
	vstr	d17, [sp, #48]
	vstr	d16, [sp, #40]
	str	r0, [r11, #-32]
	str	r1, [sp, #12]           @ 4-byte Spill
	b	.LBB0_16
.LBB0_16:                               @ %for.cond21
                                        @   Parent Loop BB0_14 Depth=1
                                        @ =>  This Loop Header: Depth=2
                                        @       Child Loop BB0_18 Depth 3
.PTM_fft_16:
	ldr	r0, [r11, #-32]
	ldr	r1, [r11, #-48]
	cmp	r0, r1
	bge	.LBB0_23
	b	.LBB0_17
.LBB0_17:                               @ %for.body23
                                        @   in Loop: Header=BB0_16 Depth=2
.PTM_fft_17:
	ldr	r0, [r11, #-32]
	str	r0, [r11, #-24]
	b	.LBB0_18
.LBB0_18:                               @ %for.cond24
                                        @   Parent Loop BB0_14 Depth=1
                                        @     Parent Loop BB0_16 Depth=2
                                        @ =>    This Inner Loop Header: Depth=3
.PTM_fft_18:
	ldr	r0, [r11, #-24]
	ldr	r1, [r11, #-20]
	cmp	r0, r1
	bge	.LBB0_21
	b	.LBB0_19
.LBB0_19:                               @ %for.body26
                                        @   in Loop: Header=BB0_18 Depth=3
.PTM_fft_19:
	movw	r0, #3
	ldr	r1, [r11, #-24]
	ldr	r2, [r11, #-48]
	add	r1, r1, r2
	str	r1, [r11, #-28]
	vldr	d16, [sp, #48]
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-28]
	add	r1, r1, r2, lsl #3
	vldr	d17, [r1]
	vmul.f64	d16, d16, d17
	vldr	d17, [sp, #40]
	ldr	r1, [r11, #-16]
	add	r1, r1, r2, lsl #3
	vldr	d18, [r1]
	vmls.f64	d16, d17, d18
	vstr	d16, [sp, #64]
	vldr	d16, [sp, #48]
	ldr	r1, [r11, #-16]
	ldr	r2, [r11, #-28]
	add	r1, r1, r2, lsl #3
	vldr	d17, [r1]
	vldr	d18, [sp, #40]
	ldr	r1, [r11, #-12]
	add	r1, r1, r2, lsl #3
	vldr	d19, [r1]
	vmul.f64	d18, d18, d19
	vmla.f64	d18, d16, d17
	vstr	d18, [sp, #56]
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-24]
	add	r2, r1, r2, lsl #3
	vldr	d16, [r2]
	vldr	d17, [sp, #64]
	vsub.f64	d16, d16, d17
	ldr	r2, [r11, #-28]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	ldr	r1, [r11, #-16]
	ldr	r2, [r11, #-24]
	add	r2, r1, r2, lsl #3
	vldr	d16, [r2]
	vldr	d17, [sp, #56]
	vsub.f64	d16, d16, d17
	ldr	r2, [r11, #-28]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	vldr	d16, [sp, #64]
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vldr	d17, [r1]
	vadd.f64	d16, d17, d16
	vstr	d16, [r1]
	vldr	d16, [sp, #56]
	ldr	r1, [r11, #-16]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vldr	d17, [r1]
	vadd.f64	d16, d17, d16
	vstr	d16, [r1]
	str	r0, [sp, #8]            @ 4-byte Spill
	b	.LBB0_20
.LBB0_20:                               @ %for.inc48
                                        @   in Loop: Header=BB0_18 Depth=3
.PTM_fft_20:
	ldr	r0, [r11, #-52]
	ldr	r1, [r11, #-24]
	add	r0, r1, r0
	str	r0, [r11, #-24]
	b	.LBB0_18
.LBB0_21:                               @ %for.end50
                                        @   in Loop: Header=BB0_16 Depth=2
.PTM_fft_21:
	vldr	d16, [sp, #48]
	vldr	d17, [r11, #-64]
	vmul.f64	d16, d16, d17
	vldr	d17, [sp, #40]
	vldr	d18, [r11, #-72]
	vmul.f64	d17, d17, d18
	vsub.f64	d16, d16, d17
	vstr	d16, [sp, #32]
	vldr	d16, [sp, #48]
	vldr	d17, [r11, #-72]
	vmul.f64	d16, d16, d17
	vldr	d17, [sp, #40]
	vldr	d18, [r11, #-64]
	vmul.f64	d17, d17, d18
	vadd.f64	d16, d16, d17
	vstr	d16, [sp, #40]
	vldr	d16, [sp, #32]
	vstr	d16, [sp, #48]
	b	.LBB0_22
.LBB0_22:                               @ %for.inc57
                                        @   in Loop: Header=BB0_16 Depth=2
.PTM_fft_22:
	ldr	r0, [r11, #-32]
	add	r0, r0, #1
	str	r0, [r11, #-32]
	b	.LBB0_16
.LBB0_23:                               @ %for.end59
                                        @   in Loop: Header=BB0_14 Depth=1
.PTM_fft_23:
	vmov.f64	d16, #2.000000e+00
	vmov.f64	d17, #1.000000e+00
	vldr	d18, [r11, #-64]
	vsub.f64	d17, d17, d18
	vdiv.f64	d0, d17, d16
	bl	sqrt
.PTM_fft_24:
	vstr	d0, [r11, #-72]
	ldrsh	lr, [r11, #-2]
	cmp	lr, #1
	bne	.LBB0_25
	b	.LBB0_24
.LBB0_24:                               @ %if.then63
                                        @   in Loop: Header=BB0_14 Depth=1
.PTM_fft_25:
	vldr	d16, [r11, #-72]
	vneg.f64	d16, d16
	vstr	d16, [r11, #-72]
	b	.LBB0_25
.LBB0_25:                               @ %if.end65
                                        @   in Loop: Header=BB0_14 Depth=1
.PTM_fft_26:
	vmov.f64	d16, #2.000000e+00
	vmov.f64	d17, #1.000000e+00
	vldr	d18, [r11, #-64]
	vadd.f64	d17, d17, d18
	vdiv.f64	d0, d17, d16
	bl	sqrt
.PTM_fft_27:
	vstr	d0, [r11, #-64]
	b	.LBB0_26
.LBB0_26:                               @ %for.inc69
                                        @   in Loop: Header=BB0_14 Depth=1
.PTM_fft_28:
	ldr	r0, [r11, #-44]
	add	r0, r0, #1
	str	r0, [r11, #-44]
	b	.LBB0_14
.LBB0_27:                               @ %for.end71
.PTM_fft_29:
	ldrsh	r0, [r11, #-2]
	cmp	r0, #1
	bne	.LBB0_33
	b	.LBB0_28
.LBB0_28:                               @ %if.then75
.PTM_fft_30:
	movw	r0, #0
	str	r0, [r11, #-24]
	b	.LBB0_29
.LBB0_29:                               @ %for.cond76
                                        @ =>This Inner Loop Header: Depth=1
.PTM_fft_31:
	ldr	r0, [r11, #-24]
	ldr	r1, [r11, #-20]
	cmp	r0, r1
	bge	.LBB0_32
	b	.LBB0_30
.LBB0_30:                               @ %for.body79
                                        @   in Loop: Header=BB0_29 Depth=1
.PTM_fft_32:
	movw	r0, #3
	vldr	s0, [r11, #-20]
	vcvt.f64.s32	d16, s0
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vldr	d17, [r1]
	vdiv.f64	d16, d17, d16
	vstr	d16, [r1]
	vldr	s0, [r11, #-20]
	vcvt.f64.s32	d16, s0
	ldr	r1, [r11, #-16]
	ldr	r2, [r11, #-24]
	add	r1, r1, r2, lsl #3
	vldr	d17, [r1]
	vdiv.f64	d16, d17, d16
	vstr	d16, [r1]
	str	r0, [sp, #4]            @ 4-byte Spill
	b	.LBB0_31
.LBB0_31:                               @ %for.inc86
                                        @   in Loop: Header=BB0_29 Depth=1
.PTM_fft_33:
	ldr	r0, [r11, #-24]
	add	r0, r0, #1
	str	r0, [r11, #-24]
	b	.LBB0_29
.LBB0_32:                               @ %for.end88
.PTM_fft_34:
	b	.LBB0_33
	b	.LBB0_33
.LBB0_33:                               @ %if.end89
.PTM_fft_35:
	movw	r0, #1
	sxth	r0, r0
	mov	sp, r11
	pop	{r11, pc}
	.p2align	3
@ BB#34:
.LCPI0_0:
	.long	0                       @ double 0
	.long	0
.Lfunc_end0:
	.size	fft, .Lfunc_end0-fft
	.cantunwind
	.fnend

	.globl	main
	.p2align	3
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#40
	sub	sp, sp, #40
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r0, #0
	bl	time
.PTM_main_1:
	bl	srand
.PTM_main_2:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB1_1
.LBB1_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_3:
	ldr	r0, [r11, #-4]
	cmp	r0, #512
	bhs	.LBB1_4
	b	.LBB1_2
.LBB1_2:                                @ %for.body
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_main_4:
	movw	r0, :lower16:main.img
	movt	r0, :upper16:main.img
	movw	r1, #3
	str	r0, [r11, #-8]          @ 4-byte Spill
	str	r1, [r11, #-12]         @ 4-byte Spill
	bl	rand
.PTM_main_5:
	vmov	s0, r0
	vcvt.f64.s32	d16, s0
	vldr	d17, .LCPI1_0
	vdiv.f64	d16, d16, d17
	vadd.f64	d16, d16, d16
	vmov.f64	d18, #1.000000e+00
	vsub.f64	d16, d16, d18
	ldr	r0, [r11, #-4]
	movw	r1, :lower16:main.real
	movt	r1, :upper16:main.real
	add	r0, r1, r0, lsl #3
	vstr	d16, [r0]
	vstr	d17, [sp, #16]          @ 8-byte Spill
	vstr	d18, [sp, #8]           @ 8-byte Spill
	bl	rand
.PTM_main_6:
	vmov	s0, r0
	vcvt.f64.s32	d16, s0
	vldr	d17, [sp, #16]          @ 8-byte Reload
	vdiv.f64	d16, d16, d17
	vadd.f64	d16, d16, d16
	vldr	d18, [sp, #8]           @ 8-byte Reload
	vsub.f64	d16, d16, d18
	ldr	r0, [r11, #-4]
	movw	r1, :lower16:main.img
	movt	r1, :upper16:main.img
	add	r0, r1, r0, lsl #3
	vstr	d16, [r0]
	b	.LBB1_3
.LBB1_3:                                @ %for.inc
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_main_7:
	ldr	r0, [r11, #-4]
	add	r0, r0, #1
	str	r0, [r11, #-4]
	b	.LBB1_1
.LBB1_4:                                @ %for.end
.PTM_main_8:
	movw	r0, #1
	movw	r1, #2
	movw	r2, :lower16:main.real
	movt	r2, :upper16:main.real
	movw	r3, :lower16:main.img
	movt	r3, :upper16:main.img
	sxth	r0, r0
	bl	fft
.PTM_main_9:
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
	.p2align	3
@ BB#5:
.LCPI1_0:
	.long	4290772992              @ double 2147483647
	.long	1105199103
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cantunwind
	.fnend

	.type	main.real,%object       @ @main.real
	.local	main.real
	.comm	main.real,4096,8
	.type	main.img,%object        @ @main.img
	.local	main.img
	.comm	main.img,4096,8

	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430432
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.long	.PTM_fft_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.long	.PTM_fft_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.long	.PTM_fft_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	478347264
	.long	478347264
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.long	.PTM_fft_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357604864
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469568
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.long	.PTM_fft_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.long	.PTM_fft_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.long	.PTM_fft_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_17
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.long	.PTM_fft_18
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_19
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357604864
	.long	478347264
	.long	478347264
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_20
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469568
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_21
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.long	.PTM_fft_22
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.long	.PTM_fft_23
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_24
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	510263296
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_25
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.long	.PTM_fft_26
	.section	.HB.annot,"a",%progbits
.Ltmp26:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp26(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_27
	.section	.HB.annot,"a",%progbits
.Ltmp27:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp27(sbrel)
	.long	.PTM_fft_28
	.section	.HB.annot,"a",%progbits
.Ltmp28:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp28(sbrel)
	.long	.PTM_fft_29
	.section	.HB.annot,"a",%progbits
.Ltmp29:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp29(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_30
	.section	.HB.annot,"a",%progbits
.Ltmp30:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp30(sbrel)
	.long	.PTM_fft_31
	.section	.HB.annot,"a",%progbits
.Ltmp31:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp31(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_fft_32
	.section	.HB.annot,"a",%progbits
.Ltmp32:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp32(sbrel)
	.long	.PTM_fft_33
	.section	.HB.annot,"a",%progbits
.Ltmp33:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp33(sbrel)
	.long	.PTM_fft_34
	.section	.HB.annot,"a",%progbits
.Ltmp34:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp34(sbrel)
	.long	.PTM_fft_35
	.section	.HB.annot,"a",%progbits
.Ltmp35:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp35(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp36:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp36(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430552
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp37:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp37(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp38:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp38(sbrel)
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp39:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp39(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp40:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp40(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp41:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp41(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp42:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp42(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp43:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp43(sbrel)
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp44:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp44(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp45:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp45(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
