#include <sys/uio.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>


int main() {

   FILE *fp;

   char str[] = "This is a test for fwrite \n";

   fp = fopen( "test.txt" , "w" );

   fwrite(str , 10 , sizeof(str) , fp );

   fclose(fp);

   return(0);

}
