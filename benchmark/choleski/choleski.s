	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"choleski.c"
	.globl	Choleski_LU_Decomposition
	.p2align	2
	.type	Choleski_LU_Decomposition,%function
	.code	32                      @ @Choleski_LU_Decomposition
Choleski_LU_Decomposition:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_Choleski_LU_Decomposition_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#72
	sub	sp, sp, #72
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r2, #0
	str	r0, [r11, #-8]
	str	r1, [r11, #-12]
	str	r2, [r11, #-20]
	ldr	r0, [r11, #-8]
	str	r0, [r11, #-28]
	b	.LBB0_1
.LBB0_1:                                @ %for.cond
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_3 Depth 2
                                        @     Child Loop BB0_9 Depth 2
                                        @       Child Loop BB0_11 Depth 3
.PTM_Choleski_LU_Decomposition_1:
	ldr	r0, [r11, #-20]
	ldr	r1, [r11, #-12]
	cmp	r0, r1
	bge	.LBB0_18
	b	.LBB0_2
.LBB0_2:                                @ %for.body
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_Choleski_LU_Decomposition_2:
	movw	r0, #0
	movw	r1, #3
	ldr	r2, [r11, #-28]
	ldr	r3, [r11, #-20]
	add	r2, r2, r3, lsl #3
	str	r2, [sp, #36]
	str	r0, [r11, #-24]
	ldr	r0, [r11, #-28]
	str	r0, [r11, #-32]
	str	r1, [sp, #20]           @ 4-byte Spill
	b	.LBB0_3
.LBB0_3:                                @ %for.cond1
                                        @   Parent Loop BB0_1 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_Choleski_LU_Decomposition_3:
	ldr	r0, [r11, #-24]
	ldr	r1, [r11, #-20]
	cmp	r0, r1
	bge	.LBB0_6
	b	.LBB0_4
.LBB0_4:                                @ %for.body3
                                        @   in Loop: Header=BB0_3 Depth=2
.PTM_Choleski_LU_Decomposition_4:
	ldr	r0, [r11, #-32]
	vldr	d16, [r0]
	ldr	r0, [r11, #-32]
	vldr	d17, [r0]
	vmul.f64	d16, d16, d17
	ldr	r0, [sp, #36]
	vldr	d17, [r0]
	vsub.f64	d16, d17, d16
	vstr	d16, [r0]
	b	.LBB0_5
.LBB0_5:                                @ %for.inc
                                        @   in Loop: Header=BB0_3 Depth=2
.PTM_Choleski_LU_Decomposition_5:
	ldr	r0, [r11, #-32]
	add	r0, r0, #8
	str	r0, [r11, #-32]
	ldr	r0, [r11, #-24]
	add	r0, r0, #1
	str	r0, [r11, #-24]
	b	.LBB0_3
.LBB0_6:                                @ %for.end
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_Choleski_LU_Decomposition_6:
	ldr	r0, [sp, #36]
	vldr	d16, [r0]
	vcmpe.f64	d16, #0
	vmrs	APSR_nzcv, fpscr
	bhi	.LBB0_8
	b	.LBB0_7
.LBB0_7:                                @ %if.then
.PTM_Choleski_LU_Decomposition_7:
	mvn	r0, #0
	str	r0, [r11, #-4]
	b	.LBB0_19
.LBB0_8:                                @ %if.end
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_Choleski_LU_Decomposition_8:
	movw	r0, #3
	ldr	r1, [sp, #36]
	vldr	d0, [r1]
	str	r0, [sp, #16]           @ 4-byte Spill
	bl	sqrt
.PTM_Choleski_LU_Decomposition_9:
	ldr	r0, [sp, #36]
	vstr	d0, [r0]
	ldr	r0, [sp, #36]
	vldr	d0, [r0]
	vmov.f64	d16, #1.000000e+00
	vdiv.f64	d16, d16, d0
	vstr	d16, [sp, #24]
	ldr	r0, [r11, #-28]
	ldr	r1, [r11, #-12]
	add	r0, r0, r1, lsl #3
	str	r0, [sp, #32]
	ldr	r0, [r11, #-20]
	add	r0, r0, #1
	str	r0, [r11, #-16]
	b	.LBB0_9
.LBB0_9:                                @ %for.cond7
                                        @   Parent Loop BB0_1 Depth=1
                                        @ =>  This Loop Header: Depth=2
                                        @       Child Loop BB0_11 Depth 3
.PTM_Choleski_LU_Decomposition_10:
	ldr	r0, [r11, #-16]
	ldr	r1, [r11, #-12]
	cmp	r0, r1
	bge	.LBB0_16
	b	.LBB0_10
.LBB0_10:                               @ %for.body9
                                        @   in Loop: Header=BB0_9 Depth=2
.PTM_Choleski_LU_Decomposition_11:
	movw	r0, #0
	str	r0, [r11, #-24]
	b	.LBB0_11
.LBB0_11:                               @ %for.cond10
                                        @   Parent Loop BB0_1 Depth=1
                                        @     Parent Loop BB0_9 Depth=2
                                        @ =>    This Inner Loop Header: Depth=3
.PTM_Choleski_LU_Decomposition_12:
	ldr	r0, [r11, #-24]
	ldr	r1, [r11, #-20]
	cmp	r0, r1
	bge	.LBB0_14
	b	.LBB0_12
.LBB0_12:                               @ %for.body12
                                        @   in Loop: Header=BB0_11 Depth=3
.PTM_Choleski_LU_Decomposition_13:
	movw	r0, #3
	ldr	r1, [sp, #32]
	ldr	r2, [r11, #-24]
	add	r3, r1, r2, lsl #3
	vldr	d16, [r3]
	ldr	r3, [r11, #-28]
	add	r2, r3, r2, lsl #3
	vldr	d17, [r2]
	vmul.f64	d16, d16, d17
	ldr	r2, [r11, #-20]
	add	r1, r1, r2, lsl #3
	vldr	d17, [r1]
	vsub.f64	d16, d17, d16
	vstr	d16, [r1]
	str	r0, [sp, #12]           @ 4-byte Spill
	b	.LBB0_13
.LBB0_13:                               @ %for.inc18
                                        @   in Loop: Header=BB0_11 Depth=3
.PTM_Choleski_LU_Decomposition_14:
	ldr	r0, [r11, #-24]
	add	r0, r0, #1
	str	r0, [r11, #-24]
	b	.LBB0_11
.LBB0_14:                               @ %for.end20
                                        @   in Loop: Header=BB0_9 Depth=2
.PTM_Choleski_LU_Decomposition_15:
	movw	r0, #3
	vldr	d16, [sp, #24]
	ldr	r1, [sp, #32]
	ldr	r2, [r11, #-20]
	add	r1, r1, r2, lsl #3
	vldr	d17, [r1]
	vmul.f64	d16, d17, d16
	vstr	d16, [r1]
	ldr	r1, [sp, #32]
	ldr	r2, [r11, #-20]
	add	r1, r1, r2, lsl #3
	vldr	d16, [r1]
	ldr	r1, [r11, #-28]
	ldr	r2, [r11, #-16]
	add	r1, r1, r2, lsl #3
	vstr	d16, [r1]
	str	r0, [sp, #8]            @ 4-byte Spill
	b	.LBB0_15
.LBB0_15:                               @ %for.inc25
                                        @   in Loop: Header=BB0_9 Depth=2
.PTM_Choleski_LU_Decomposition_16:
	movw	r0, #3
	ldr	r1, [r11, #-12]
	ldr	r2, [sp, #32]
	add	r1, r2, r1, lsl #3
	str	r1, [sp, #32]
	ldr	r1, [r11, #-16]
	add	r1, r1, #1
	str	r1, [r11, #-16]
	str	r0, [sp, #4]            @ 4-byte Spill
	b	.LBB0_9
.LBB0_16:                               @ %for.end28
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_Choleski_LU_Decomposition_17:
	b	.LBB0_17
	b	.LBB0_17
.LBB0_17:                               @ %for.inc29
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_Choleski_LU_Decomposition_18:
	movw	r0, #3
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-28]
	add	r1, r2, r1, lsl #3
	str	r1, [r11, #-28]
	ldr	r1, [r11, #-20]
	add	r1, r1, #1
	str	r1, [r11, #-20]
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB0_1
.LBB0_18:                               @ %for.end32
.PTM_Choleski_LU_Decomposition_19:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB0_19
.LBB0_19:                               @ %return
.PTM_Choleski_LU_Decomposition_20:
	ldr	r0, [r11, #-4]
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	Choleski_LU_Decomposition, .Lfunc_end0-Choleski_LU_Decomposition
	.cantunwind
	.fnend

	.globl	main
	.p2align	3
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r0, #0
	bl	time
.PTM_main_1:
	bl	srand
.PTM_main_2:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB1_1
.LBB1_1:                                @ %for.cond
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB1_3 Depth 2
.PTM_main_3:
	ldr	r0, [r11, #-4]
	cmp	r0, #64
	bhs	.LBB1_8
	b	.LBB1_2
.LBB1_2:                                @ %for.body
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_main_4:
	movw	r0, #0
	str	r0, [sp, #8]
	b	.LBB1_3
.LBB1_3:                                @ %for.cond1
                                        @   Parent Loop BB1_1 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_main_5:
	ldr	r0, [sp, #8]
	cmp	r0, #64
	bhs	.LBB1_6
	b	.LBB1_4
.LBB1_4:                                @ %for.body3
                                        @   in Loop: Header=BB1_3 Depth=2
.PTM_main_6:
	movw	r0, #3
	str	r0, [sp, #4]            @ 4-byte Spill
	bl	rand
.PTM_main_7:
	vmov	s0, r0
	vcvt.f64.s32	d16, s0
	vldr	d17, .LCPI1_0
	vdiv.f64	d16, d16, d17
	ldr	r0, [r11, #-4]
	movw	lr, :lower16:main.A
	movt	lr, :upper16:main.A
	add	r0, lr, r0, lsl #9
	ldr	lr, [sp, #8]
	add	r0, r0, lr, lsl #3
	vstr	d16, [r0]
	b	.LBB1_5
.LBB1_5:                                @ %for.inc
                                        @   in Loop: Header=BB1_3 Depth=2
.PTM_main_8:
	ldr	r0, [sp, #8]
	add	r0, r0, #1
	str	r0, [sp, #8]
	b	.LBB1_3
.LBB1_6:                                @ %for.end
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_main_9:
	b	.LBB1_7
	b	.LBB1_7
.LBB1_7:                                @ %for.inc6
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_main_10:
	ldr	r0, [r11, #-4]
	add	r0, r0, #1
	str	r0, [r11, #-4]
	b	.LBB1_1
.LBB1_8:                                @ %for.end8
.PTM_main_11:
	movw	r0, :lower16:main.A
	movt	r0, :upper16:main.A
	movw	r1, #64
	bl	Choleski_LU_Decomposition
.PTM_main_12:
	str	r0, [sp]                @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
	.p2align	3
@ BB#9:
.LCPI1_0:
	.long	4290772992              @ double 2147483647
	.long	1105199103
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cantunwind
	.fnend

	.type	main.A,%object          @ @main.A
	.local	main.A
	.comm	main.A,32768,8

	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430520
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.long	.PTM_Choleski_LU_Decomposition_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.long	.PTM_Choleski_LU_Decomposition_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.long	.PTM_Choleski_LU_Decomposition_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	470417408
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.long	.PTM_Choleski_LU_Decomposition_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.long	.PTM_Choleski_LU_Decomposition_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.long	.PTM_Choleski_LU_Decomposition_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	480444416
	.long	478478336
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.long	.PTM_Choleski_LU_Decomposition_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.long	.PTM_Choleski_LU_Decomposition_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	476315648
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_17
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.long	.PTM_Choleski_LU_Decomposition_18
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	476315648
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_Choleski_LU_Decomposition_19
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.long	.PTM_Choleski_LU_Decomposition_20
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430576
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp26:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp26(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp27:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp27(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp28:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp28(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp29:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp29(sbrel)
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp30:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp30(sbrel)
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp31:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp31(sbrel)
	.long	.PTM_main_11
	.section	.HB.annot,"a",%progbits
.Ltmp32:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp32(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_12
	.section	.HB.annot,"a",%progbits
.Ltmp33:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp33(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
