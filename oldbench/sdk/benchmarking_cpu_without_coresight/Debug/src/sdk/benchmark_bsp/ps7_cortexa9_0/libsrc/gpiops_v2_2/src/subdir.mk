################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_g.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_hw.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_intr.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_selftest.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_sinit.c 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_g.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_hw.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_intr.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_selftest.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_sinit.o 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_g.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_hw.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_intr.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_selftest.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/xgpiops_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/gpiops_v2_2/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


