#include <sys/uio.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>


int main() {
  ssize_t bytes_read;
  int fd;
  char buf0[20];
  char buf1[30];
  char buf2[40];
  int iovcnt;
  struct iovec iov[3];


  fd = open("test.txt", O_RDONLY);

  iov[0].iov_base = buf0;
  iov[0].iov_len = sizeof(buf0);
  iov[1].iov_base = buf1;
  iov[1].iov_len = sizeof(buf1);
  iov[2].iov_base = buf2;
  iov[2].iov_len = sizeof(buf2);

  iovcnt = sizeof(iov) / sizeof(struct iovec);

  bytes_read = readv(fd, iov, iovcnt);

  printf("buf0 = %p | size buf0 = %lu\n", buf0, sizeof(buf0));
  printf("buf1 = %p | size buf0 = %lu\n", buf1, sizeof(buf1));
  printf("buf2 = %p | size buf2 = %lu\n", buf2, sizeof(buf2));

  return 0;
}
