	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"mallocFree.c"
	.globl	useAfterFree
	.p2align	2
	.type	useAfterFree,%function
	.code	32                      @ @useAfterFree
useAfterFree:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_useAfterFree_0:
	.pad	#4
	sub	sp, sp, #4
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	str	r0, [sp]
	ldr	r0, [sp]
	movw	r1, :lower16:.L.str
	movt	r1, :upper16:.L.str
	strb	r1, [r0]
	add	sp, sp, #4
	bx	lr
.Lfunc_end0:
	.size	useAfterFree, .Lfunc_end0-useAfterFree
	.cantunwind
	.fnend

	.globl	doubleFree
	.p2align	2
	.type	doubleFree,%function
	.code	32                      @ @doubleFree
doubleFree:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_doubleFree_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#8
	sub	sp, sp, #8
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r1, :lower16:free
	movt	r1, :upper16:free
	str	r0, [sp, #4]
	ldr	r0, [sp, #4]
	blx	r1
.PTM_doubleFree_1:
	str	r0, [sp]                @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end1:
	.size	doubleFree, .Lfunc_end1-doubleFree
	.cantunwind
	.fnend

	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#40
	sub	sp, sp, #40
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC2_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC2_0+8))
.LPC2_0:
	ldr	r9, [pc, r9]
	ldr	r2, .LCPI2_0
	movw	r3, #0
	str	r3, [r11, #-4]
	str	r0, [r11, #-8]
	str	r1, [r11, #-12]
	mov	r0, r2
	bl	malloc
.PTM_main_1:
	ldr	r1, .LCPI2_0
	str	r0, [r11, #-16]
	mov	r0, r1
	bl	malloc
.PTM_main_2:
	str	r0, [sp, #20]
	ldr	r0, [r11, #-8]
	cmp	r0, #1
	bne	.LBB2_2
	b	.LBB2_1
.LBB2_1:                                @ %if.then
.PTM_main_3:
	movw	r0, :lower16:.L.str.1
	movt	r0, :upper16:.L.str.1
	bl	printf
.PTM_main_4:
	movw	lr, :lower16:free
	movt	lr, :upper16:free
	ldr	r1, [r11, #-16]
	str	r0, [sp, #16]           @ 4-byte Spill
	mov	r0, r1
	blx	lr
.PTM_main_5:
	ldr	r1, [r11, #-16]
	str	r0, [sp, #12]           @ 4-byte Spill
	mov	r0, r1
	bl	useAfterFree
	b	.LBB2_2
.LBB2_2:                                @ %if.end
.PTM_main_6:
	ldr	r0, [r11, #-8]
	cmp	r0, #2
	bne	.LBB2_4
	b	.LBB2_3
.LBB2_3:                                @ %if.then5
.PTM_main_7:
	movw	r0, :lower16:.L.str.2
	movt	r0, :upper16:.L.str.2
	bl	printf
.PTM_main_8:
	ldr	lr, [r11, #-16]
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, lr
	bl	doubleFree
.PTM_main_9:
	ldr	r1, .LCPI2_0
	movw	r0, :lower16:munmap
	movt	r0, :upper16:munmap
	ldr	lr, [r11, #-16]
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, lr
	ldr	lr, [sp, #4]            @ 4-byte Reload
	blx	lr
.PTM_main_10:
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB2_4
.LBB2_4:                                @ %if.end8
.PTM_main_11:
	ldr	r0, [r11, #-4]
	mov	sp, r11
	pop	{r11, pc}
	.p2align	2
@ BB#5:
.LCPI2_0:
	.long	262144                  @ 0x40000
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"Hello"
	.size	.L.str, 6

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"useAfterFree"
	.size	.L.str.1, 13

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"doubleFree"
	.size	.L.str.2, 11


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_useAfterFree_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430588
	.long	504365060
	.long	502136832
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_doubleFree_0
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430584
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_doubleFree_1
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430552
	.long	474218496
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.long	.PTM_main_11
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
