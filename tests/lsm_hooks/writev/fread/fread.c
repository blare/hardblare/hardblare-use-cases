#include <sys/uio.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>




int main() {
  FILE *fd;
  int ret;
  char c[200];

  fd = fopen("test.txt", "rw");
  if (!fd) { perror("Error"); exit(1); }

  ret = fread(c, sizeof(char), 10, fd);

  c[ret] = '\0';

  printf("Address of variable = %p\n", c);
  printf("Value of the variable = %s", c);

  return 0;
}
