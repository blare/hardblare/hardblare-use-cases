/*

  CWE-125: Out-of-bounds Read


  Description :
  -------------

  The software reads data past the end, or before the beginning, of the intended buffer.
  Typically, this can allow attackers to read sensitive information from other memory locations or cause a crash. A crash can occur when the code reads a variable amount of data and assumes that a sentinel exists to stop the read operation, such as a NUL in a string. The expected sentinel might not be located in the out-of-bounds memory, causinfg excessive data to be read, leading to a segmentation fault or a buffer overflow. The software may modify an index or perform pointer arithmetic that references a memory location that is outside of the boundaries of the buffer. A subsequent read operation then produces undefined or unexpected results.


  Example :
  ---------

  In the following code, the method retrieves a value from an array at a specific array index location that is given as an input parameter to the method

  However, this method only verifies that the given array index is less than the maximum length of the array but does not check for the minimum value (CWE-839). This will allow a negative value to be accepted as the input array index, which will result in a out of bounds read (CWE-125) and may allow access to sensitive memory. The input array index should be checked to verify that is within the maximum and minimum range required for the array (CWE-129). In this example the if statement should be modified to include a minimum range check, as shown below.


...
// check that the array index is within the correct

// range of values for the array
if (index >= 0 && index < len) {
...


*/

#include <stdio.h>


int getValueFromArray(int *array, int len, int index)
{
  int value;
  
  if (index < len)
    {
      value = array[index];
    }
  else
    {
      value = -1;
    }

  return value;
}


int main(int argc, const char* argv[])
{
  int array[42] = { 42 };
  int x = atoi(argv[1]);
  int result = getValueFromArray(&array, 42, x);
  printf("Value is: %x\n", result);
}
