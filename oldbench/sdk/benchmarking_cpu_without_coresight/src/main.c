#include <stdio.h>
#include <stdlib.h>
#include "benchmark.h"
#include "xtime_l.h"

#define TIMING

#ifdef TIMING
XTime t1,t2;
int EffectiveClockRate;
int Calibration;

#define TIMING_START    XTime_GetTime(&t1)
#define TIMING_STOP     XTime_GetTime(&t2)
#define TIMING_DISPLAY(txt) DisplayTime(txt,t2-t1-Calibration)
#define TIMING_INIT 	InitTiming()
#define NB_TIMES 10
void InitTiming()
{
    /*Calculate Global Timer Counter Consecutive Read overhead*/
    EffectiveClockRate = XPAR_CPU_CORTEXA9_0_CPU_CLK_FREQ_HZ / 2;
    XTime_GetTime(&t1);
    XTime_GetTime(&t2);
    Calibration = t2-t1;

}
void DisplayTime(char *txt,int Nclk)
{
    double Time_s = (double)Nclk/(double)EffectiveClockRate/(double)NB_TIMES;
    double Time_millis = 1e3*Time_s;
    double Time_micros = 1e6*Time_s;
    int MilS_int = (int)Time_millis;
    int MilS_frac = (int)((Time_millis-MilS_int)*1000.0);
    int MicS_int = (int)Time_micros;
    int MicS_frac = (int)((Time_micros-MicS_int)*1000.0);

    xil_printf("---------------------------------------------------------\n\r");
    xil_printf(txt);
    xil_printf("Number of Processor Clock Cycles : %d in %d iterations \n\r",2*Nclk, NB_TIMES);
    xil_printf("Mean Time duration: %d.%03d ms\n\r",MilS_int,MilS_frac);
    xil_printf("Mean Time duration: %d.%03d µs\n\r",MicS_int,MicS_frac);
}

#else
#define TIMING_START
#define TIMING_STOP
#define TIMING_DISPLAY(txt)
#define TIMING_INIT

#endif

#define CHOLESKI
//#define DFT
//#define FFT
//#define FIR
//#define LU
//#define MATRIX
//#define RADIX
//#define WHT
//#define NBODY
//#define CRC


int main()
{
	unsigned int inst;
    //xil_printf("Hello World\n\r");

    TIMING_INIT;
    
    TIMING_START;
    for ( inst = 0; inst < NB_TIMES; inst++ )
    {
		#ifdef CHOLESKI
			app_choleski();
		#endif
		#ifdef DFT
			app_dft();
		#endif
		#ifdef FFT
			app_fft();
		#endif
		#ifdef FIR
			app_fir();
		#endif
		#ifdef LU
			app_lu();
		#endif
		#ifdef MATRIX
			app_matrix();
		#endif
		#ifdef RADIX
			app_radix();
		#endif
		#ifdef WHT
			app_wht();
		#endif
		#ifdef NBODY
			app_nbody();
		#endif
		#ifdef CRC
			app_crc();
		#endif

		//xil_printf(".");

    }

    TIMING_STOP;

    
    TIMING_DISPLAY("Without enabling PTM");

/*    xil_printf("valid instr: %d\n", 	instr_val);
    xil_printf(" data access: %d\n", 	data_access);
    xil_printf("  data write: %d\n", 	data_write);
    xil_printf("  data read: %d\n\n", 	data_read);

    xil_printf("dcache request: %d\n", 	data_req);
    xil_printf(" dcache hit: %d\n", 	data_hit);
    xil_printf(" dcache miss: %d\n\n", 	data_req - data_hit);

    xil_printf("icache request: %d\n", 	inst_req);
    xil_printf(" icache hit: %d\n", 	inst_hit);
    xil_printf(" icache miss: %d\n", 	inst_req - inst_hit);
*/
    return 0;
}
