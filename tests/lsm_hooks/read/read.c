#include <sys/uio.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>


int main() {

  int fd, ret;
  char c[50];

  fd = open("test.txt", O_RDONLY);
  if (fd < 0) { perror("Error"); exit(1); }

  ret = read(fd, c, sizeof(c));

  c[ret] = '\0';

  printf("Address of the variable = %p \n", c);
  printf("Value of the variable = %s \n", c);

  free(c);
  close(fd);

}
