/*
 * benchmark.h
 *
 *  Created on: 1 juil. 2011
 */

#ifndef BENCHMARK_H_
#define BENCHMARK_H_

void app_choleski(void);
void app_dft(void);
void app_fft(void);
void app_fir(void);
void app_lu(void);
void app_matrix(void);
void app_radix(void);
void app_wht(void);
void app_nbody(void);
void app_crc(void);

#endif /* BENCHMARK_H_ */
