#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 15
#define K 8

void FIRFilter( double *in, double *out, double *kernel, unsigned int ksize,
		unsigned int n )
{
    unsigned int i, j;
    for( i = 0; i < n - ksize + 1; ++i )
    {
	out[ i ] = 0.0;
	for( j = 0; j < ksize; ++j )
	    out[ i ] += in[ i + j ] * kernel[ j ];
    }
}

void app_fir()
{
	static double in[N];
	static double out[N];
	static double kernel[K];

	unsigned int i;

	srand(time(NULL));

	for ( i = 0; i < N; i++ )
	{
		in[i] = rand()/((double)RAND_MAX);
		out[i] = 0.0;
	}

	for ( i = 0; i < K; i++ )
		kernel[i] = rand()/((double)RAND_MAX);

	FIRFilter( in, out, kernel, K, N);

}
