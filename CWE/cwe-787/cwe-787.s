	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"cwe-787.c"
	.globl	example1
	.p2align	2
	.type	example1,%function
	.code	32                      @ @example1
example1:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_example1_0:
	.pad	#12
	sub	sp, sp, #12
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, #456
	movw	r1, #345
	movw	r2, #234
	movw	r3, #123
	str	r3, [sp]
	str	r2, [sp, #4]
	str	r1, [sp, #8]
	str	r0, [sp, #12]
	add	sp, sp, #12
	bx	lr
.Lfunc_end0:
	.size	example1, .Lfunc_end0-example1
	.cantunwind
	.fnend

	.globl	returnChunkSize
	.p2align	2
	.type	returnChunkSize,%function
	.code	32                      @ @returnChunkSize
returnChunkSize:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_returnChunkSize_0:
	.pad	#4
	sub	sp, sp, #4
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	mvn	r1, #0
	str	r0, [sp]
	mov	r0, r1
	add	sp, sp, #4
	bx	lr
.Lfunc_end1:
	.size	returnChunkSize, .Lfunc_end1-returnChunkSize
	.cantunwind
	.fnend

	.globl	example2
	.p2align	2
	.type	example2,%function
	.code	32                      @ @example2
example2:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_example2_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#24
	sub	sp, sp, #24
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC2_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC2_0+8))
.LPC2_0:
	ldr	r9, [pc, r9]
	sub	r0, r11, #1
	sub	r1, r11, #9
	movw	r2, :lower16:.Lexample2.srcBuf
	movt	r2, :upper16:.Lexample2.srcBuf
	movw	r3, :lower16:.Lexample2.destBuf
	movt	r3, :upper16:.Lexample2.destBuf
	ldrb	r3, [r3]
	strb	r3, [r11, #-1]
	ldrb	r3, [r2]
	strb	r3, [r11, #-9]
	ldrb	r3, [r2, #1]
	strb	r3, [r11, #-8]
	ldrb	r3, [r2, #2]
	strb	r3, [r11, #-7]
	ldrb	r3, [r2, #3]
	strb	r3, [r11, #-6]
	ldrb	r3, [r2, #4]
	strb	r3, [r11, #-5]
	ldrb	r3, [r2, #5]
	strb	r3, [r11, #-4]
	ldrb	r3, [r2, #6]
	strb	r3, [r11, #-3]
	ldrb	r2, [r2, #7]
	strb	r2, [r11, #-2]
	str	r0, [sp, #8]            @ 4-byte Spill
	str	r1, [sp, #4]            @ 4-byte Spill
	bl	returnChunkSize
.PTM_example2_1:
	sub	r2, r0, #1
	ldr	r0, [sp, #8]            @ 4-byte Reload
	ldr	r1, [sp, #4]            @ 4-byte Reload
	bl	memcpy
.PTM_example2_2:
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end2:
	.size	example2, .Lfunc_end2-example2
	.cantunwind
	.fnend

	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#8
	sub	sp, sp, #8
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC3_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC3_0+8))
.LPC3_0:
	ldr	r9, [pc, r9]
	str	r0, [sp, #4]
	str	r1, [sp]
	bl	example1
.PTM_main_1:
	bl	example2
.PTM_main_2:
	movw	r0, #0
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cantunwind
	.fnend

	.type	.Lexample2.destBuf,%object @ @example2.destBuf
	.section	.rodata,"a",%progbits
.Lexample2.destBuf:
	.byte	65
	.size	.Lexample2.destBuf, 1

	.type	.Lexample2.srcBuf,%object @ @example2.srcBuf
	.section	.rodata.cst8,"aM",%progbits,8
.Lexample2.srcBuf:
	.zero	8,88
	.size	.Lexample2.srcBuf, 8


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_example1_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430580
	.long	504365068
	.long	502136832
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_returnChunkSize_0
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430588
	.long	474152960
	.long	504365060
	.long	502136832
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_example2_0
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430568
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_example2_1
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	478281728
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_example2_2
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430584
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
