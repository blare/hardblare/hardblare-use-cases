#include <sys/uio.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>




int main() {
  int fd, ret;
  char *c = "This is a test for pwrite\n";

  fd = open("test.txt", O_RDWR);
  if (fd < 0) { perror("Error"); exit(1); }

  ret = pwrite(fd, c, sizeof(char) * 7 , sizeof(char) * 5 );


  printf("Address of the variable = %p\n", c);
  printf("Value = %s\n", c);

}
