/*

  CWE-20: Improper Input Validation :
  -----------------------------------

  Description :
  -------------

  The product does not validate or incorrectly validates input that can affect the control flow or data flow of a program.
  When software does not validate input properly, an attacker is able to craft the input in a form that is not expected by the rest of the application. This will lead to parts of the system receiving unintended input, which may result in altered control flow, arbitrary control of a resource, or arbitrary code execution.


  Example :
  --------

  This example asks the user for a height and width of an m X n game board with a maximum dimension of 100 squares.
  While this code checks to make sure the user cannot specify large, positive integers and consume too much memory, it does not check for negative values supplied by the user. As a result, an attacker can perform a resource consumption (CWE-400) attack against this program by specifying two, large negative values that will not overflow, resulting in a very large memory allocation (CWE-789) and possibly a system crash. Alternatively, an attacker can provide very large negative values which will cause an integer overflow (CWE-190) and unexpected behavior will follow depending on how the values are treated in the remainder of the program.

 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_DIM 100
/* board dimensions */


typedef struct
{
  int x;
} board_square_t;


int main()
{
  int m,n, error;
  board_square_t *board;
  printf("Please specify the board height: \n");
  error = scanf("%d", &m);
  if ( EOF == error ){
    // die("No integer passed: Die evil hacker!\n");
  }
  printf("Please specify the board width: \n");
  error = scanf("%d", &n);
  if ( EOF == error ){
    //die("No integer passed: Die evil hacker!\n");
  }
  if ( m > MAX_DIM || n > MAX_DIM ) {
    //die("Value too large: Die evil hacker!\n");
  }

  board = (board_square_t*) malloc( m * n * sizeof(board_square_t));

}
