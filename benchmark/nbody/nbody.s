	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"nbody.c"
	.globl	main
	.p2align	3
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r4, r10, r11, lr}
	push	{r4, r10, r11, lr}
	.setfp	r11, sp, #8
	add	r11, sp, #8
	.pad	#152
	sub	sp, sp, #152
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:main.init
	movt	r0, :upper16:main.init
	vldr	d16, .LCPI0_3
	vstr	d16, [sp, #64]
	vstr	d16, [sp, #56]
	ldr	r0, [r0]
	cmp	r0, #0
	bne	.LBB0_6
	b	.LBB0_1
.LBB0_1:                                @ %if.then
.PTM_main_1:
	movw	r0, #0
	bl	time
.PTM_main_2:
	bl	srand
.PTM_main_3:
	movw	r0, #0
	str	r0, [r11, #-12]
	b	.LBB0_2
.LBB0_2:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_4:
	ldr	r0, [r11, #-12]
	cmp	r0, #42
	bhs	.LBB0_5
	b	.LBB0_3
.LBB0_3:                                @ %for.body
                                        @   in Loop: Header=BB0_2 Depth=1
.PTM_main_5:
	vldr	d16, .LCPI0_4
	movw	r0, :lower16:main.vz
	movt	r0, :upper16:main.vz
	movw	r1, #3
	vstr	d16, [sp, #48]          @ 8-byte Spill
	str	r0, [sp, #44]           @ 4-byte Spill
	str	r1, [sp, #40]           @ 4-byte Spill
	bl	rand
.PTM_main_6:
	vmov	s0, r0
	vcvt.f64.s32	d16, s0
	vldr	d17, .LCPI0_5
	vdiv.f64	d16, d16, d17
	vadd.f64	d16, d16, d16
	vmov.f64	d18, #1.000000e+00
	vsub.f64	d16, d16, d18
	ldr	r0, [r11, #-12]
	movw	r1, :lower16:main.x
	movt	r1, :upper16:main.x
	add	r0, r1, r0, lsl #3
	vstr	d16, [r0]
	vstr	d17, [sp, #32]          @ 8-byte Spill
	vstr	d18, [sp, #24]          @ 8-byte Spill
	bl	rand
.PTM_main_7:
	vmov	s0, r0
	vcvt.f64.s32	d16, s0
	vldr	d17, [sp, #32]          @ 8-byte Reload
	vdiv.f64	d16, d16, d17
	vadd.f64	d16, d16, d16
	vldr	d18, [sp, #24]          @ 8-byte Reload
	vsub.f64	d16, d16, d18
	ldr	r0, [r11, #-12]
	movw	r1, :lower16:main.y
	movt	r1, :upper16:main.y
	add	r0, r1, r0, lsl #3
	vstr	d16, [r0]
	bl	rand
.PTM_main_8:
	vmov	s0, r0
	vcvt.f64.s32	d16, s0
	vldr	d17, [sp, #32]          @ 8-byte Reload
	vdiv.f64	d16, d16, d17
	vadd.f64	d16, d16, d16
	vldr	d18, [sp, #24]          @ 8-byte Reload
	vsub.f64	d16, d16, d18
	ldr	r0, [r11, #-12]
	movw	r1, :lower16:main.z
	movt	r1, :upper16:main.z
	add	r0, r1, r0, lsl #3
	vstr	d16, [r0]
	bl	rand
.PTM_main_9:
	vmov	s0, r0
	vcvt.f64.s32	d16, s0
	vldr	d17, [sp, #32]          @ 8-byte Reload
	vdiv.f64	d16, d16, d17
	vadd.f64	d16, d16, d16
	vldr	d18, [sp, #24]          @ 8-byte Reload
	vsub.f64	d16, d16, d18
	ldr	r0, [r11, #-12]
	movw	r1, :lower16:main.m
	movt	r1, :upper16:main.m
	add	r0, r1, r0, lsl #3
	vstr	d16, [r0]
	ldr	r0, [r11, #-12]
	movw	r1, :lower16:main.xnew
	movt	r1, :upper16:main.xnew
	mov	lr, #0
	str	lr, [r1, r0, lsl #3]!
	str	lr, [r1, #4]
	ldr	r0, [r11, #-12]
	movw	r1, :lower16:main.ynew
	movt	r1, :upper16:main.ynew
	str	lr, [r1, r0, lsl #3]!
	str	lr, [r1, #4]
	ldr	r0, [r11, #-12]
	movw	r1, :lower16:main.znew
	movt	r1, :upper16:main.znew
	str	lr, [r1, r0, lsl #3]!
	str	lr, [r1, #4]
	ldr	r0, [r11, #-12]
	movw	r1, :lower16:main.vx
	movt	r1, :upper16:main.vx
	str	lr, [r1, r0, lsl #3]!
	str	lr, [r1, #4]
	ldr	r0, [r11, #-12]
	movw	r1, :lower16:main.vy
	movt	r1, :upper16:main.vy
	str	lr, [r1, r0, lsl #3]!
	str	lr, [r1, #4]
	ldr	r0, [r11, #-12]
	movw	r1, :lower16:main.vz
	movt	r1, :upper16:main.vz
	add	r0, r1, r0, lsl #3
	vldr	d16, [sp, #48]          @ 8-byte Reload
	vstr	d16, [r0]
	b	.LBB0_4
.LBB0_4:                                @ %for.inc
                                        @   in Loop: Header=BB0_2 Depth=1
.PTM_main_10:
	ldr	r0, [r11, #-12]
	add	r0, r0, #1
	str	r0, [r11, #-12]
	b	.LBB0_2
.LBB0_5:                                @ %for.end
.PTM_main_11:
	movw	r0, #1
	movw	r1, :lower16:main.init
	movt	r1, :upper16:main.init
	str	r0, [r1]
	b	.LBB0_6
.LBB0_6:                                @ %if.end
.PTM_main_12:
	movw	r0, #0
	str	r0, [r11, #-12]
	b	.LBB0_7
.LBB0_7:                                @ %for.cond26
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_9 Depth 2
.PTM_main_13:
	ldr	r0, [r11, #-12]
	cmp	r0, #42
	bhs	.LBB0_17
	b	.LBB0_8
.LBB0_8:                                @ %for.body29
                                        @   in Loop: Header=BB0_7 Depth=1
.PTM_main_14:
	movw	r0, #0
	vldr	d16, .LCPI0_1
	vstr	d16, [r11, #-24]
	vstr	d16, [r11, #-32]
	vstr	d16, [r11, #-40]
	str	r0, [r11, #-16]
	b	.LBB0_9
.LBB0_9:                                @ %for.cond30
                                        @   Parent Loop BB0_7 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_main_15:
	ldr	r0, [r11, #-16]
	cmp	r0, #42
	bhs	.LBB0_15
	b	.LBB0_10
.LBB0_10:                               @ %for.body33
                                        @   in Loop: Header=BB0_9 Depth=2
.PTM_main_16:
	movw	r0, :lower16:main.m
	movt	r0, :upper16:main.m
	movw	r1, #3
	ldr	r2, [r11, #-16]
	movw	r3, :lower16:main.x
	movt	r3, :upper16:main.x
	add	r2, r3, r2, lsl #3
	vldr	d16, [r2]
	ldr	r2, [r11, #-12]
	add	r2, r3, r2, lsl #3
	vldr	d17, [r2]
	vsub.f64	d16, d16, d17
	vstr	d16, [r11, #-48]
	ldr	r2, [r11, #-16]
	movw	r3, :lower16:main.y
	movt	r3, :upper16:main.y
	add	r2, r3, r2, lsl #3
	vldr	d16, [r2]
	ldr	r2, [r11, #-12]
	add	r2, r3, r2, lsl #3
	vldr	d17, [r2]
	vsub.f64	d16, d16, d17
	vstr	d16, [r11, #-56]
	ldr	r2, [r11, #-16]
	movw	r3, :lower16:main.z
	movt	r3, :upper16:main.z
	add	r2, r3, r2, lsl #3
	vldr	d16, [r2]
	ldr	r2, [r11, #-12]
	add	r2, r3, r2, lsl #3
	vldr	d17, [r2]
	vsub.f64	d16, d16, d17
	vstr	d16, [r11, #-64]
	vldr	d16, [r11, #-48]
	vldr	d17, [r11, #-56]
	vmul.f64	d17, d17, d17
	vmla.f64	d17, d16, d16
	vldr	d16, [r11, #-64]
	vmla.f64	d17, d16, d16
	vldr	d16, [sp, #64]
	vadd.f64	d0, d17, d16
	str	r0, [sp, #20]           @ 4-byte Spill
	str	r1, [sp, #16]           @ 4-byte Spill
	bl	sqrt
.PTM_main_17:
	vmov.f64	d16, #1.000000e+00
	vdiv.f64	d16, d16, d0
	vstr	d16, [r11, #-72]
	vldr	d16, [r11, #-72]
	vmul.f64	d17, d16, d16
	vmul.f64	d16, d17, d16
	vstr	d16, [sp, #80]
	ldr	r0, [r11, #-16]
	movw	r1, :lower16:main.m
	movt	r1, :upper16:main.m
	add	r0, r1, r0, lsl #3
	vldr	d16, [r0]
	vldr	d17, [sp, #80]
	vmul.f64	d16, d16, d17
	vstr	d16, [sp, #72]
	vldr	d16, [sp, #72]
	vldr	d17, [r11, #-48]
	vmul.f64	d16, d16, d17
	vldr	d17, [r11, #-24]
	vadd.f64	d16, d17, d16
	vstr	d16, [r11, #-24]
	vldr	d16, [sp, #72]
	vldr	d17, [r11, #-56]
	vmul.f64	d16, d16, d17
	vldr	d17, [r11, #-32]
	vadd.f64	d16, d17, d16
	vstr	d16, [r11, #-32]
	vldr	d16, [sp, #72]
	vldr	d17, [r11, #-48]
	vmul.f64	d16, d16, d17
	vldr	d17, [r11, #-40]
	vadd.f64	d16, d17, d16
	vstr	d16, [r11, #-40]
	b	.LBB0_11
.LBB0_11:                               @ %for.inc60
                                        @   in Loop: Header=BB0_9 Depth=2
.PTM_main_18:
	ldr	r0, [r11, #-16]
	add	r0, r0, #1
	str	r0, [r11, #-16]
	b	.LBB0_9
	.p2align	3
@ BB#12:
.LCPI0_3:
	.long	3539053052              @ double 0.001
	.long	1062232653
	.p2align	3
@ BB#13:
.LCPI0_4:
	.long	0                       @ double 0
	.long	0
	.p2align	3
@ BB#14:
.LCPI0_5:
	.long	4290772992              @ double 2147483647
	.long	1105199103
.LBB0_15:                               @ %for.end62
                                        @   in Loop: Header=BB0_7 Depth=1
.PTM_main_19:
	movw	r0, :lower16:main.vz
	movt	r0, :upper16:main.vz
	movw	r1, #3
	ldr	r2, [r11, #-12]
	movw	r3, :lower16:main.x
	movt	r3, :upper16:main.x
	add	r3, r3, r2, lsl #3
	vldr	d16, [r3]
	vldr	d17, [sp, #56]
	movw	r3, :lower16:main.vx
	movt	r3, :upper16:main.vx
	add	r12, r3, r2, lsl #3
	vldr	d18, [r12]
	vmla.f64	d16, d17, d18
	vmov.f64	d18, #5.000000e-01
	vmul.f64	d19, d17, d18
	vmul.f64	d17, d19, d17
	vldr	d19, [r11, #-24]
	vmla.f64	d16, d17, d19
	movw	r12, :lower16:main.xnew
	movt	r12, :upper16:main.xnew
	add	r2, r12, r2, lsl #3
	vstr	d16, [r2]
	ldr	r2, [r11, #-12]
	movw	r12, :lower16:main.y
	movt	r12, :upper16:main.y
	add	r12, r12, r2, lsl #3
	vldr	d16, [r12]
	vldr	d17, [sp, #56]
	movw	r12, :lower16:main.vy
	movt	r12, :upper16:main.vy
	add	lr, r12, r2, lsl #3
	vldr	d19, [lr]
	vmla.f64	d16, d17, d19
	vmul.f64	d19, d17, d18
	vmul.f64	d17, d19, d17
	vldr	d19, [r11, #-32]
	vmla.f64	d16, d17, d19
	movw	lr, :lower16:main.ynew
	movt	lr, :upper16:main.ynew
	add	r2, lr, r2, lsl #3
	vstr	d16, [r2]
	ldr	r2, [r11, #-12]
	movw	lr, :lower16:main.z
	movt	lr, :upper16:main.z
	add	lr, lr, r2, lsl #3
	vldr	d16, [lr]
	vldr	d17, [sp, #56]
	movw	lr, :lower16:main.vz
	movt	lr, :upper16:main.vz
	add	r4, lr, r2, lsl #3
	vldr	d19, [r4]
	vmla.f64	d16, d17, d19
	vmul.f64	d18, d17, d18
	vmul.f64	d17, d18, d17
	vldr	d18, [r11, #-40]
	vmla.f64	d16, d17, d18
	movw	r4, :lower16:main.znew
	movt	r4, :upper16:main.znew
	add	r2, r4, r2, lsl #3
	vstr	d16, [r2]
	vldr	d16, [sp, #56]
	vldr	d17, [r11, #-24]
	ldr	r2, [r11, #-12]
	add	r2, r3, r2, lsl #3
	vldr	d18, [r2]
	vmla.f64	d18, d16, d17
	vstr	d18, [r2]
	vldr	d16, [sp, #56]
	vldr	d17, [r11, #-32]
	ldr	r2, [r11, #-12]
	add	r2, r12, r2, lsl #3
	vldr	d18, [r2]
	vmla.f64	d18, d16, d17
	vstr	d18, [r2]
	vldr	d16, [sp, #56]
	vldr	d17, [r11, #-40]
	vmul.f64	d16, d16, d17
	ldr	r2, [r11, #-12]
	add	r2, lr, r2, lsl #3
	vldr	d17, [r2]
	vadd.f64	d16, d17, d16
	vstr	d16, [r2]
	str	r0, [sp, #12]           @ 4-byte Spill
	str	r1, [sp, #8]            @ 4-byte Spill
	b	.LBB0_16
.LBB0_16:                               @ %for.inc99
                                        @   in Loop: Header=BB0_7 Depth=1
.PTM_main_20:
	ldr	r0, [r11, #-12]
	add	r0, r0, #1
	str	r0, [r11, #-12]
	b	.LBB0_7
.LBB0_17:                               @ %for.end101
.PTM_main_21:
	movw	r0, #0
	str	r0, [r11, #-12]
	b	.LBB0_18
.LBB0_18:                               @ %for.cond102
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_22:
	ldr	r0, [r11, #-12]
	cmp	r0, #42
	bhs	.LBB0_21
	b	.LBB0_19
.LBB0_19:                               @ %for.body105
                                        @   in Loop: Header=BB0_18 Depth=1
.PTM_main_23:
	movw	r0, :lower16:main.z
	movt	r0, :upper16:main.z
	movw	r1, #3
	ldr	r2, [r11, #-12]
	movw	r3, :lower16:main.xnew
	movt	r3, :upper16:main.xnew
	add	r3, r3, r2, lsl #3
	vldr	d16, [r3]
	movw	r3, :lower16:main.x
	movt	r3, :upper16:main.x
	add	r2, r3, r2, lsl #3
	vstr	d16, [r2]
	ldr	r2, [r11, #-12]
	movw	r3, :lower16:main.ynew
	movt	r3, :upper16:main.ynew
	add	r3, r3, r2, lsl #3
	vldr	d16, [r3]
	movw	r3, :lower16:main.y
	movt	r3, :upper16:main.y
	add	r2, r3, r2, lsl #3
	vstr	d16, [r2]
	ldr	r2, [r11, #-12]
	movw	r3, :lower16:main.znew
	movt	r3, :upper16:main.znew
	add	r3, r3, r2, lsl #3
	vldr	d16, [r3]
	movw	r3, :lower16:main.z
	movt	r3, :upper16:main.z
	add	r2, r3, r2, lsl #3
	vstr	d16, [r2]
	str	r0, [sp, #4]            @ 4-byte Spill
	str	r1, [sp]                @ 4-byte Spill
	b	.LBB0_20
.LBB0_20:                               @ %for.inc112
                                        @   in Loop: Header=BB0_18 Depth=1
.PTM_main_24:
	ldr	r0, [r11, #-12]
	add	r0, r0, #1
	str	r0, [r11, #-12]
	b	.LBB0_18
.LBB0_21:                               @ %for.end114
.PTM_main_25:
	sub	sp, r11, #8
	pop	{r4, r10, r11, pc}
	.p2align	3
@ BB#22:
.LCPI0_1:
	.long	0                       @ double 0
	.long	0
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend

	.type	main.init,%object       @ @main.init
	.local	main.init
	.comm	main.init,4,4
	.type	main.x,%object          @ @main.x
	.local	main.x
	.comm	main.x,336,8
	.type	main.y,%object          @ @main.y
	.local	main.y
	.comm	main.y,336,8
	.type	main.z,%object          @ @main.z
	.local	main.z
	.comm	main.z,336,8
	.type	main.m,%object          @ @main.m
	.local	main.m
	.comm	main.m,336,8
	.type	main.xnew,%object       @ @main.xnew
	.local	main.xnew
	.comm	main.xnew,336,8
	.type	main.ynew,%object       @ @main.ynew
	.local	main.ynew
	.comm	main.ynew,336,8
	.type	main.znew,%object       @ @main.znew
	.local	main.znew
	.comm	main.znew,336,8
	.type	main.vx,%object         @ @main.vx
	.local	main.vx
	.comm	main.vx,336,8
	.type	main.vy,%object         @ @main.vy
	.local	main.vy
	.comm	main.vy,336,8
	.type	main.vz,%object         @ @main.vz
	.local	main.vz
	.comm	main.vz,336,8

	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430440
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	474152960
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.long	.PTM_main_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.long	.PTM_main_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.long	.PTM_main_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.long	.PTM_main_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	7
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	478478336
	.long	478478336
	.long	478478336
	.long	478478336
	.long	478478336
	.long	478478336
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_17
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_18
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.long	.PTM_main_19
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	9
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	499449856
	.long	479068160
	.long	491651072
	.long	478806016
	.long	483000320
	.long	478543872
	.long	478478336
	.long	479068160
	.long	478806016
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_20
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.long	.PTM_main_21
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.long	.PTM_main_22
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_23
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	478478336
	.long	478478336
	.long	478478336
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_24
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.long	.PTM_main_25
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430584

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
