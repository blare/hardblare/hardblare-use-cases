Author : Mounir NASR ALLAH
Date : 20/02/2020

Test instrumentation FIFO : OK

Description: Testing that the instrumentation FIFO is working correctly, we send some defined value through the address stored in r9 (instrumentation FIFO) and checking if the same values are read from the Dispatcher via the firmware.



Code :

```assembly

  // Tests instrumentation FIFO
  movw	r8, #42
  str r8, [r9]

  movw	r8, #52
  str r8, [r9]

  movw	r8, #62
  str r8, [r9]

  movw	r8, #72
  str r8, [r9]

  movw	r8, #82
  str r8, [r9]
  
```





Results :

```
Address b6f8c79c = 	 2200000 
Address b6f8c7a0 = 	 bee93c58 
Address b6f8c7a4 = 	 2200000 
Address b6f8c7a8 = 	 2a 
Address b6f8c7ac = 	 2200000 
Address b6f8c7b0 = 	 34 
Address b6f8c7b4 = 	 2200000 
Address b6f8c7b8 = 	 3e 
Address b6f8c7bc = 	 2200000 
Address b6f8c7c0 = 	 48 
Address b6f8c7c4 = 	 3c00000 
Address b6f8c7c8 = 	 52 

```