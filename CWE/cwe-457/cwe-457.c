#include <stdio.h>

int main(int argc, char *argv[])
{
  int a, b;

  switch (argc) {
  case 0:
    a = 42;
    b = 42;
    break;

  case 1:
    a = 43;
    b = 43;
    break;

  default:
    a = 3;
    a = 3;
    break;
  }
  printf("a = %d, b = %d \n", a, b);
}
