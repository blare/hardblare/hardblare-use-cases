	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"sql.c"
	.globl	check_integrity_word
	.p2align	2
	.type	check_integrity_word,%function
	.code	32                      @ @check_integrity_word
check_integrity_word:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_check_integrity_word_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#96
	sub	sp, sp, #96
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r1, :lower16:.L.str
	movt	r1, :upper16:.L.str
	movw	r2, #6
	str	r0, [r11, #-8]
	ldr	r0, [r11, #-8]
	str	r0, [r11, #-12]         @ 4-byte Spill
	mov	r0, r1
	ldr	r1, [r11, #-12]         @ 4-byte Reload
	bl	strncmp
.PTM_check_integrity_word_1:
	cmp	r0, #0
	beq	.LBB0_2
	b	.LBB0_1
.LBB0_1:                                @ %if.then
.PTM_check_integrity_word_2:
	movw	r0, :lower16:.L.str.1
	movt	r0, :upper16:.L.str.1
	sub	r2, r11, #8
	ldr	r1, [r11, #-8]
	ldr	r3, [r11, #-8]
	str	r0, [r11, #-16]         @ 4-byte Spill
	mov	r0, r3
	str	r1, [r11, #-20]         @ 4-byte Spill
	str	r2, [r11, #-24]         @ 4-byte Spill
	bl	strlen
.PTM_check_integrity_word_3:
	ldr	r1, [r11, #-16]         @ 4-byte Reload
	str	r0, [r11, #-28]         @ 4-byte Spill
	mov	r0, r1
	ldr	r1, [r11, #-20]         @ 4-byte Reload
	ldr	r2, [r11, #-24]         @ 4-byte Reload
	ldr	r3, [r11, #-28]         @ 4-byte Reload
	bl	printf
.PTM_check_integrity_word_4:
	str	r0, [r11, #-32]         @ 4-byte Spill
	b	.LBB0_12
.LBB0_2:                                @ %if.else
.PTM_check_integrity_word_5:
	movw	r0, :lower16:.L.str.2
	movt	r0, :upper16:.L.str.2
	movw	r2, #6
	ldr	r1, [r11, #-8]
	bl	strncmp
.PTM_check_integrity_word_6:
	cmp	r0, #0
	beq	.LBB0_4
	b	.LBB0_3
.LBB0_3:                                @ %if.then5
.PTM_check_integrity_word_7:
	movw	r0, :lower16:.L.str.1
	movt	r0, :upper16:.L.str.1
	sub	r2, r11, #8
	ldr	r1, [r11, #-8]
	ldr	r3, [r11, #-8]
	str	r0, [r11, #-36]         @ 4-byte Spill
	mov	r0, r3
	str	r1, [r11, #-40]         @ 4-byte Spill
	str	r2, [r11, #-44]         @ 4-byte Spill
	bl	strlen
.PTM_check_integrity_word_8:
	ldr	r1, [r11, #-36]         @ 4-byte Reload
	str	r0, [sp, #48]           @ 4-byte Spill
	mov	r0, r1
	ldr	r1, [r11, #-40]         @ 4-byte Reload
	ldr	r2, [r11, #-44]         @ 4-byte Reload
	ldr	r3, [sp, #48]           @ 4-byte Reload
	bl	printf
.PTM_check_integrity_word_9:
	str	r0, [sp, #44]           @ 4-byte Spill
	b	.LBB0_11
.LBB0_4:                                @ %if.else8
.PTM_check_integrity_word_10:
	movw	r0, :lower16:.L.str.3
	movt	r0, :upper16:.L.str.3
	movw	r2, #2
	ldr	r1, [r11, #-8]
	bl	strncmp
.PTM_check_integrity_word_11:
	cmp	r0, #0
	beq	.LBB0_6
	b	.LBB0_5
.LBB0_5:                                @ %if.then11
.PTM_check_integrity_word_12:
	movw	r0, :lower16:.L.str.1
	movt	r0, :upper16:.L.str.1
	sub	r2, r11, #8
	ldr	r1, [r11, #-8]
	ldr	r3, [r11, #-8]
	str	r0, [sp, #40]           @ 4-byte Spill
	mov	r0, r3
	str	r1, [sp, #36]           @ 4-byte Spill
	str	r2, [sp, #32]           @ 4-byte Spill
	bl	strlen
.PTM_check_integrity_word_13:
	ldr	r1, [sp, #40]           @ 4-byte Reload
	str	r0, [sp, #28]           @ 4-byte Spill
	mov	r0, r1
	ldr	r1, [sp, #36]           @ 4-byte Reload
	ldr	r2, [sp, #32]           @ 4-byte Reload
	ldr	r3, [sp, #28]           @ 4-byte Reload
	bl	printf
.PTM_check_integrity_word_14:
	str	r0, [sp, #24]           @ 4-byte Spill
	b	.LBB0_10
.LBB0_6:                                @ %if.else14
.PTM_check_integrity_word_15:
	movw	r0, :lower16:.L.str.4
	movt	r0, :upper16:.L.str.4
	movw	r2, #1
	ldr	r1, [r11, #-8]
	bl	strncmp
.PTM_check_integrity_word_16:
	cmp	r0, #0
	beq	.LBB0_8
	b	.LBB0_7
.LBB0_7:                                @ %if.then17
.PTM_check_integrity_word_17:
	movw	r0, :lower16:.L.str.1
	movt	r0, :upper16:.L.str.1
	sub	r2, r11, #8
	ldr	r1, [r11, #-8]
	ldr	r3, [r11, #-8]
	str	r0, [sp, #20]           @ 4-byte Spill
	mov	r0, r3
	str	r1, [sp, #16]           @ 4-byte Spill
	str	r2, [sp, #12]           @ 4-byte Spill
	bl	strlen
.PTM_check_integrity_word_18:
	ldr	r1, [sp, #20]           @ 4-byte Reload
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, r1
	ldr	r1, [sp, #16]           @ 4-byte Reload
	ldr	r2, [sp, #12]           @ 4-byte Reload
	ldr	r3, [sp, #8]            @ 4-byte Reload
	bl	printf
.PTM_check_integrity_word_19:
	str	r0, [sp, #4]            @ 4-byte Spill
	b	.LBB0_9
.LBB0_8:                                @ %if.else20
.PTM_check_integrity_word_20:
	b	.LBB0_9
	b	.LBB0_9
.LBB0_9:                                @ %if.end
.PTM_check_integrity_word_21:
	b	.LBB0_10
	b	.LBB0_10
.LBB0_10:                               @ %if.end21
.PTM_check_integrity_word_22:
	b	.LBB0_11
	b	.LBB0_11
.LBB0_11:                               @ %if.end22
.PTM_check_integrity_word_23:
	b	.LBB0_12
	b	.LBB0_12
.LBB0_12:                               @ %if.end23
.PTM_check_integrity_word_24:
	ldr	r0, [r11, #-4]
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	check_integrity_word, .Lfunc_end0-check_integrity_word
	.cantunwind
	.fnend

	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r4, r10, r11, lr}
	push	{r4, r10, r11, lr}
	.setfp	r11, sp, #8
	add	r11, sp, #8
	.pad	#448
	sub	sp, sp, #448
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r2, #100
	movw	r3, :lower16:stdin
	movt	r3, :upper16:stdin
	sub	r12, r11, #120
	movw	lr, :lower16:.L.str.5
	movt	lr, :upper16:.L.str.5
	movw	r4, #0
	str	r4, [r11, #-12]
	str	r0, [r11, #-16]
	str	r1, [r11, #-20]
	str	lr, [sp, #28]
	ldr	r0, [r3]
	str	r0, [sp, #24]           @ 4-byte Spill
	mov	r0, r12
	mov	r1, r2
	ldr	r2, [sp, #24]           @ 4-byte Reload
	bl	fgets
.PTM_main_1:
	movw	r1, :lower16:.L.str.6
	movt	r1, :upper16:.L.str.6
	sub	r2, r11, #120
	add	r3, sp, #36
	str	r0, [sp, #20]           @ 4-byte Spill
	mov	r0, r3
	bl	sprintf
.PTM_main_2:
	movw	r1, :lower16:.L.str.7
	movt	r1, :upper16:.L.str.7
	add	r2, sp, #36
	str	r0, [sp, #16]           @ 4-byte Spill
	mov	r0, r1
	mov	r1, r2
	bl	printf
.PTM_main_3:
	add	r1, sp, #36
	ldr	r2, [sp, #28]
	str	r0, [sp, #12]           @ 4-byte Spill
	mov	r0, r1
	mov	r1, r2
	bl	strtok
.PTM_main_4:
	str	r0, [sp, #32]
	b	.LBB1_1
.LBB1_1:                                @ %while.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_5:
	movw	r0, #0
	ldr	r1, [sp, #32]
	cmp	r1, r0
	beq	.LBB1_3
	b	.LBB1_2
.LBB1_2:                                @ %while.body
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_main_6:
	movw	r0, :lower16:.L.str.8
	movt	r0, :upper16:.L.str.8
	add	r1, sp, #32
	ldr	r2, [sp, #32]
	bl	printf
.PTM_main_7:
	ldr	r1, [sp, #32]
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, r1
	bl	check_integrity_word
.PTM_main_8:
	movw	r1, #0
	ldr	r2, [sp, #28]
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, r1
	mov	r1, r2
	bl	strtok
.PTM_main_9:
	str	r0, [sp, #32]
	b	.LBB1_1
.LBB1_3:                                @ %while.end
.PTM_main_10:
	ldr	r0, [r11, #-12]
	sub	sp, r11, #8
	pop	{r4, r10, r11, pc}
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"SELECT"
	.size	.L.str, 7

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"Check %s , pointer = %p , len %d\n"
	.size	.L.str.1, 34

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"CREATE"
	.size	.L.str.2, 7

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"OR"
	.size	.L.str.3, 3

	.type	.L.str.4,%object        @ @.str.4
.L.str.4:
	.asciz	"="
	.size	.L.str.4, 2

	.type	.L.str.5,%object        @ @.str.5
.L.str.5:
	.asciz	" "
	.size	.L.str.5, 2

	.type	.L.str.6,%object        @ @.str.6
.L.str.6:
	.asciz	"SELECT * FROM users WHERE name = 'admin' AND password = '%s'"
	.size	.L.str.6, 61

	.type	.L.str.7,%object        @ @.str.7
.L.str.7:
	.asciz	"%s\n"
	.size	.L.str.7, 4

	.type	.L.str.8,%object        @ @.str.8
.L.str.8:
	.asciz	"%p = %s\n"
	.size	.L.str.8, 9


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430496
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474284032
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.long	.PTM_check_integrity_word_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474284032
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.long	.PTM_check_integrity_word_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474284032
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.long	.PTM_check_integrity_word_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_17
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474284032
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_18
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_check_integrity_word_19
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.long	.PTM_check_integrity_word_20
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.long	.PTM_check_integrity_word_21
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.long	.PTM_check_integrity_word_22
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.long	.PTM_check_integrity_word_23
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.long	.PTM_check_integrity_word_24
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430144
	.long	474873856
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp26:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp26(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474284032
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp27:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp27(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp28:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp28(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp29:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp29(sbrel)
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp30:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp30(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp31:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp31(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp32:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp32(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp33:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp33(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp34:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp34(sbrel)
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp35:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp35(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430584

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
