	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"matrix.c"
	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#48
	sub	sp, sp, #48
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, #0
	movw	r1, #32
	str	r1, [r11, #-16]
	str	r1, [r11, #-20]
	str	r1, [sp, #24]
	str	r1, [sp, #20]
	bl	time
.PTM_main_1:
	bl	srand
.PTM_main_2:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB0_1
.LBB0_1:                                @ %for.cond
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_3 Depth 2
.PTM_main_3:
	ldr	r0, [r11, #-4]
	ldr	r1, [r11, #-16]
	cmp	r0, r1
	bhs	.LBB0_8
	b	.LBB0_2
.LBB0_2:                                @ %for.body
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_4:
	movw	r0, #0
	str	r0, [r11, #-8]
	b	.LBB0_3
.LBB0_3:                                @ %for.cond1
                                        @   Parent Loop BB0_1 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_main_5:
	ldr	r0, [r11, #-8]
	ldr	r1, [r11, #-20]
	cmp	r0, r1
	bhs	.LBB0_6
	b	.LBB0_4
.LBB0_4:                                @ %for.body3
                                        @   in Loop: Header=BB0_3 Depth=2
.PTM_main_6:
	movw	r0, #2
	str	r0, [sp, #16]           @ 4-byte Spill
	bl	rand
.PTM_main_7:
	asr	lr, r0, #31
	add	lr, r0, lr, lsr #24
	bfc	lr, #0, #8
	sub	r0, r0, lr
	ldr	lr, [r11, #-4]
	movw	r1, :lower16:main.m1
	movt	r1, :upper16:main.m1
	add	r1, r1, lr, lsl #7
	ldr	lr, [r11, #-8]
	add	r1, r1, lr, lsl #2
	str	r0, [r1]
	b	.LBB0_5
.LBB0_5:                                @ %for.inc
                                        @   in Loop: Header=BB0_3 Depth=2
.PTM_main_8:
	ldr	r0, [r11, #-8]
	add	r0, r0, #1
	str	r0, [r11, #-8]
	b	.LBB0_3
.LBB0_6:                                @ %for.end
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_9:
	b	.LBB0_7
	b	.LBB0_7
.LBB0_7:                                @ %for.inc6
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_main_10:
	ldr	r0, [r11, #-4]
	add	r0, r0, #1
	str	r0, [r11, #-4]
	b	.LBB0_1
.LBB0_8:                                @ %for.end8
.PTM_main_11:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB0_9
.LBB0_9:                                @ %for.cond9
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_11 Depth 2
.PTM_main_12:
	ldr	r0, [r11, #-4]
	ldr	r1, [sp, #24]
	cmp	r0, r1
	bhs	.LBB0_16
	b	.LBB0_10
.LBB0_10:                               @ %for.body11
                                        @   in Loop: Header=BB0_9 Depth=1
.PTM_main_13:
	movw	r0, #0
	str	r0, [r11, #-8]
	b	.LBB0_11
.LBB0_11:                               @ %for.cond12
                                        @   Parent Loop BB0_9 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_main_14:
	ldr	r0, [r11, #-8]
	ldr	r1, [sp, #20]
	cmp	r0, r1
	bhs	.LBB0_14
	b	.LBB0_12
.LBB0_12:                               @ %for.body14
                                        @   in Loop: Header=BB0_11 Depth=2
.PTM_main_15:
	movw	r0, #2
	str	r0, [sp, #12]           @ 4-byte Spill
	bl	rand
.PTM_main_16:
	asr	lr, r0, #31
	add	lr, r0, lr, lsr #24
	bfc	lr, #0, #8
	sub	r0, r0, lr
	ldr	lr, [r11, #-4]
	movw	r1, :lower16:main.m2
	movt	r1, :upper16:main.m2
	add	r1, r1, lr, lsl #7
	ldr	lr, [r11, #-8]
	add	r1, r1, lr, lsl #2
	str	r0, [r1]
	b	.LBB0_13
.LBB0_13:                               @ %for.inc19
                                        @   in Loop: Header=BB0_11 Depth=2
.PTM_main_17:
	ldr	r0, [r11, #-8]
	add	r0, r0, #1
	str	r0, [r11, #-8]
	b	.LBB0_11
.LBB0_14:                               @ %for.end21
                                        @   in Loop: Header=BB0_9 Depth=1
.PTM_main_18:
	b	.LBB0_15
	b	.LBB0_15
.LBB0_15:                               @ %for.inc22
                                        @   in Loop: Header=BB0_9 Depth=1
.PTM_main_19:
	ldr	r0, [r11, #-4]
	add	r0, r0, #1
	str	r0, [r11, #-4]
	b	.LBB0_9
.LBB0_16:                               @ %for.end24
.PTM_main_20:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB0_17
.LBB0_17:                               @ %for.cond25
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_19 Depth 2
                                        @       Child Loop BB0_21 Depth 3
.PTM_main_21:
	ldr	r0, [r11, #-4]
	ldr	r1, [r11, #-16]
	cmp	r0, r1
	bhs	.LBB0_28
	b	.LBB0_18
.LBB0_18:                               @ %for.body27
                                        @   in Loop: Header=BB0_17 Depth=1
.PTM_main_22:
	movw	r0, #0
	str	r0, [r11, #-8]
	b	.LBB0_19
.LBB0_19:                               @ %for.cond28
                                        @   Parent Loop BB0_17 Depth=1
                                        @ =>  This Loop Header: Depth=2
                                        @       Child Loop BB0_21 Depth 3
.PTM_main_23:
	ldr	r0, [r11, #-8]
	ldr	r1, [sp, #20]
	cmp	r0, r1
	bhs	.LBB0_26
	b	.LBB0_20
.LBB0_20:                               @ %for.body30
                                        @   in Loop: Header=BB0_19 Depth=2
.PTM_main_24:
	movw	r0, #0
	movw	r1, #2
	ldr	r2, [r11, #-4]
	movw	r3, :lower16:main.mult
	movt	r3, :upper16:main.mult
	add	r2, r3, r2, lsl #7
	ldr	r3, [r11, #-8]
	add	r2, r2, r3, lsl #2
	str	r0, [r2]
	str	r0, [r11, #-12]
	str	r1, [sp, #8]            @ 4-byte Spill
	b	.LBB0_21
.LBB0_21:                               @ %for.cond33
                                        @   Parent Loop BB0_17 Depth=1
                                        @     Parent Loop BB0_19 Depth=2
                                        @ =>    This Inner Loop Header: Depth=3
.PTM_main_25:
	ldr	r0, [r11, #-12]
	ldr	r1, [r11, #-16]
	cmp	r0, r1
	bhs	.LBB0_24
	b	.LBB0_22
.LBB0_22:                               @ %for.body35
                                        @   in Loop: Header=BB0_21 Depth=3
.PTM_main_26:
	movw	r0, #2
	ldr	r1, [r11, #-4]
	movw	r2, :lower16:main.m1
	movt	r2, :upper16:main.m1
	add	r2, r2, r1, lsl #7
	ldr	r3, [r11, #-12]
	ldr	r2, [r2, r3, lsl #2]
	movw	r12, :lower16:main.m2
	movt	r12, :upper16:main.m2
	add	r3, r12, r3, lsl #7
	ldr	r12, [r11, #-8]
	ldr	r3, [r3, r12, lsl #2]
	mul	r2, r2, r3
	movw	r3, :lower16:main.mult
	movt	r3, :upper16:main.mult
	add	r1, r3, r1, lsl #7
	add	r1, r1, r12, lsl #2
	ldr	r3, [r1]
	add	r2, r3, r2
	str	r2, [r1]
	str	r0, [sp, #4]            @ 4-byte Spill
	b	.LBB0_23
.LBB0_23:                               @ %for.inc42
                                        @   in Loop: Header=BB0_21 Depth=3
.PTM_main_27:
	ldr	r0, [r11, #-12]
	add	r0, r0, #1
	str	r0, [r11, #-12]
	b	.LBB0_21
.LBB0_24:                               @ %for.end44
                                        @   in Loop: Header=BB0_19 Depth=2
.PTM_main_28:
	b	.LBB0_25
	b	.LBB0_25
.LBB0_25:                               @ %for.inc45
                                        @   in Loop: Header=BB0_19 Depth=2
.PTM_main_29:
	ldr	r0, [r11, #-8]
	add	r0, r0, #1
	str	r0, [r11, #-8]
	b	.LBB0_19
.LBB0_26:                               @ %for.end47
                                        @   in Loop: Header=BB0_17 Depth=1
.PTM_main_30:
	b	.LBB0_27
	b	.LBB0_27
.LBB0_27:                               @ %for.inc48
                                        @   in Loop: Header=BB0_17 Depth=1
.PTM_main_31:
	ldr	r0, [r11, #-4]
	add	r0, r0, #1
	str	r0, [r11, #-4]
	b	.LBB0_17
.LBB0_28:                               @ %for.end50
.PTM_main_32:
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend

	.type	main.m1,%object         @ @main.m1
	.local	main.m1
	.comm	main.m1,4096,4
	.type	main.m2,%object         @ @main.m2
	.local	main.m2
	.comm	main.m2,4096,4
	.type	main.mult,%object       @ @main.mult
	.local	main.mult
	.comm	main.mult,4096,4

	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430544
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	490864640
	.long	2357470464
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.long	.PTM_main_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.long	.PTM_main_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.long	.PTM_main_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	490864640
	.long	2357470464
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_17
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.long	.PTM_main_18
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.long	.PTM_main_19
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.long	.PTM_main_20
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.long	.PTM_main_21
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_22
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.long	.PTM_main_23
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_24
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	478478336
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_25
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_26
	.section	.HB.annot,"a",%progbits
.Ltmp26:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp26(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	481165312
	.long	2357740160
	.long	476381184
	.long	2357740160
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_27
	.section	.HB.annot,"a",%progbits
.Ltmp27:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp27(sbrel)
	.long	.PTM_main_28
	.section	.HB.annot,"a",%progbits
.Ltmp28:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp28(sbrel)
	.long	.PTM_main_29
	.section	.HB.annot,"a",%progbits
.Ltmp29:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp29(sbrel)
	.long	.PTM_main_30
	.section	.HB.annot,"a",%progbits
.Ltmp30:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp30(sbrel)
	.long	.PTM_main_31
	.section	.HB.annot,"a",%progbits
.Ltmp31:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp31(sbrel)
	.long	.PTM_main_32
	.section	.HB.annot,"a",%progbits
.Ltmp32:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp32(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
