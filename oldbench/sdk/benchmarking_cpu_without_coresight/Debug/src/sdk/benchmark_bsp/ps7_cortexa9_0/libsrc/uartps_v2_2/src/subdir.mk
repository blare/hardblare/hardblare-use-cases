################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_g.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_hw.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_intr.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_options.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_selftest.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_sinit.c 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_g.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_hw.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_intr.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_options.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_selftest.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_sinit.o 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_g.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_hw.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_intr.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_options.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_selftest.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/xuartps_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/uartps_v2_2/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


