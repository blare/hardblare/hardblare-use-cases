################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/choleski.c \
../src/crc.c \
../src/dft.c \
../src/fft.c \
../src/fir.c \
../src/lu.c \
../src/main.c \
../src/matrix.c \
../src/nbody.c \
../src/radix.c \
../src/wht.c 

OBJS += \
./src/choleski.o \
./src/crc.o \
./src/dft.o \
./src/fft.o \
./src/fir.o \
./src/lu.o \
./src/main.o \
./src/matrix.o \
./src/nbody.o \
./src/radix.o \
./src/wht.o 

C_DEPS += \
./src/choleski.d \
./src/crc.d \
./src/dft.d \
./src/fft.d \
./src/fir.d \
./src/lu.d \
./src/main.d \
./src/matrix.d \
./src/nbody.d \
./src/radix.d \
./src/wht.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_cpu_with_coresight_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


