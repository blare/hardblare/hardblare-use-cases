#include <stdio.h>
#include <stdlib.h>

#define N 48

int Doolittle_LU_Decomposition(double *A, int n)
{
   int i, j, k, p;
   double *p_k, *p_row, *p_col;

//         For each row and column, k = 0, ..., n-1,
//            find the upper triangular matrix elements for row k
//            and if the matrix is non-singular (nonzero diagonal element).
//            find the lower triangular matrix elements for column k.

   for (k = 0, p_k = A; k < n; p_k += n, k++) {
      for (j = k; j < n; j++) {
         for (p = 0, p_col = A; p < k; p_col += n,  p++)
            *(p_k + j) -= *(p_k + p) * *(p_col + j);
      }
      if ( *(p_k + k) == 0.0 ) return -1;
      for (i = k+1, p_row = p_k + n; i < n; p_row += n, i++) {
         for (p = 0, p_col = A; p < k; p_col += n, p++)
            *(p_row + k) -= *(p_row + p) * *(p_col + k);
         *(p_row + k) /= *(p_k + k);
      }
   }
   return 0;
}

void app_lu()
{
	static double A[N][N];

	unsigned int i, j;

	srand(123333314);

	for ( i = 0; i < N; i++ )
		for (  j = 0; j < N; j++ )
			A[i][j] = rand()/((double)RAND_MAX);

	Doolittle_LU_Decomposition(&A[0][0], N);

}
