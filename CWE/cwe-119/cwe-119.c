/*

  CWE-119: Improper Restriction of Operations within the Bounds of a Memory Buffer
  --------------------------------------------------------------------------------


  Description :
  -------------

  The software performs operations on a memory buffer, but it can read from or write to a memory location that is outside of the intended boundary of the buffer.
  Certain languages allow direct addressing of memory locations and do not automatically ensure that these locations are valid for the memory buffer that is being referenced. This can cause read or write operations to be performed on memory locations that may be associated with other variables, data structures, or internal program data.
  As a result, an attacker may be able to execute arbitrary code, alter the intended control flow, read sensitive information, or cause the system to crash.


  Example :
  ---------

  This example applies an encoding procedure to an input string and stores it into a buffer.
  The programmer attempts to encode the ampersand character in the user-controlled string, however the length of the string is validated before the encoding procedure is applied. Furthermore, the programmer assumes encoding expansion will only expand a given character by a factor of 4, while the encoding of the ampersand expands by 5. As a result, when the encoding procedure expands the string it is possible to overflow the destination buffer if the attacker provides a string of many ampersands.


 */

#include <stdio.h>
#define MAX_SIZE 42

char * copy_input(char *user_supplied_string){
  int i, dst_index;
  char *dst_buf = (char*)malloc(4*sizeof(char) * MAX_SIZE);
  if ( MAX_SIZE <= strlen(user_supplied_string) ){
    printf("user string too long, die evil hacker!");
    exit(-1);
  }
  dst_index = 0;
  for ( i = 0; i < strlen(user_supplied_string); i++ ){
    if( '&' == user_supplied_string[i] ){
      dst_buf[dst_index++] = '&';
      dst_buf[dst_index++] = 'a';
      dst_buf[dst_index++] = 'm';
      dst_buf[dst_index++] = 'p';
      dst_buf[dst_index++] = ';';
    }
    else if ('<' == user_supplied_string[i] ){

      /* encode to &lt; */
    }
    else dst_buf[dst_index++] = user_supplied_string[i];
  }
  return dst_buf;
}

int main(int argc, char *argv[])
{
  char* testArg = "Hello World it's me & you !";
  copy_input(testArg);
}
