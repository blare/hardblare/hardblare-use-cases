#include <sys/uio.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>


int main() {

  int fd_input, fd_output, ret;
  char c[50];

  fd_input = open("input.txt", O_RDONLY);
  fd_output = open("output.txt",  O_WRONLY | O_APPEND);

  if (fd_input < 0) {
    perror("Error input.txt \n"); exit(1);
  }

  if (fd_output < 0) {
    perror("Error output.txt \n"); exit(1);
  }

  ret = read(fd_input, c, sizeof(c));
  c[ret] = '\0';

  write(fd_output, c, sizeof(c));

  close(fd_input);
  close(fd_output);

}
