#include <sys/uio.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>


int main() {

  char buf[100];
  FILE* ptr = fopen("test.txt","r");

  if (ptr==NULL)
    {
      printf("no such file.");
      return 0;
    }

  fscanf(ptr,"%s ", buf);
  printf("%s\n", buf);

  return 0;

}
