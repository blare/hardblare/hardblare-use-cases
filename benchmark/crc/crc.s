	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"crc.c"
	.globl	crcInit
	.p2align	2
	.type	crcInit,%function
	.code	32                      @ @crcInit
crcInit:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_crcInit_0:
	.pad	#32
	sub	sp, sp, #32
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, #0
	str	r0, [sp, #24]
	b	.LBB0_1
.LBB0_1:                                @ %for.cond
                                        @ =>This Loop Header: Depth=1
                                        @     Child Loop BB0_3 Depth 2
.PTM_crcInit_1:
	ldr	r0, [sp, #24]
	cmp	r0, #256
	bge	.LBB0_11
	b	.LBB0_2
.LBB0_2:                                @ %for.body
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_crcInit_2:
	movw	r0, #8
	movw	r1, #24
	ldr	r2, [sp, #24]
	lsl	r2, r2, #24
	str	r2, [sp, #28]
	strb	r0, [sp, #23]
	str	r1, [sp, #16]           @ 4-byte Spill
	b	.LBB0_3
.LBB0_3:                                @ %for.cond1
                                        @   Parent Loop BB0_1 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.PTM_crcInit_3:
	ldrb	r0, [sp, #23]
	cmp	r0, #0
	ble	.LBB0_9
	b	.LBB0_4
.LBB0_4:                                @ %for.body4
                                        @   in Loop: Header=BB0_3 Depth=2
.PTM_crcInit_4:
	ldr	r0, [sp, #28]
	and	r0, r0, #-2147483648
	cmp	r0, #0
	beq	.LBB0_6
	b	.LBB0_5
.LBB0_5:                                @ %if.then
                                        @   in Loop: Header=BB0_3 Depth=2
.PTM_crcInit_5:
	ldr	r0, .LCPI0_0
	movw	r1, #1
	ldr	r2, [sp, #28]
	lsl	r2, r2, #1
	eor	r0, r2, r0
	str	r0, [sp, #28]
	str	r1, [sp, #12]           @ 4-byte Spill
	b	.LBB0_7
.LBB0_6:                                @ %if.else
                                        @   in Loop: Header=BB0_3 Depth=2
.PTM_crcInit_6:
	movw	r0, #1
	ldr	r1, [sp, #28]
	lsl	r1, r1, #1
	str	r1, [sp, #28]
	str	r0, [sp, #8]            @ 4-byte Spill
	b	.LBB0_7
.LBB0_7:                                @ %if.end
                                        @   in Loop: Header=BB0_3 Depth=2
.PTM_crcInit_7:
	b	.LBB0_8
	b	.LBB0_8
.LBB0_8:                                @ %for.inc
                                        @   in Loop: Header=BB0_3 Depth=2
.PTM_crcInit_8:
	movw	r0, #255
	ldrb	r1, [sp, #23]
	add	r0, r1, r0
	strb	r0, [sp, #23]
	b	.LBB0_3
.LBB0_9:                                @ %for.end
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_crcInit_9:
	movw	r0, :lower16:crcTable
	movt	r0, :upper16:crcTable
	movw	r1, #2
	ldr	r2, [sp, #28]
	ldr	r3, [sp, #24]
	movw	r12, :lower16:crcTable
	movt	r12, :upper16:crcTable
	add	r3, r12, r3, lsl #2
	str	r2, [r3]
	str	r0, [sp, #4]            @ 4-byte Spill
	str	r1, [sp]                @ 4-byte Spill
	b	.LBB0_10
.LBB0_10:                               @ %for.inc7
                                        @   in Loop: Header=BB0_1 Depth=1
.PTM_crcInit_10:
	ldr	r0, [sp, #24]
	add	r0, r0, #1
	str	r0, [sp, #24]
	b	.LBB0_1
.LBB0_11:                               @ %for.end8
.PTM_crcInit_11:
	add	sp, sp, #32
	bx	lr
	.p2align	2
@ BB#12:
.LCPI0_0:
	.long	79764919                @ 0x4c11db7
.Lfunc_end0:
	.size	crcInit, .Lfunc_end0-crcInit
	.cantunwind
	.fnend

	.globl	crcFast
	.p2align	2
	.type	crcFast,%function
	.code	32                      @ @crcFast
crcFast:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_crcFast_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#40
	sub	sp, sp, #40
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r2, #0
	mvn	r3, #0
	str	r0, [r11, #-4]
	str	r1, [r11, #-8]
	str	r3, [r11, #-12]
	str	r2, [sp, #20]
	b	.LBB1_1
.LBB1_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_crcFast_1:
	ldr	r0, [sp, #20]
	ldr	r1, [r11, #-8]
	cmp	r0, r1
	bge	.LBB1_4
	b	.LBB1_2
.LBB1_2:                                @ %for.body
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_crcFast_2:
	movw	r0, #8
	movw	r1, :lower16:crcTable
	movt	r1, :upper16:crcTable
	movw	r2, #2
	ldr	r3, [r11, #-4]
	ldr	r12, [sp, #20]
	ldrb	r3, [r3, r12]
	mov	r12, #8
	str	r0, [sp, #16]           @ 4-byte Spill
	mov	r0, r3
	str	r1, [sp, #12]           @ 4-byte Spill
	mov	r1, r12
	str	r2, [sp, #8]            @ 4-byte Spill
	bl	reflect
.PTM_crcFast_3:
	ldrb	r1, [r11, #-9]
	eor	r0, r0, r1
	strb	r0, [r11, #-13]
	ldrb	r0, [r11, #-13]
	mov	r1, r0
	movw	r2, :lower16:crcTable
	movt	r2, :upper16:crcTable
	add	r0, r2, r0, lsl #2
	ldr	r0, [r0]
	ldr	r2, [r11, #-12]
	lsl	r2, r2, #8
	eor	r0, r0, r2
	str	r0, [r11, #-12]
	str	r1, [sp, #4]            @ 4-byte Spill
	b	.LBB1_3
.LBB1_3:                                @ %for.inc
                                        @   in Loop: Header=BB1_1 Depth=1
.PTM_crcFast_4:
	ldr	r0, [sp, #20]
	add	r0, r0, #1
	str	r0, [sp, #20]
	b	.LBB1_1
.LBB1_4:                                @ %for.end
.PTM_crcFast_5:
	movw	r0, #32
	ldr	r1, [r11, #-12]
	str	r0, [sp]                @ 4-byte Spill
	mov	r0, r1
	ldr	r1, [sp]                @ 4-byte Reload
	and	r1, r1, #255
	bl	reflect
.PTM_crcFast_6:
	mvn	r1, #0
	eor	r0, r0, r1
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end1:
	.size	crcFast, .Lfunc_end1-crcFast
	.cantunwind
	.fnend

	.p2align	2
	.type	reflect,%function
	.code	32                      @ @reflect
reflect:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_reflect_0:
	.pad	#20
	sub	sp, sp, #20
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC2_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC2_0+8))
.LPC2_0:
	ldr	r9, [pc, r9]
	movw	r2, #0
	movw	r3, #0
	str	r0, [sp, #16]
	strb	r1, [sp, #15]
	str	r3, [sp, #8]
	strb	r2, [sp, #7]
	b	.LBB2_1
.LBB2_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_reflect_1:
	ldrb	r0, [sp, #7]
	ldrb	r1, [sp, #15]
	cmp	r0, r1
	bge	.LBB2_6
	b	.LBB2_2
.LBB2_2:                                @ %for.body
                                        @   in Loop: Header=BB2_1 Depth=1
.PTM_reflect_2:
	ldr	r0, [sp, #16]
	and	r0, r0, #1
	cmp	r0, #0
	beq	.LBB2_4
	b	.LBB2_3
.LBB2_3:                                @ %if.then
                                        @   in Loop: Header=BB2_1 Depth=1
.PTM_reflect_3:
	movw	r0, #1
	ldrb	r1, [sp, #15]
	sub	r1, r1, #1
	ldrb	r2, [sp, #7]
	sub	r1, r1, r2
	lsl	r0, r0, r1
	ldr	r1, [sp, #8]
	orr	r0, r1, r0
	str	r0, [sp, #8]
	b	.LBB2_4
.LBB2_4:                                @ %if.end
                                        @   in Loop: Header=BB2_1 Depth=1
.PTM_reflect_4:
	movw	r0, #1
	ldr	r1, [sp, #16]
	lsr	r1, r1, #1
	str	r1, [sp, #16]
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB2_5
.LBB2_5:                                @ %for.inc
                                        @   in Loop: Header=BB2_1 Depth=1
.PTM_reflect_5:
	movw	r0, #1
	ldrb	r1, [sp, #7]
	add	r0, r1, r0
	strb	r0, [sp, #7]
	b	.LBB2_1
.LBB2_6:                                @ %for.end
.PTM_reflect_6:
	ldr	r0, [sp, #8]
	add	sp, sp, #20
	bx	lr
.Lfunc_end2:
	.size	reflect, .Lfunc_end2-reflect
	.cantunwind
	.fnend

	.globl	main
	.p2align	3
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#24
	sub	sp, sp, #24
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC3_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC3_0+8))
.LPC3_0:
	ldr	r9, [pc, r9]
	movw	r0, #0
	bl	time
.PTM_main_1:
	bl	srand
.PTM_main_2:
	movw	r0, #0
	str	r0, [r11, #-4]
	b	.LBB3_1
.LBB3_1:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_3:
	ldr	r0, [r11, #-4]
	cmp	r0, #4096
	bhs	.LBB3_4
	b	.LBB3_2
.LBB3_2:                                @ %for.body
                                        @   in Loop: Header=BB3_1 Depth=1
.PTM_main_4:
	movw	r0, :lower16:main.test
	movt	r0, :upper16:main.test
	str	r0, [r11, #-8]          @ 4-byte Spill
	bl	rand
.PTM_main_5:
	vmov	s0, r0
	vcvt.f64.s32	d16, s0
	vldr	d17, .LCPI3_0
	vdiv.f64	d16, d16, d17
	vcvt.u32.f64	s0, d16
	vmov	r0, s0
	ldr	lr, [r11, #-4]
	ldr	r1, [r11, #-8]          @ 4-byte Reload
	add	lr, r1, lr
	strb	r0, [lr]
	b	.LBB3_3
.LBB3_3:                                @ %for.inc
                                        @   in Loop: Header=BB3_1 Depth=1
.PTM_main_6:
	ldr	r0, [r11, #-4]
	add	r0, r0, #1
	str	r0, [r11, #-4]
	b	.LBB3_1
.LBB3_4:                                @ %for.end
.PTM_main_7:
	bl	crcInit
.PTM_main_8:
	movw	r0, :lower16:main.test
	movt	r0, :upper16:main.test
	movw	lr, :lower16:main.test
	movt	lr, :upper16:main.test
	str	r0, [sp, #12]           @ 4-byte Spill
	mov	r0, lr
	bl	strlen
.PTM_main_9:
	ldr	lr, [sp, #12]           @ 4-byte Reload
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, lr
	ldr	r1, [sp, #8]            @ 4-byte Reload
	bl	crcFast
.PTM_main_10:
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
	.p2align	3
@ BB#5:
.LCPI3_0:
	.long	4290772992              @ double 2147483647
	.long	1105199103
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cantunwind
	.fnend

	.type	crcTable,%object        @ @crcTable
	.local	crcTable
	.comm	crcTable,1024,4
	.type	main.test,%object       @ @main.test
	.local	main.test
	.comm	main.test,4096,1

	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcInit_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430560
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcInit_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcInit_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.long	.PTM_crcInit_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcInit_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcInit_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469696
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcInit_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.long	.PTM_crcInit_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.long	.PTM_crcInit_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469568
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcInit_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	481165312
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcInit_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.long	.PTM_crcInit_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365088
	.long	502136832
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcFast_0
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430552
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcFast_1
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcFast_2
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474284032
	.long	476971008
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcFast_3
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469568
	.long	476184576
	.long	474218496
	.long	2357469696
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcFast_4
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.long	.PTM_crcFast_5
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_crcFast_6
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469568
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_reflect_0
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430572
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_reflect_1
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_reflect_2
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_reflect_3
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357604864
	.long	2357469568
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_reflect_4
	.section	.HB.annot,"a",%progbits
.Ltmp23:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp23(sbrel)
	.long	.PTM_reflect_5
	.section	.HB.annot,"a",%progbits
.Ltmp24:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp24(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469568
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_reflect_6
	.section	.HB.annot,"a",%progbits
.Ltmp25:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp25(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365076
	.long	502136832
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp26:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp26(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430568
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp27:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp27(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp28:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp28(sbrel)
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp29:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp29(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp30:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp30(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp31:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp31(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2358550912
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp32:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp32(sbrel)
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp33:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp33(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp34:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp34(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp35:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp35(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp36:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp36(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
