	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"cwe-119.c"
	.globl	copy_input
	.p2align	2
	.type	copy_input,%function
	.code	32                      @ @copy_input
copy_input:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_copy_input_0:
	.save	{r4, r5, r11, lr}
	push	{r4, r5, r11, lr}
	.setfp	r11, sp, #8
	add	r11, sp, #8
	.pad	#32
	sub	sp, sp, #32
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r1, #168
	str	r0, [r11, #-12]
	mov	r0, r1
	bl	malloc
.PTM_copy_input_1:
	movw	r1, #42
	str	r0, [sp, #16]
	ldr	r0, [r11, #-12]
	str	r1, [sp, #12]           @ 4-byte Spill
	bl	strlen
.PTM_copy_input_2:
	ldr	r1, [sp, #12]           @ 4-byte Reload
	cmp	r1, r0
	bhi	.LBB0_2
	b	.LBB0_1
.LBB0_1:                                @ %if.then
.PTM_copy_input_3:
	movw	r0, :lower16:.L.str
	movt	r0, :upper16:.L.str
	bl	printf
.PTM_copy_input_4:
	mvn	lr, #0
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, lr
	bl	exit
.LBB0_2:                                @ %if.end
.PTM_copy_input_5:
	movw	r0, #0
	str	r0, [sp, #20]
	str	r0, [r11, #-16]
	b	.LBB0_3
.LBB0_3:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_copy_input_6:
	ldr	r0, [r11, #-16]
	ldr	r1, [r11, #-12]
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, r1
	bl	strlen
.PTM_copy_input_7:
	ldr	r1, [sp, #4]            @ 4-byte Reload
	cmp	r1, r0
	bhs	.LBB0_12
	b	.LBB0_4
.LBB0_4:                                @ %for.body
                                        @   in Loop: Header=BB0_3 Depth=1
.PTM_copy_input_8:
	movw	r0, #38
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-16]
	add	r1, r1, r2
	ldrb	r1, [r1]
	cmp	r0, r1
	bne	.LBB0_6
	b	.LBB0_5
.LBB0_5:                                @ %if.then7
                                        @   in Loop: Header=BB0_3 Depth=1
.PTM_copy_input_9:
	movw	r0, #59
	movw	r1, #112
	movw	r2, #109
	movw	r3, #97
	movw	r12, #38
	ldr	lr, [sp, #16]
	ldr	r4, [sp, #20]
	add	r5, r4, #1
	str	r5, [sp, #20]
	add	lr, lr, r4
	strb	r12, [lr]
	ldr	r12, [sp, #16]
	ldr	lr, [sp, #20]
	add	r4, lr, #1
	str	r4, [sp, #20]
	add	r12, r12, lr
	strb	r3, [r12]
	ldr	r3, [sp, #16]
	ldr	r12, [sp, #20]
	add	lr, r12, #1
	str	lr, [sp, #20]
	add	r3, r3, r12
	strb	r2, [r3]
	ldr	r2, [sp, #16]
	ldr	r3, [sp, #20]
	add	r12, r3, #1
	str	r12, [sp, #20]
	add	r2, r2, r3
	strb	r1, [r2]
	ldr	r1, [sp, #16]
	ldr	r2, [sp, #20]
	add	r3, r2, #1
	str	r3, [sp, #20]
	add	r1, r1, r2
	strb	r0, [r1]
	b	.LBB0_10
.LBB0_6:                                @ %if.else
                                        @   in Loop: Header=BB0_3 Depth=1
.PTM_copy_input_10:
	movw	r0, #60
	ldr	r1, [r11, #-12]
	ldr	r2, [r11, #-16]
	add	r1, r1, r2
	ldrb	r1, [r1]
	cmp	r0, r1
	bne	.LBB0_8
	b	.LBB0_7
.LBB0_7:                                @ %if.then21
                                        @   in Loop: Header=BB0_3 Depth=1
.PTM_copy_input_11:
	b	.LBB0_9
.LBB0_8:                                @ %if.else22
                                        @   in Loop: Header=BB0_3 Depth=1
.PTM_copy_input_12:
	ldr	r0, [r11, #-12]
	ldr	r1, [r11, #-16]
	add	r0, r0, r1
	ldrb	r0, [r0]
	ldr	r1, [sp, #16]
	ldr	r2, [sp, #20]
	add	r3, r2, #1
	str	r3, [sp, #20]
	add	r1, r1, r2
	strb	r0, [r1]
	b	.LBB0_9
.LBB0_9:                                @ %if.end26
                                        @   in Loop: Header=BB0_3 Depth=1
.PTM_copy_input_13:
	b	.LBB0_10
	b	.LBB0_10
.LBB0_10:                               @ %if.end27
                                        @   in Loop: Header=BB0_3 Depth=1
.PTM_copy_input_14:
	b	.LBB0_11
	b	.LBB0_11
.LBB0_11:                               @ %for.inc
                                        @   in Loop: Header=BB0_3 Depth=1
.PTM_copy_input_15:
	ldr	r0, [r11, #-16]
	add	r0, r0, #1
	str	r0, [r11, #-16]
	b	.LBB0_3
.LBB0_12:                               @ %for.end
.PTM_copy_input_16:
	ldr	r0, [sp, #16]
	sub	sp, r11, #8
	pop	{r4, r5, r11, pc}
.Lfunc_end0:
	.size	copy_input, .Lfunc_end0-copy_input
	.cantunwind
	.fnend

	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r2, :lower16:.L.str.1
	movt	r2, :upper16:.L.str.1
	str	r0, [r11, #-4]
	str	r1, [sp, #8]
	str	r2, [sp, #4]
	ldr	r0, [sp, #4]
	bl	copy_input
.PTM_main_1:
	movw	r1, #0
	str	r0, [sp]                @ 4-byte Spill
	mov	r0, r1
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"user string too long, die evil hacker!"
	.size	.L.str, 39

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"Hello World it's me & you !"
	.size	.L.str.1, 28


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430560
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.long	.PTM_copy_input_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357604864
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	9
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	484835328
	.long	2358551296
	.long	2359092480
	.long	491651072
	.long	2357876480
	.long	499449856
	.long	2357740160
	.long	480509952
	.long	2357604864
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357604864
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.long	.PTM_copy_input_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469568
	.long	480509952
	.long	2357604864
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_copy_input_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.long	.PTM_copy_input_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.long	.PTM_copy_input_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.long	.PTM_copy_input_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504430584
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430576
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
