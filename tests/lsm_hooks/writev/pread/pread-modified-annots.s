	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"pread.c"
	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#232
	sub	sp, sp, #232
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str
	movt	r0, :upper16:.L.str
	movw	r1, #0
	bl	open
.PTM_main_1:
	str	r0, [r11, #-4]
	ldr	r0, [r11, #-4]
	cmp	r0, #0
	bge	.LBB0_2
	b	.LBB0_1
.LBB0_1:                                @ %if.then
.PTM_main_2:
	movw	r0, :lower16:.L.str.1
	movt	r0, :upper16:.L.str.1
	bl	perror
.PTM_main_3:
	movw	r0, #1
	bl	exit
.LBB0_2:                                @ %if.end
.PTM_main_4:
	movw	r0, #40
	add	r1, sp, #24
	ldr	r2, [r11, #-4]
	mov	r3, sp
	mov	r12, #0
	str	r12, [r3, #4]
	mov	r12, #5
	str	r12, [r3]
	mov	r3, #40
	str	r0, [sp, #20]           @ 4-byte Spill
	mov	r0, r2
	mov	r2, r3
	bl	pread
.PTM_main_5:
	movw	r1, :lower16:.L.str.2
	movt	r1, :upper16:.L.str.2
	add	r2, sp, #24
	movw	r3, #0
	str	r0, [r11, #-8]
	ldr	r0, [r11, #-8]
	add	r0, r2, r0
	strb	r3, [r0]
	mov	r0, r1
	mov	r1, r2
	bl	printf
.PTM_main_6:
	movw	r1, :lower16:.L.str.3
	movt	r1, :upper16:.L.str.3
	add	r2, sp, #24
	str	r0, [sp, #16]           @ 4-byte Spill
	mov	r0, r1
	mov	r1, r2
	bl	printf
.PTM_main_7:
	movw	r1, #0
	str	r0, [sp, #12]           @ 4-byte Spill
	mov	r0, r1
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"test.txt"
	.size	.L.str, 9

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"Error"
	.size	.L.str.1, 6

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"Address of the variable = %p\n"
	.size	.L.str.2, 30

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"Value of the variable = %s\n"
	.size	.L.str.3, 28


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm 2b5c7ea7a610af4c0e9bb77143223b95c7b22f8b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430360
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	481034240
	.long	474218496
	.long	478478336
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357469696
	.long	474152960
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
