################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps_g.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps_options.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps_sinit.c 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps_g.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps_options.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps_sinit.o 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps_g.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps_options.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/xsdps_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/sdps_v2_3/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


