	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"read-annnotsPropa-write.c"
	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#136
	sub	sp, sp, #136
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str
	movt	r0, :upper16:.L.str
	movw	r1, #0
	str	r1, [r11, #-4]
	bl	open
.PTM_main_1:
	movw	r1, :lower16:.L.str.1
	movt	r1, :upper16:.L.str.1
	movw	lr, #1025
	str	r0, [r11, #-8]
	mov	r0, r1
	mov	r1, lr
	bl	open
.PTM_main_2:
	str	r0, [r11, #-12]
	ldr	r0, [r11, #-8]
	cmp	r0, #0
	bge	.LBB0_2
	b	.LBB0_1
.LBB0_1:                                @ %if.then
.PTM_main_3:
	movw	r0, :lower16:.L.str.2
	movt	r0, :upper16:.L.str.2
	bl	perror
.PTM_main_4:
	movw	r0, #1
	bl	exit
.LBB0_2:                                @ %if.end
.PTM_main_5:
	ldr	r0, [r11, #-12]
	cmp	r0, #0
	bge	.LBB0_4
	b	.LBB0_3
.LBB0_3:                                @ %if.then3
.PTM_main_6:
	movw	r0, :lower16:.L.str.3
	movt	r0, :upper16:.L.str.3
	bl	perror
.PTM_main_7:
	movw	r0, #1
	bl	exit
.LBB0_4:                                @ %if.end4
.PTM_main_8:
	movw	r2, #50
	add	r1, sp, #66
	ldr	r0, [r11, #-8]
	bl	read
.PTM_main_9:
	movw	r1, #0
	movw	r2, #0
	add	lr, sp, #66
	str	r0, [r11, #-16]
	ldr	r0, [r11, #-16]
	add	r0, lr, r0
	strb	r2, [r0]
	str	r1, [r11, #-20]
	b	.LBB0_5
.LBB0_5:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_10:
	ldr	r0, [r11, #-20]
	cmp	r0, #50
	bhs	.LBB0_8
	b	.LBB0_6
.LBB0_6:                                @ %for.body
                                        @   in Loop: Header=BB0_5 Depth=1
.PTM_main_11:
	add	r0, sp, #16
	add	r1, sp, #66
	ldr	r2, [r11, #-20]
	add	r1, r1, r2
  str r1, [r9]                         @ Instrumentation
	ldrb	r1, [r1]                       @ r1 <- Mem(r1)
	ldr	r2, [r11, #-20]
	add	r0, r0, r2
  str r0, [r9]                         @ Instrumentation r0
	strb	r1, [r0]                       @ r1 <- Mem(r0)
	b	.LBB0_7
.LBB0_7:                                @ %for.inc
                                        @   in Loop: Header=BB0_5 Depth=1
.PTM_main_12:
	ldr	r0, [r11, #-20]
	add	r0, r0, #1
	str	r0, [r11, #-20]
	b	.LBB0_5
.LBB0_8:                                @ %for.end
.PTM_main_13:
	movw	r2, #50
	add	r1, sp, #16
	ldr	r0, [r11, #-12]
	bl	write
.PTM_main_14:
	ldr	r1, [r11, #-8]
	str	r0, [sp, #12]           @ 4-byte Spill
	mov	r0, r1
	bl	close
.PTM_main_15:
	ldr	r1, [r11, #-12]
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, r1
	bl	close
.PTM_main_16:
	ldr	r1, [r11, #-4]
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, r1
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"input.txt"
	.size	.L.str, 10

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"output.txt"
	.size	.L.str.1, 11

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"Error input.txt \n"
	.size	.L.str.2, 18

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"Error output.txt \n"
	.size	.L.str.3, 19


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430456
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476708864
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2357470464
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	33554432           // Instr g1
	.long	2483224576         // Tag_tr_mem(t1) = Mem(g1)
  .long	37748736           // Instr g3
  .long 2418282496         // Tag_mem_tr tag(Mem(g3)) <= Tag_tr_mem(t1)
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.long	.PTM_main_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
