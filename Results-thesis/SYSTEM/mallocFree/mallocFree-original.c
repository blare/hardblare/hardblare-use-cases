#include <stdio.h>
#include <unistd.h>
#define BUFSIZE 262144

void useAfterFree(char *buf){
	*buf = "Hello";
}

void doubleFree(char *buf){
	free(buf);
}

int main(int argc, char **argv)
{
  char *buf = (char *) malloc(BUFSIZE);
  char *buf_next = buf++;

  if(argc == 1) {
    printf("useAfterFree");
    free(buf);
    useAfterFree(buf);
  }
  if(argc == 2) {
    printf("doubleFree");
    doubleFree(buf_next);
    free(buf);
  }
}
