	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"cfi.c"
	.globl	targetFunction
	.p2align	2
	.type	targetFunction,%function
	.code	32                      @ @targetFunction
targetFunction:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_targetFunction_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#8
	sub	sp, sp, #8
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str
	movt	r0, :upper16:.L.str
	bl	printf
.PTM_targetFunction_1:
	movw	lr, :lower16:.L.str.1
	movt	lr, :upper16:.L.str.1
	movw	r1, :lower16:system
	movt	r1, :upper16:system
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, lr
	blx	r1
.PTM_targetFunction_2:
	str	r0, [sp]                @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	targetFunction, .Lfunc_end0-targetFunction
	.cantunwind
	.fnend

	.globl	vuln
	.p2align	2
	.type	vuln,%function
	.code	32                      @ @vuln
vuln:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_vuln_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str.2
	movt	r0, :upper16:.L.str.2
	bl	printf
.PTM_vuln_1:
	movw	lr, :lower16:.L.str.3
	movt	lr, :upper16:.L.str.3
	sub	r1, r11, #4
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, lr
	bl	scanf
.PTM_vuln_2:
	movw	r1, :lower16:.L.str.4
	movt	r1, :upper16:.L.str.4
	sub	lr, r11, #4
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, r1
	mov	r1, lr
	bl	printf
.PTM_vuln_3:
	str	r0, [sp]                @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end1:
	.size	vuln, .Lfunc_end1-vuln
	.cantunwind
	.fnend

	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
  str sp, [r9]
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC2_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC2_0+8))
.LPC2_0:
	ldr	r9, [pc, r9]
	movw	r2, #0
	str	r2, [r11, #-4]
	str	r0, [sp, #8]
	str	r1, [sp, #4]
	bl	vuln
.PTM_main_1:
	movw	r0, #0
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"Pwned !\n"
	.size	.L.str, 9

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"/bin/bash"
	.size	.L.str.1, 10

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"Veuillez entrer votre nom:\n"
	.size	.L.str.2, 28

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"%s"
	.size	.L.str.3, 3

	.type	.L.str.4,%object        @ @.str.4
.L.str.4:
	.asciz	"Bonjour %s!\n"
	.size	.L.str.4, 13


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_targetFunction_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430584
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_targetFunction_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_targetFunction_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_0
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430576
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_1
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_2
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	476708864
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_3
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long 62914560 // SPvalue = INSTR
  .long 17856    // INIT_SYSTEM_TAG_REG_POLICY(LR) = SECURITY_POLICY_DEFAULT_TAG[DEFAULT_TAG_LR];
	.long 1181590 // Tag_mem_tr(SP) = Tag(R11)
  .long 590798 // Tag_mem_tr(SP + 4) = Tag(LR)

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
