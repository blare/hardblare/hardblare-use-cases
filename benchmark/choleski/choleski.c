#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define N 64

int Choleski_LU_Decomposition(double *A, int n)
{
   int i, k, p;
   double *p_Lk0;                   // pointer to L[k][0]
   double *p_Lkp;                   // pointer to L[k][p]  
   double *p_Lkk;                   // pointer to diagonal element on row k.
   double *p_Li0;                   // pointer to L[i][0]
   double reciprocal;

   for (k = 0, p_Lk0 = A; k < n; p_Lk0 += n, k++) {
           
//            Update pointer to row k diagonal element.   

      p_Lkk = p_Lk0 + k;

//            Calculate the difference of the diagonal element in row k
//            from the sum of squares of elements row k from column 0 to 
//            column k-1.

      for (p = 0, p_Lkp = p_Lk0; p < k; p_Lkp += 1,  p++)
         *p_Lkk -= *p_Lkp * *p_Lkp;

//            If diagonal element is not positive, return the error code,
//            the matrix is not positive definite symmetric.

      if ( *p_Lkk <= 0.0 ) return -1;

//            Otherwise take the square root of the diagonal element.

      *p_Lkk = sqrt( *p_Lkk );
      reciprocal = 1.0 / *p_Lkk;

//            For rows i = k+1 to n-1, column k, calculate the difference
//            between the i,k th element and the inner product of the first
//            k-1 columns of row i and row k, then divide the difference by
//            the diagonal element in row k.
//            Store the transposed element in the upper triangular matrix.

      p_Li0 = p_Lk0 + n;
      for (i = k + 1; i < n; p_Li0 += n, i++) {
         for (p = 0; p < k; p++)
            *(p_Li0 + k) -= *(p_Li0 + p) * *(p_Lk0 + p);
         *(p_Li0 + k) *= reciprocal;
         *(p_Lk0 + i) = *(p_Li0 + k);
      }  
   }
   return 0;
}

void main()
{

	static double A[N][N];

	unsigned int i, j;
           
	srand(time(NULL));

	for ( i = 0; i < N; i++ )

		for (  j = 0; j < N; j++ )
			A[i][j] = rand()/((double)RAND_MAX);

	Choleski_LU_Decomposition(&A[0][0], N);

}
