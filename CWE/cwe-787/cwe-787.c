
/*

  CWE-787: Out-of-bounds Write :
  ------------------------------

  Description :
  -------------

  The software writes data past the end, or before the beginning, of the intended buffer.
  Typically, this can result in corruption of data, a crash, or code execution. The software may modify an index or perform pointer arithmetic that references a memory location that is outside of the boundaries of the buffer. A subsequent write operation then produces undefined or unexpected results.


  Example 1 :
  -----------
  The following code attempts to save four different identification numbers into an array.

  Example 2 :
  --------
  In the following example, it is possible to request that memcpy move a much larger segment of memory than assumed :

  If returnChunkSize() happens to encounter an error it will return -1. Notice that the return value is not checked before the memcpy operation (CWE-252), so -1 can be passed as the size argument to memcpy() (CWE-805). Because memcpy() assumes that the value is unsigned, it will be interpreted as MAXINT-1 (CWE-195), and therefore will copy far more memory than is likely available to the destination buffer (CWE-787, CWE-788).

 */



// Example 1


void example1()
{
  int id_sequence[3];
  /* Populate the id array. */
  id_sequence[0] = 123;
  id_sequence[1] = 234;
  id_sequence[2] = 345;
  id_sequence[3] = 456;
}



// Example 2

int returnChunkSize(void *b) {
  /* if chunk info is valid, return the size of usable memory,
   * else, return -1 to indicate an error
   */
  if(sizeof(b) > (sizeof(char) * 4))
    { sizeof(b); }
  else
    { return -1; }
}


void example2()
{

  char destBuf[] = {'A'};
  char srcBuf[] = { 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'};
  
  memcpy(destBuf, srcBuf, (returnChunkSize(destBuf)-1));
}

int main(int argc, char **argv) {

  example1();
  example2();
}

