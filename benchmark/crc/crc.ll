; ModuleID = 'crc.c'
source_filename = "crc.c"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.13.0"

@crcTable = internal global [256 x i64] zeroinitializer, align 16
@app_crc.test = internal global [4096 x i8] zeroinitializer, align 16

; Function Attrs: noinline nounwind optnone ssp uwtable
define void @crcInit() #0 {
  %1 = alloca i64, align 8
  %2 = alloca i32, align 4
  %3 = alloca i8, align 1
  store i32 0, i32* %2, align 4
  br label %4

; <label>:4:                                      ; preds = %35, %0
  %5 = load i32, i32* %2, align 4
  %6 = icmp slt i32 %5, 256
  br i1 %6, label %7, label %38

; <label>:7:                                      ; preds = %4
  %8 = load i32, i32* %2, align 4
  %9 = shl i32 %8, 56
  %10 = sext i32 %9 to i64
  store i64 %10, i64* %1, align 8
  store i8 8, i8* %3, align 1
  br label %11

; <label>:11:                                     ; preds = %27, %7
  %12 = load i8, i8* %3, align 1
  %13 = zext i8 %12 to i32
  %14 = icmp sgt i32 %13, 0
  br i1 %14, label %15, label %30

; <label>:15:                                     ; preds = %11
  %16 = load i64, i64* %1, align 8
  %17 = and i64 %16, 0
  %18 = icmp ne i64 %17, 0
  br i1 %18, label %19, label %23

; <label>:19:                                     ; preds = %15
  %20 = load i64, i64* %1, align 8
  %21 = shl i64 %20, 1
  %22 = xor i64 %21, 79764919
  store i64 %22, i64* %1, align 8
  br label %26

; <label>:23:                                     ; preds = %15
  %24 = load i64, i64* %1, align 8
  %25 = shl i64 %24, 1
  store i64 %25, i64* %1, align 8
  br label %26

; <label>:26:                                     ; preds = %23, %19
  br label %27

; <label>:27:                                     ; preds = %26
  %28 = load i8, i8* %3, align 1
  %29 = add i8 %28, -1
  store i8 %29, i8* %3, align 1
  br label %11

; <label>:30:                                     ; preds = %11
  %31 = load i64, i64* %1, align 8
  %32 = load i32, i32* %2, align 4
  %33 = sext i32 %32 to i64
  %34 = getelementptr inbounds [256 x i64], [256 x i64]* @crcTable, i64 0, i64 %33
  store i64 %31, i64* %34, align 8
  br label %35

; <label>:35:                                     ; preds = %30
  %36 = load i32, i32* %2, align 4
  %37 = add nsw i32 %36, 1
  store i32 %37, i32* %2, align 4
  br label %4

; <label>:38:                                     ; preds = %4
  ret void
}

; Function Attrs: noinline nounwind optnone ssp uwtable
define i64 @crcFast(i8*, i32) #0 {
  %3 = alloca i8*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i64, align 8
  %6 = alloca i8, align 1
  %7 = alloca i32, align 4
  store i8* %0, i8** %3, align 8
  store i32 %1, i32* %4, align 4
  store i64 4294967295, i64* %5, align 8
  store i32 0, i32* %7, align 4
  br label %8

; <label>:8:                                      ; preds = %33, %2
  %9 = load i32, i32* %7, align 4
  %10 = load i32, i32* %4, align 4
  %11 = icmp slt i32 %9, %10
  br i1 %11, label %12, label %36

; <label>:12:                                     ; preds = %8
  %13 = load i8*, i8** %3, align 8
  %14 = load i32, i32* %7, align 4
  %15 = sext i32 %14 to i64
  %16 = getelementptr inbounds i8, i8* %13, i64 %15
  %17 = load i8, i8* %16, align 1
  %18 = zext i8 %17 to i64
  %19 = call i64 @reflect(i64 %18, i8 zeroext 8)
  %20 = trunc i64 %19 to i8
  %21 = zext i8 %20 to i64
  %22 = load i64, i64* %5, align 8
  %23 = lshr i64 %22, 56
  %24 = xor i64 %21, %23
  %25 = trunc i64 %24 to i8
  store i8 %25, i8* %6, align 1
  %26 = load i8, i8* %6, align 1
  %27 = zext i8 %26 to i64
  %28 = getelementptr inbounds [256 x i64], [256 x i64]* @crcTable, i64 0, i64 %27
  %29 = load i64, i64* %28, align 8
  %30 = load i64, i64* %5, align 8
  %31 = shl i64 %30, 8
  %32 = xor i64 %29, %31
  store i64 %32, i64* %5, align 8
  br label %33

; <label>:33:                                     ; preds = %12
  %34 = load i32, i32* %7, align 4
  %35 = add nsw i32 %34, 1
  store i32 %35, i32* %7, align 4
  br label %8

; <label>:36:                                     ; preds = %8
  %37 = load i64, i64* %5, align 8
  %38 = call i64 @reflect(i64 %37, i8 zeroext 64)
  %39 = xor i64 %38, 4294967295
  ret i64 %39
}

; Function Attrs: noinline nounwind optnone ssp uwtable
define internal i64 @reflect(i64, i8 zeroext) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i8, align 1
  %5 = alloca i64, align 8
  %6 = alloca i8, align 1
  store i64 %0, i64* %3, align 8
  store i8 %1, i8* %4, align 1
  store i64 0, i64* %5, align 8
  store i8 0, i8* %6, align 1
  br label %7

; <label>:7:                                      ; preds = %31, %2
  %8 = load i8, i8* %6, align 1
  %9 = zext i8 %8 to i32
  %10 = load i8, i8* %4, align 1
  %11 = zext i8 %10 to i32
  %12 = icmp slt i32 %9, %11
  br i1 %12, label %13, label %34

; <label>:13:                                     ; preds = %7
  %14 = load i64, i64* %3, align 8
  %15 = and i64 %14, 1
  %16 = icmp ne i64 %15, 0
  br i1 %16, label %17, label %28

; <label>:17:                                     ; preds = %13
  %18 = load i8, i8* %4, align 1
  %19 = zext i8 %18 to i32
  %20 = sub nsw i32 %19, 1
  %21 = load i8, i8* %6, align 1
  %22 = zext i8 %21 to i32
  %23 = sub nsw i32 %20, %22
  %24 = shl i32 1, %23
  %25 = sext i32 %24 to i64
  %26 = load i64, i64* %5, align 8
  %27 = or i64 %26, %25
  store i64 %27, i64* %5, align 8
  br label %28

; <label>:28:                                     ; preds = %17, %13
  %29 = load i64, i64* %3, align 8
  %30 = lshr i64 %29, 1
  store i64 %30, i64* %3, align 8
  br label %31

; <label>:31:                                     ; preds = %28
  %32 = load i8, i8* %6, align 1
  %33 = add i8 %32, 1
  store i8 %33, i8* %6, align 1
  br label %7

; <label>:34:                                     ; preds = %7
  %35 = load i64, i64* %5, align 8
  ret i64 %35
}

; Function Attrs: noinline nounwind optnone ssp uwtable
define void @app_crc() #0 {
  %1 = alloca i32, align 4
  %2 = call i64 @time(i64* null)
  %3 = trunc i64 %2 to i32
  call void @srand(i32 %3)
  store i32 0, i32* %1, align 4
  br label %4

; <label>:4:                                      ; preds = %15, %0
  %5 = load i32, i32* %1, align 4
  %6 = icmp ult i32 %5, 4096
  br i1 %6, label %7, label %18

; <label>:7:                                      ; preds = %4
  %8 = call i32 @rand()
  %9 = sitofp i32 %8 to double
  %10 = fdiv double %9, 0x41DFFFFFFFC00000
  %11 = fptoui double %10 to i8
  %12 = load i32, i32* %1, align 4
  %13 = zext i32 %12 to i64
  %14 = getelementptr inbounds [4096 x i8], [4096 x i8]* @app_crc.test, i64 0, i64 %13
  store i8 %11, i8* %14, align 1
  br label %15

; <label>:15:                                     ; preds = %7
  %16 = load i32, i32* %1, align 4
  %17 = add i32 %16, 1
  store i32 %17, i32* %1, align 4
  br label %4

; <label>:18:                                     ; preds = %4
  call void @crcInit()
  %19 = call i64 @strlen(i8* getelementptr inbounds ([4096 x i8], [4096 x i8]* @app_crc.test, i32 0, i32 0))
  %20 = trunc i64 %19 to i32
  %21 = call i64 @crcFast(i8* getelementptr inbounds ([4096 x i8], [4096 x i8]* @app_crc.test, i32 0, i32 0), i32 %20)
  ret void
}

declare void @srand(i32) #1

declare i64 @time(i64*) #1

declare i32 @rand() #1

declare i64 @strlen(i8*) #1

attributes #0 = { noinline nounwind optnone ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"Apple LLVM version 9.1.0 (clang-902.0.39.2)"}
