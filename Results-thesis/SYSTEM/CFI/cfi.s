	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"cfi.c"
	.globl	english
	.p2align	2
	.type	english,%function
	.code	32                      @ @english
english:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_english_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#8
	sub	sp, sp, #8
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str
	movt	r0, :upper16:.L.str
	bl	printf
.PTM_english_1:
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	english, .Lfunc_end0-english
	.cantunwind
	.fnend

	.globl	french
	.p2align	2
	.type	french,%function
	.code	32                      @ @french
french:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_french_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#8
	sub	sp, sp, #8
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC1_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC1_0+8))
.LPC1_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str.1
	movt	r0, :upper16:.L.str.1
	bl	printf
.PTM_french_1:
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end1:
	.size	french, .Lfunc_end1-french
	.cantunwind
	.fnend

	.globl	spanish
	.p2align	2
	.type	spanish,%function
	.code	32                      @ @spanish
spanish:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_spanish_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#8
	sub	sp, sp, #8
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC2_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC2_0+8))
.LPC2_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str.2
	movt	r0, :upper16:.L.str.2
	bl	printf
.PTM_spanish_1:
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end2:
	.size	spanish, .Lfunc_end2-spanish
	.cantunwind
	.fnend

	.globl	german
	.p2align	2
	.type	german,%function
	.code	32                      @ @german
german:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_german_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#8
	sub	sp, sp, #8
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC3_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC3_0+8))
.LPC3_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str.3
	movt	r0, :upper16:.L.str.3
	bl	printf
.PTM_german_1:
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end3:
	.size	german, .Lfunc_end3-german
	.cantunwind
	.fnend

	.globl	targetFunctionForward
	.p2align	2
	.type	targetFunctionForward,%function
	.code	32                      @ @targetFunctionForward
targetFunctionForward:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_targetFunctionForward_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#8
	sub	sp, sp, #8
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC4_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC4_0+8))
.LPC4_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str.4
	movt	r0, :upper16:.L.str.4
	bl	printf
.PTM_targetFunctionForward_1:
	movw	lr, :lower16:.L.str.5
	movt	lr, :upper16:.L.str.5
	str	r0, [sp]                @ 4-byte Spill
	mov	r0, lr
	bl	system
.PTM_targetFunctionForward_2:
	str	r0, [sp, #4]
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end4:
	.size	targetFunctionForward, .Lfunc_end4-targetFunctionForward
	.cantunwind
	.fnend

	.globl	targetFunctionBackward
	.p2align	2
	.type	targetFunctionBackward,%function
	.code	32                      @ @targetFunctionBackward
targetFunctionBackward:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_targetFunctionBackward_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#8
	sub	sp, sp, #8
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC5_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC5_0+8))
.LPC5_0:
	ldr	r9, [pc, r9]
	movw	r0, :lower16:.L.str.6
	movt	r0, :upper16:.L.str.6
	bl	printf
.PTM_targetFunctionBackward_1:
	movw	lr, :lower16:.L.str.5
	movt	lr, :upper16:.L.str.5
	str	r0, [sp]                @ 4-byte Spill
	mov	r0, lr
	bl	system
.PTM_targetFunctionBackward_2:
	str	r0, [sp, #4]
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end5:
	.size	targetFunctionBackward, .Lfunc_end5-targetFunctionBackward
	.cantunwind
	.fnend

	.globl	vuln
	.p2align	2
	.type	vuln,%function
	.code	32                      @ @vuln
vuln:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_vuln_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#48
	sub	sp, sp, #48
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC6_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC6_0+8))
.LPC6_0:
	ldr	r9, [pc, r9]
	sub	r0, r11, #16
	movw	r1, #2
	movw	r2, :lower16:.Lvuln.language
	movt	r2, :upper16:.Lvuln.language
	vld1.32	{d16, d17}, [r2]
	sub	r2, r11, #16
	vst1.64	{d16, d17}, [r2]
	mov	r3, #0
	str	r0, [sp, #20]           @ 4-byte Spill
	mov	r0, r3
	str	r2, [sp, #16]           @ 4-byte Spill
	str	r1, [sp, #12]           @ 4-byte Spill
	bl	time
.PTM_vuln_1:
	bl	srand
.PTM_vuln_2:
	bl	rand
.PTM_vuln_3:
	movw	r1, #26215
	movt	r1, #26214
	smmul	r1, r0, r1
	asr	r2, r1, #1
	add	r1, r2, r1, lsr #31
	add	r1, r1, r1, lsl #2
	sub	r0, r0, r1
	str	r0, [sp, #24]
	movw	r0, :lower16:.L.str.7
	movt	r0, :upper16:.L.str.7
	sub	r1, r11, #20
	bl	scanf
.PTM_vuln_4:
	ldr	r1, [sp, #24]
	ldr	r2, [sp, #16]           @ 4-byte Reload
	add	r1, r2, r1, lsl #2
	ldr	r1, [r1]
	str	r0, [sp, #8]            @ 4-byte Spill
	blx	r1
.PTM_vuln_5:
	movw	r0, :lower16:.L.str.8
	movt	r0, :upper16:.L.str.8
	sub	r1, r11, #20
	bl	printf
.PTM_vuln_6:
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end6:
	.size	vuln, .Lfunc_end6-vuln
	.cantunwind
	.fnend

	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC7_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC7_0+8))
.LPC7_0:
	ldr	r9, [pc, r9]
	movw	r2, #0
	str	r2, [r11, #-4]
	str	r0, [sp, #8]
	str	r1, [sp, #4]
	bl	vuln
.PTM_main_1:
	movw	r0, #0
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end7:
	.size	main, .Lfunc_end7-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"Hello "
	.size	.L.str, 7

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"Bonjour "
	.size	.L.str.1, 9

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"Hola "
	.size	.L.str.2, 6

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"Hallo "
	.size	.L.str.3, 7

	.type	.L.str.4,%object        @ @.str.4
.L.str.4:
	.asciz	"Hacked Forward!\n"
	.size	.L.str.4, 17

	.type	.L.str.5,%object        @ @.str.5
.L.str.5:
	.asciz	"bash"
	.size	.L.str.5, 5

	.type	.L.str.6,%object        @ @.str.6
.L.str.6:
	.asciz	"Hacked Backward!\n"
	.size	.L.str.6, 18

	.type	.Lvuln.language,%object @ @vuln.language
	.section	.rodata,"a",%progbits
	.p2align	2
.Lvuln.language:
	.long	french
	.long	english
	.long	spanish
	.long	german
	.size	.Lvuln.language, 16

	.type	.L.str.7,%object        @ @.str.7
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str.7:
	.asciz	"%s"
	.size	.L.str.7, 3

	.type	.L.str.8,%object        @ @.str.8
.L.str.8:
	.asciz	" %s!\n"
	.size	.L.str.8, 6


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_english_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430584
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_english_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_french_0
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430584
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_french_1
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_spanish_0
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430584
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_spanish_1
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_german_0
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430584
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_german_1
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_targetFunctionForward_0
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430584
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_targetFunctionForward_1
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_targetFunctionForward_2
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_targetFunctionBackward_0
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430584
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_targetFunctionBackward_1
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_targetFunctionBackward_2
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_0
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430544
	.long	474284032
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_1
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_2
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_3
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2391159040
	.long	476315648
	.long	2357469568
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_4
	.section	.HB.annot,"a",%progbits
.Ltmp18:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp18(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	476315648
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_5
	.section	.HB.annot,"a",%progbits
.Ltmp19:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp19(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_vuln_6
	.section	.HB.annot,"a",%progbits
.Ltmp20:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp20(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp21:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp21(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430576
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp22:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp22(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
