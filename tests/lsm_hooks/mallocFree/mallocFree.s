	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a9
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 3	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"mallocFree.c"
	.globl	main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ BB#0:                                 @ %entry
.PTM_main_0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#48
	sub	sp, sp, #48
	movw	r9, :lower16:(__hardblare_instr_addr-(.LPC0_0+8))
	movt	r9, :upper16:(__hardblare_instr_addr-(.LPC0_0+8))
.LPC0_0:
	ldr	r9, [pc, r9]
	movw	r0, #2
	mov	r1, #0
	str	r1, [r11, #-4]
	movw	r1, #1500
	str	r1, [r11, #-12]
	ldr	r1, [r11, #-12]
	movw	r2, :lower16:.L.str
	movt	r2, :upper16:.L.str
	str	r0, [r11, #-20]         @ 4-byte Spill
	mov	r0, r2
	bl	printf
.PTM_main_1:
	ldr	r1, [r11, #-12]
	lsl	r1, r1, #2
	str	r0, [sp, #24]           @ 4-byte Spill
	mov	r0, r1
	bl	malloc
.PTM_main_2:
	movw	r1, #0
	str	r0, [r11, #-8]
	ldr	r0, [r11, #-8]
	cmp	r0, r1
	bne	.LBB0_2
	b	.LBB0_1
.LBB0_1:                                @ %if.then
.PTM_main_3:
	movw	r0, :lower16:.L.str.1
	movt	r0, :upper16:.L.str.1
	bl	printf
.PTM_main_4:
	movw	lr, #0
	str	r0, [sp, #20]           @ 4-byte Spill
	mov	r0, lr
	bl	exit
.LBB0_2:                                @ %if.else
.PTM_main_5:
	movw	r0, :lower16:.L.str.2
	movt	r0, :upper16:.L.str.2
	bl	printf
.PTM_main_6:
	movw	lr, #0
	str	lr, [r11, #-16]
	str	r0, [sp, #16]           @ 4-byte Spill
	b	.LBB0_3
.LBB0_3:                                @ %for.cond
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_7:
	ldr	r0, [r11, #-16]
	ldr	r1, [r11, #-12]
	cmp	r0, r1
	bge	.LBB0_6
	b	.LBB0_4
.LBB0_4:                                @ %for.body
                                        @   in Loop: Header=BB0_3 Depth=1
.PTM_main_8:
	movw	r0, #2
	ldr	r1, [r11, #-16]
	add	r2, r1, #1
	ldr	r3, [r11, #-8]
	add	r1, r3, r1, lsl #2
	str	r2, [r1]
	str	r0, [sp, #12]           @ 4-byte Spill
	b	.LBB0_5
.LBB0_5:                                @ %for.inc
                                        @   in Loop: Header=BB0_3 Depth=1
.PTM_main_9:
	ldr	r0, [r11, #-16]
	add	r0, r0, #1
	str	r0, [r11, #-16]
	b	.LBB0_3
.LBB0_6:                                @ %for.end
.PTM_main_10:
	movw	r0, :lower16:.L.str.3
	movt	r0, :upper16:.L.str.3
	bl	printf
.PTM_main_11:
	movw	lr, #0
	str	lr, [r11, #-16]
	str	r0, [sp, #8]            @ 4-byte Spill
	b	.LBB0_7
.LBB0_7:                                @ %for.cond6
                                        @ =>This Inner Loop Header: Depth=1
.PTM_main_12:
	ldr	r0, [r11, #-16]
	ldr	r1, [r11, #-12]
	cmp	r0, r1
	bge	.LBB0_10
	b	.LBB0_8
.LBB0_8:                                @ %for.body8
                                        @   in Loop: Header=BB0_7 Depth=1
.PTM_main_13:
	movw	r0, :lower16:.L.str.4
	movt	r0, :upper16:.L.str.4
	movw	r1, #2
	ldr	r2, [r11, #-8]
	ldr	r3, [r11, #-16]
	add	r2, r2, r3, lsl #2
	mov	r3, r2
	ldr	r2, [r2]
	str	r1, [sp, #4]            @ 4-byte Spill
	mov	r1, r3
	bl	printf
.PTM_main_14:
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB0_9
.LBB0_9:                                @ %for.inc12
                                        @   in Loop: Header=BB0_7 Depth=1
.PTM_main_15:
	ldr	r0, [r11, #-16]
	add	r0, r0, #1
	str	r0, [r11, #-16]
	b	.LBB0_7
.LBB0_10:                               @ %for.end14
.PTM_main_16:
	b	.LBB0_11
	b	.LBB0_11
.LBB0_11:                               @ %if.end
.PTM_main_17:
	movw	r0, #0
	mov	sp, r11
	pop	{r11, pc}
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend

	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"Enter number of elements: %d\n"
	.size	.L.str, 30

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"Memory not allocated.\n"
	.size	.L.str.1, 23

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"Memory successfully allocated using malloc. \n"
	.size	.L.str.2, 46

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"The elements of the array are: "
	.size	.L.str.3, 32

	.type	.L.str.4,%object        @ @.str.4
.L.str.4:
	.asciz	"\t%p = \t%d\n, "
	.size	.L.str.4, 13


	.ident	"clang version 4.0.1 (file:///home/mounir/HardBlare/clang 20de541bc2453e775529f0c27fb2a75b4e1d0c0d) (file:///home/mounir/HardBlare/llvm f87c959f63fe950e5c5990cc673c077ffbe74d7b)"
	.section	".note.GNU-stack","",%progbits
	.section	.HB.info,"a",%progbits
	.long	2
	.long	2

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_0
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	4
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	506527744
	.long	504430544
	.long	474218496
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_1
	.section	.HB.annot,"a",%progbits
.Ltmp1:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp1(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474152960
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_2
	.section	.HB.annot,"a",%progbits
.Ltmp2:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp2(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_3
	.section	.HB.annot,"a",%progbits
.Ltmp3:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp3(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_4
	.section	.HB.annot,"a",%progbits
.Ltmp4:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp4(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	474611712
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_5
	.section	.HB.annot,"a",%progbits
.Ltmp5:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp5(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_6
	.section	.HB.annot,"a",%progbits
.Ltmp6:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp6(sbrel)
	.long	.PTM_main_7
	.section	.HB.annot,"a",%progbits
.Ltmp7:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp7(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_8
	.section	.HB.annot,"a",%progbits
.Ltmp8:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp8(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	478347264
	.long	476381184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_9
	.section	.HB.annot,"a",%progbits
.Ltmp9:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp9(sbrel)
	.long	.PTM_main_10
	.section	.HB.annot,"a",%progbits
.Ltmp10:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp10(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_11
	.section	.HB.annot,"a",%progbits
.Ltmp11:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp11(sbrel)
	.long	.PTM_main_12
	.section	.HB.annot,"a",%progbits
.Ltmp12:
	.long	2
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp12(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	509739008
	.long	2237661184
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_13
	.section	.HB.annot,"a",%progbits
.Ltmp13:
	.long	3
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp13(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	480509952
	.long	476381184
	.long	2244935680
	.section	.HB.bbt,"a",%progbits
	.long	.PTM_main_14
	.section	.HB.annot,"a",%progbits
.Ltmp14:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp14(sbrel)
	.long	.PTM_main_15
	.section	.HB.annot,"a",%progbits
.Ltmp15:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp15(sbrel)
	.long	.PTM_main_16
	.section	.HB.annot,"a",%progbits
.Ltmp16:
	.long	0
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp16(sbrel)
	.long	.PTM_main_17
	.section	.HB.annot,"a",%progbits
.Ltmp17:
	.long	1
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp17(sbrel)
	.section	.HB.annot,"a",%progbits
	.long	504365056

	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
