#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_ROW 32
#define NUM_COL 32

void app_matrix()
{
	static int m1[NUM_ROW][NUM_COL];
	static int m2[NUM_ROW][NUM_COL];
	static int mult[NUM_ROW][NUM_COL];
	unsigned int i, j, k, r1, c1, r2, c2;

	r1 = NUM_ROW;
	c1 = NUM_COL;
	r2 = NUM_ROW;
	c2 = NUM_COL;

	srand(time(NULL));

	for(i=0;i<r1;i++)
		for(j=0;j<c1;j++)
			m1[i][j] = rand()%256;

	for(i=0;i<r2;i++)
		for(j=0;j<c2;j++)
			m2[i][j] = rand()%256;

	for(i=0;i<r1;i++)
	{
		for(j=0;j<c2;j++)
		{
			mult[i][j]=0;
			for(k=0;k<r1;k++)
				mult[i][j]+=m1[i][k]*m2[k][j];
		}
	}

}
