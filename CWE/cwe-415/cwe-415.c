/*

  CWE-415: Double Free :
  ---------------------


  Description :
  -------------

  The product calls free() twice on the same memory address, potentially leading to modification of unexpected memory locations.
  When a program calls free() twice with the same argument, the program's memory management data structures become corrupted. This corruption can cause the program to crash or, in some circumstances, cause two later calls to malloc() to return the same pointer. If malloc() returns the same value twice and the program later gives the attacker control over the data that is written into this doubly-allocated memory, the program becomes vulnerable to a buffer overflow attack.


  Example :
  ---------

  While contrived, this code should be exploitable on Linux distributions which do not ship with heap-chunk check summing turned on.

 */

#include <stdio.h>
#include <unistd.h>
#define BUFSIZE 512

int main(int argc, char **argv) 
{
  char *buf;

  printf("Malloc");
  buf = (char *) malloc(BUFSIZE);

  int val = atoi(argv[1]);
  printf("Arg: %d", val);
    
  free(buf);
  printf("Free buf");
  
  if(val == 2)
    {
      free(buf);
      printf("Free buf");
    }
 
}
