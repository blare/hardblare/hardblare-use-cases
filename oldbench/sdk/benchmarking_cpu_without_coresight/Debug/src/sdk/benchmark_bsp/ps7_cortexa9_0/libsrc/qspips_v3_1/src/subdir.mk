################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_g.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_hw.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_options.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_selftest.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_sinit.c 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_g.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_hw.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_options.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_selftest.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_sinit.o 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_g.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_hw.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_options.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_selftest.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/xqspips_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/qspips_v3_1/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


