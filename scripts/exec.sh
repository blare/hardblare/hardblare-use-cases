#!/bin/bash

NAME_EXEC_CWE_415=cwe-415-modified-annots.elf
NAME_EXEC_CWE_125=cwe-125-modified-annots.elf

PATH_EXEC=$1
RESULT_DEBUG=$2
LEN=$1

ELF_EXEC=$PATH_EXEC/$NAME_EXEC.elf

echo 0 > /proc/sys/kernel/printk


printf "\n\n\n\n\n\n\n\n"

printf "\n\n\033[33m ********************************************************************************************* \033[0m \n"
printf "\033[33m ************************ $NAME_EXEC_CWE_415 (Double Free) **************************  \033[0m \n"
printf "\033[33m *********************************************************************************************  \033[0m \n"

printf "\n\n\n"

printf "\n\033[33m[TEST $NAME_EXEC_CWE_415] - EXEC NORMAL \033[0m \n"
printf "\033[33m -------------------------------------------------------  \033[0m \n"
printf "\033[36m ./$NAME_EXEC_CWE_415 1  \033[0m \n\n"

sh script.sh $NAME_EXEC_CWE_415 1 $1

sleep 2 
printf "\n\n\n\n\n\n\n\n"

printf "\n\033[33m[TEST $NAME_EXEC_CWE_415] - EXEC EXPLOIT \033[0m \n"
printf "\033[33m -------------------------------------------------------  \033[0m \n"
printf "\033[36m ./$NAME_EXEC_CWE_415 2  \033[0m \n\n"
sh script.sh $NAME_EXEC_CWE_415 2 $1


printf "\n\n\n\n\n\n\n\n" 

sleep 2

printf "\n\n\033[33m *******************************************************************************************************************  \033[0m \n"
printf "\033[33m ************************ $NAME_EXEC_CWE_125 buffer overflow (Out of Bound read) **************************  \033[0m \n"
printf "\033[33m *******************************************************************************************************************  \033[0m \n"


printf "\n\n\n" 

printf "\n\033[33m[TEST $NAME_EXEC_CWE_125] - EXEC NORMAL \033[0m \n"
printf "\033[33m -------------------------------------------------------  \033[0m \n"
printf "\033[36m ./$NAME_EXEC_CWE_125 9  \033[0m \n\n"
sh script.sh $NAME_EXEC_CWE_125 0 $1

sleep 2 
printf "\n\n\n\n\n\n\n\n"


printf "\n\033[33m[TEST $NAME_EXEC_CWE_125] - EXEC EXPLOIT \033[0m \n"
printf "\033[33m -------------------------------------------------------  \033[0m \n"
printf "\033[36m ./$NAME_EXEC_CWE_125 -3  \033[0m \n\n"
sh script.sh $NAME_EXEC_CWE_125 -12 $1



