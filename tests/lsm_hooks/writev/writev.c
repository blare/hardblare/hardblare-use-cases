#include <sys/uio.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>


int main() {


  char *str0 = "hello ";
  char *str1 = "world\n";
  struct iovec iov[2];
  ssize_t nwritten;
  int fd;

  fd = open("test.txt", O_RDWR);

  iov[0].iov_base = str0;
  iov[0].iov_len = strlen(str0);
  iov[1].iov_base = str1;
  iov[1].iov_len = strlen(str1);

  nwritten = writev(fd, iov, 2);

  return 0;
}
