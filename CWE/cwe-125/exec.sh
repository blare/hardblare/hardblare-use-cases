#!/bin/bash

NAME_EXEC=cwe-125

PATH_EXEC=$1
RESULT_DEBUG=$2

ELF_EXEC=$PATH_EXEC/$NAME_EXEC.elf


printf "\n\n\033[32m **************************************************************  \033[0m \n"
printf "\033[32m ************************ $NAME_EXEC **************************  \033[0m \n"
printf "\033[32m **************************************************************  \033[0m \n"

sleep 1

printf "\n\033[32m[TEST $NAME_EXEC] - TAG_MEM BEFORE \033[0m \n"
$RESULT_DEBUG

sleep 1

printf "\n\033[32m[TEST $NAME_EXEC] - EXEC NORMAL \033[0m \n"
$ELF_EXEC 2

sleep 1

printf "\n\033[32m[TEST $NAME_EXEC] - TAG_MEM NORMAL \033[0m \n"
$RESULT_DEBUG

sleep 1

printf "\n\033[32m[TEST $NAME_EXEC] - EXEC EXPLOIT \033[0m \n"
$ELF_EXEC 9999

sleep 1

printf "\n\033[32m[TEST $NAME_EXEC] - TAG_MEM EXPLOIT \033[0m \n"
$RESULT_DEBUG
