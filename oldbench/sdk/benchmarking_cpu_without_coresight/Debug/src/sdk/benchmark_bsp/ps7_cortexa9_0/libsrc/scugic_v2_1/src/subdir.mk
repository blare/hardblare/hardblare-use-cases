################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_g.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_hw.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_intr.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_selftest.c \
../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_sinit.c 

OBJS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_g.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_hw.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_intr.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_selftest.o \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_sinit.o 

C_DEPS += \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_g.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_hw.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_intr.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_selftest.d \
./src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/xscugic_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/%.o: ../src/sdk/benchmark_bsp/ps7_cortexa9_0/libsrc/scugic_v2_1/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


