#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/types.h>
#include <getopt.h>
#include <string.h>
#include <unistd.h>

typedef __u32 u32;
typedef __s32 s32;

typedef __u16 u16;
typedef __s16 s16;

typedef __u8  u8;
typedef __s8  s8;

#define AXI_BRAM_CTRL_KERNEL_TO_MONITOR     0x43C00000
#define AXI_BRAM_CTRL_KERNEL_TO_MONITOR_MAP_SIZE  4096UL
#define AXI_BRAM_CTRL_KERNEL_TO_MONITOR_OFFSET 0
#define AXI_BRAM_CTRL_KERNEL_TO_MONITOR_MAP_MASK (AXI_BRAM_CTRL_KERNEL_TO_MONITOR_MAP_SIZE - 1)

#define AXI_BRAM_CTRL_MONITOR_TO_KERNEL     0x43C20000
#define AXI_BRAM_CTRL_MONITOR_TO_KERNEL_MAP_SIZE  4096UL
#define AXI_BRAM_CTRL_MONITOR_TO_KERNEL_OFFSET 0
#define AXI_BRAM_CTRL_MONITOR_TO_KERNEL_MAP_MASK (AXI_BRAM_CTRL_MONITOR_TO_KERNEL_MAP_SIZE - 1)

#define AXI_BRAM_CTRL_CONFIG     0x42000000
#define AXI_BRAM_CTRL_CONFIG_MAP_SIZE  4096UL
#define AXI_BRAM_CTRL_CONFIG_OFFSET 0
#define AXI_BRAM_CTRL_CONFIG_MAP_MASK (AXI_BRAM_CTRL_CONFIG_MAP_SIZE - 1)

#define AXI_BRAM_CTRL_ANNOT     0x48000000
#define AXI_BRAM_CTRL_ANNOT_MAP_SIZE  32768UL
#define AXI_BRAM_CTRL_ANNOT_OFFSET 0
#define AXI_BRAM_CTRL_ANNOT_MAP_MASK (AXI_BRAM_CTRL_ANNOT_MAP_SIZE - 1)

#define AXI_BRAM_CTRL_INSTR     0x43C10000
#define AXI_BRAM_CTRL_INSTR_MAP_SIZE 4096UL
#define AXI_BRAM_CTRL_INSTR_OFFSET 0
#define AXI_BRAM_CTRL_INSTR_MAP_MASK (AXI_BRAM_CTRL_INSTR_MAP_SIZE - 1)

#define AXI_BRAM_CTRL_DEBUG     0x44000000
#define AXI_BRAM_CTRL_DEBUG_MAP_SIZE 65536UL
#define AXI_BRAM_CTRL_DEBUG_OFFSET 0
#define AXI_BRAM_CTRL_DEBUG_MAP_MASK (AXI_BRAM_CTRL_DEBUG_MAP_SIZE - 1)


#define AXI_BRAM_CTRL_PLtoPS_INTR     0x40000000
#define AXI_BRAM_CTRL_PLtoPS_INTR_MAP_SIZE  4096UL
#define AXI_BRAM_CTRL_PLtoPS_INTR_OFFSET 0
#define AXI_BRAM_CTRL_PLtoPS_INTR_MAP_MASK (AXI_BRAM_CTRL_PLtoPS_INTR_MAP_SIZE - 1)


#define AXI_BRAM_CTRL_PLtoPS     0x41000000
#define AXI_BRAM_CTRL_PLtoPS_MAP_SIZE  4096UL
#define AXI_BRAM_CTRL_PLtoPS_OFFSET 0
#define AXI_BRAM_CTRL_PLtoPS_MAP_MASK (AXI_BRAM_CTRL_PLtoPS_INTR_MAP_SIZE - 1)


int main (int argc, char **argv) {

  extern char * optarg;
  extern int optind;
  extern int optopt;
  int opt;

  int memfd;

  size_t pagesize = sysconf(_SC_PAGE_SIZE);
  size_t len = pagesize;
  size_t len_base = pagesize;
  unsigned long int page_base, page_offset, offset;
  unsigned long int addr;
  size_t size;

  unsigned need_to_print_values = 0;


  while((opt = getopt(argc, argv, "s:x:")) != -1)
    {
      switch(opt)
	{	  
	case 's':
	  len = atoi(optarg);
	  printf("Printed size: %d \n", len);
	  break;
        case 'x':
	{
          //memory_type = optarg;
	  if(strcmp(optarg, "kernel2monitor") == 0)
	    {
	      addr = AXI_BRAM_CTRL_KERNEL_TO_MONITOR;
	      page_base = (addr / pagesize) * pagesize;
	      size = AXI_BRAM_CTRL_KERNEL_TO_MONITOR_MAP_SIZE;
	      page_offset = AXI_BRAM_CTRL_KERNEL_TO_MONITOR - page_base;
	      len_base = pagesize + ((AXI_BRAM_CTRL_KERNEL_TO_MONITOR_MAP_SIZE / pagesize) * pagesize );
	      // offset = AXI_BRAM_CTRL_KERNEL_TO_MONITOR_OFFSET - addr;
	      // mask = AXI_BRAM_CTRL_KERNEL_TO_MONITOR_MAP_MASK;
	      need_to_print_values = 1;
	    }

	  else if(strcmp(optarg, "monitor2kernel") == 0)
	    {
	      addr = AXI_BRAM_CTRL_MONITOR_TO_KERNEL;
	      page_base = (addr / pagesize) * pagesize;
	      size = AXI_BRAM_CTRL_MONITOR_TO_KERNEL_MAP_SIZE;
	      page_offset = AXI_BRAM_CTRL_MONITOR_TO_KERNEL - page_base;
	      len_base = pagesize + ((AXI_BRAM_CTRL_MONITOR_TO_KERNEL_MAP_SIZE / pagesize) * pagesize );
	      // offset = AXI_BRAM_CTRL_MONITOR_TO_KERNEL_OFFSET - addr;
	      //mask = AXI_BRAM_CTRL_MONITOR_TO_KERNEL_MAP_MASK;
	      need_to_print_values = 1;
	    }
	  else if(strcmp(optarg, "config") == 0)
	    {
	      addr = AXI_BRAM_CTRL_CONFIG;	 
	      page_base = (addr / pagesize) * pagesize;
	      size = AXI_BRAM_CTRL_CONFIG_MAP_SIZE;
	      page_offset = AXI_BRAM_CTRL_CONFIG - page_base;
	      len_base = pagesize + ((AXI_BRAM_CTRL_CONFIG_MAP_SIZE / pagesize) * pagesize );
	      // offset = AXI_BRAM_CTRL_CONFIG_OFFSET - addr;
	      //  mask = AXI_BRAM_CTRL_CONFIG_MAP_MASK;
	      need_to_print_values = 1;
	    }
	  else if(strcmp(optarg, "bbt-annot") == 0)
	    {
	      addr = AXI_BRAM_CTRL_ANNOT;
	      page_base = (addr / pagesize) * pagesize;
	      size = AXI_BRAM_CTRL_ANNOT_MAP_SIZE;
	      page_offset = AXI_BRAM_CTRL_ANNOT - page_base;
	      len_base = pagesize + ((AXI_BRAM_CTRL_ANNOT_MAP_SIZE / pagesize) * pagesize );
	      // offset = AXI_BRAM_CTRL_ANNOT_OFFSET - addr;
	      //mask = AXI_BRAM_CTRL_ANNOT_MAP_MASK;
	      need_to_print_values = 1;
	    }
	  else if(strcmp(optarg, "instr") == 0)
	    {
	      addr = AXI_BRAM_CTRL_INSTR;
	      page_base = (addr / pagesize) * pagesize;
	      size = AXI_BRAM_CTRL_INSTR_MAP_SIZE;
	      page_offset = AXI_BRAM_CTRL_INSTR - page_base;
	      len_base = pagesize + ((AXI_BRAM_CTRL_INSTR_MAP_SIZE / pagesize) * pagesize );
	      // offset = AXI_BRAM_CTRL_INSTR_OFFSET - addr;
	      //mask = AXI_BRAM_CTRL_INSTR_MAP_MASK;
	      need_to_print_values = 1;
	    }
	  else if(strcmp(optarg, "debug") == 0)
	    {
	      addr = AXI_BRAM_CTRL_DEBUG;
	      page_base = (addr / pagesize) * pagesize;
	      size = AXI_BRAM_CTRL_DEBUG_MAP_SIZE;
	      page_offset = AXI_BRAM_CTRL_DEBUG - page_base;
	      len_base = pagesize + ((AXI_BRAM_CTRL_DEBUG_MAP_SIZE / pagesize) * pagesize );
	      // offset = AXI_BRAM_CTRL_DEBUG_OFFSET - addr;
	      // mask = AXI_BRAM_CTRL_DEBUG_MAP_MASK;
	      need_to_print_values = 1;
	    }
	  else if(strcmp(optarg, "pltops-intr") == 0)
	    {
	      addr = AXI_BRAM_CTRL_PLtoPS_INTR;
	      page_base = (addr / pagesize) * pagesize;
	      size = AXI_BRAM_CTRL_PLtoPS_INTR_MAP_SIZE;
	      page_offset = AXI_BRAM_CTRL_PLtoPS_INTR - page_base;
	      len_base = pagesize + ((AXI_BRAM_CTRL_PLtoPS_INTR_MAP_SIZE / pagesize) * pagesize );
	      // offset = AXI_BRAM_CTRL_DEBUG_OFFSET - addr;
	      // mask = AXI_BRAM_CTRL_DEBUG_MAP_MASK;
	      need_to_print_values = 1;
	    }
    else if(strcmp(optarg, "pltops") == 0)
	    {
	      addr = AXI_BRAM_CTRL_PLtoPS;
	      page_base = (addr / pagesize) * pagesize;
	      size = AXI_BRAM_CTRL_PLtoPS_MAP_SIZE;
	      page_offset = AXI_BRAM_CTRL_PLtoPS - page_base;
	      len_base = pagesize + ((AXI_BRAM_CTRL_PLtoPS_MAP_SIZE / pagesize) * pagesize );
	      // offset = AXI_BRAM_CTRL_DEBUG_OFFSET - addr;
	      // mask = AXI_BRAM_CTRL_DEBUG_MAP_MASK;
	      need_to_print_values = 1;
	    }
	  else
	    {
	      need_to_print_values = 0;
	      printf("Error: memory type not recognised \n");
	    }
	  break;
	}
	case ':':
    {
      printf("Option needs a value\n");
      break;
    }
	case '?':
    {
	  if (optopt == 'x')
	    {
        fprintf(stderr, "Option -x needs a memory type :\n \t - kernel2monitor\n \t - monitor2kernel\n \t - config\n  \t - bbt-annot\n \t - instr\n \t - debug\n \t - pltops-intr \n\t - pltops  \n\n");
	    }
	  else if(optopt == 's')
	    {
	      fprintf(stderr, "Option -s needs a size\n");
	    }
	  else
	    {
	      printf("Unknown option \n");
	    }

	  break;
	}
  }
    }



  if(need_to_print_values == 0)
    {
	
    }
  else{

	  memfd = open("/dev/mem", O_SYNC);

	  if (memfd == -1) {
	    printf("Can't open /dev/mem.\n");
	    exit(0);
	  }
	  
	  printf("/dev/mem opened.\n");

	  printf("Physical address 0x%lx \n\n ", addr);
	
    unsigned char *mapped_base = mmap(NULL, size, PROT_READ, MAP_SHARED, memfd, page_base );
	  
	  if (mapped_base == (void *) -1) {
	    printf("Can't map the memory to user space.\n");
	    exit(0);
	  }

	  int i = 0;
	  static int count = 0;

	  volatile u32 *value_address = (volatile u32 *) mapped_base;
	  u32 value = 0;

	   
	  while (i < len ) {
	    printf("Address %p = \t ", value_address);
	    value = *value_address;
	    printf("%x \n", value);
	    value_address++;
	    i++;
	    count++;
	  }

	  
	  // unmap the memory before exiting
	  if (munmap(mapped_base, len_base) == -1) {
	    printf("Can't unmap memory from user space.\n");
	    exit(0);
	  }
	  
	  close(memfd);
	  
	  printf(" \n");

	}

  return 0;
}

