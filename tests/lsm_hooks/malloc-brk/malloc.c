#include <stdio.h>
#include <stdlib.h>


int main()
{
    // This pointer will hold the
    // base address of the block created
    int* ptr;
    int n, i;

    void *curr_brk = NULL;

    // Get the number of elements for the array
    n = 1500;
    printf("Enter number of elements: %d\n", n);

    curr_brk = sbrk(0);

    printf("Program break Location 1 :%p\n", curr_brk);

    // Dynamically allocate memory using malloc()
    ptr = (int*)malloc(n * sizeof(int));

    // Check if the memory has been successfully
    // allocated by malloc or not
    if (ptr == NULL) {
        printf("Memory not allocated.\n");
        exit(0);
    }
    else {

      // Memory has been successfully allocated
      printf("Memory successfully allocated using malloc. \n");

      // Get the elements of the array
      //for (i = 0; i < n; ++i) {
      //  ptr[i] = i + 1;
      //}

      // Print the elements of the array
      printf("The elements of the array are: ");
      // for (i = 0; i < n; ++i) {
      // printf("\t%p = \t%d\n, ", &(ptr[i]), ptr[i]);
      //}
      curr_brk = sbrk(0);

      printf("Program break Location 2 :%p\n", curr_brk);

      free(ptr);

      curr_brk = sbrk(0);

      printf("Program break Location 3 :%p\n", curr_brk);

    }

    return 0;
}
