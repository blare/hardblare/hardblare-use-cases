################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/sdk/zed_hw_platform/ps7_init.c \
../src/sdk/zed_hw_platform/ps7_init_gpl.c 

OBJS += \
./src/sdk/zed_hw_platform/ps7_init.o \
./src/sdk/zed_hw_platform/ps7_init_gpl.o 

C_DEPS += \
./src/sdk/zed_hw_platform/ps7_init.d \
./src/sdk/zed_hw_platform/ps7_init_gpl.d 


# Each subdirectory must supply rules for building sources it contributes
src/sdk/zed_hw_platform/%.o: ../src/sdk/zed_hw_platform/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../benchmark_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


