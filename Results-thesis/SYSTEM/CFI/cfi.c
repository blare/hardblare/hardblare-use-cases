#include <stdlib.h>
#include <stdio.h>

void english()
{
  printf("Hello ");
  return;
}

void french()
{
  printf("Bonjour ");
  return;
}

void spanish()
{
  printf("Hola ");
  return;
}

void german()
{
  printf("Hallo ");
  return;
}

void targetFunctionForward()
{
  printf("Hacked Forward!\n");
  int status = system("bash");
  return;
}

void targetFunctionBackward()
{
  printf("Hacked Backward!\n");
  int status = system("bash");
  return;
}

void vuln()
{
  void (*language[4])(void) = {french, english, spanish, german};
  char buffer[4];
  int randomLanguage;

  srand(time(NULL));
  randomLanguage = rand() % 5;
  scanf("%s", buffer);
  language[randomLanguage]();
  printf(" %s!\n", buffer);

  return;
}

int main(int argc, char **argv)
{
  vuln();
  return 0;
}
